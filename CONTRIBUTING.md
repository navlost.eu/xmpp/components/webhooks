# Contribution guidelines

## Reporting issues

Please include as much information as is helpful, particularly:

- a concise description of the issue;
- how to reproduce it, step by step;
- what results you got;
- what results you expected;
- information about the execution environment (operating system, XMPP server, client, etc.); and
- why it is a problem, or otherwise significant enough to merit a report.

### Without a GitLab account

Not everyone has or wants a GitLab account. Issues may be reported by email to: contact-project+navlost-eu-xmpp-components-webhooks-24572215-issue-@incoming.gitlab.com. Once created, each issue has its own unique email address which may be found in the issue page, on the right-hand-side sidebar (search for ‘issue email‘).

### Security considerations

As a general rule, do not file a public issue for serious vulnerabilities. If you have access to GitLab, mark your issue as confidential; otherwise, state the need for confidentiality in your email.

When sending logs or reproducible examples, be sure to sanitise all content that is not relevant to the issue at hand, such as XMPP JIDs, IP addresses and other personal data.

### What happens then

Just because an issue has been filed it doesn't mean that action can, need, or will be taken. Some issues may simply serve a clarification purpose; others may not be aligned with the vision for this project, or may take an unreasonable amount of time to deal with. Regardless, your interest in the project is always appreciated.

## Sending patches

Patches may be offered either in the form of GitLab *merge requests*, or via the project's [email address](contact-project+navlost-eu-xmpp-components-webhooks-24572215-issue-@incoming.gitlab.com). For emailed patches, consider using `git send-email` ([learn more](https://git-send-email.io/)).

All contributions are subject to the following Developer's Certificate of Origin (DCO). By sending your patch to the project you are deemed to have made the statements stipulated in the Developer's Certificate of Origin and agreed to its conditions. For further clarity, it is recommended that you sign off your patches with `git commit --signoff`.

### Developer's Certificate of Origin (version 1.1)

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.

```
By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```

#### Licence

See [LICENCE](LICENSE).
