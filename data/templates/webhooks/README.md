# Webhook templates

The files in this directory define templates for webhooks
events. See the general [README](/README.md) for details
on the format and usage, as well as the comments in
[`default.yaml`](./default.yaml).
