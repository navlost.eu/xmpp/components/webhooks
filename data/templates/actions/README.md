# Action set templates

The files in this directory define templates for *‘action sets’*.
For instance, in [`gitlab.yaml`](./gitlab.yaml),
`gitlab-write` and `gitlab-read` are each an action set.

An action set is a group of zero or more *‘actions’* that have
something in common, such as interacting with the same API
or having the same authorisation level.

Action sets have variables that apply to all its commands.
A typical use of these variables would be to configure the
remote server URL or access tokens.

Furthermore, action sets may allow, or require, users to *‘register’*
in a way that is similar but not exactly the same as described in
[XEP-0077](https://xmpp.org/extensions/xep-0077.html "In-Band Registration")
(in-band registration is not supported at the node level). This is
used for instance so that the user can provide his own access token
to a service.

An action is a command that may be sent to a remote API to
execute an action, omnipotent or not.

Command definitions are suitable both as
[XEP-0050](https://xmpp.org/extensions/xep-0050.html "Ad-Hoc Commands")
ad-hoc commands and as *‘slash commands’* sent in the body of a
`<message>` stanza.

Commands may have *‘parameters’*, which are variables that apply
to a specific invocation of the command. These are passed on via
[XEP-0004](https://xmpp.org/extensions/xep-0004.html "Data Forms")
forms for ad hoc commands, or parsed from the text
for `<message>` stanzas.

Each command ultimately must translate to one (and only one) REST
API call, the parameters of which are defined in the command template.

The definition allows for other interfaces, such as SQL
databases, but only REST calls are defined at present. Likewise, it
would be possible to support commands consisting of a chain of actions
(e.g., multiple REST calls) but that is another level of complexity.
