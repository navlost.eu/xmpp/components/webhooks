#!/bin/bash
#
# Create and start a containerised XMPP server running
# an instance of the webhooks component.
#
# Usage:
#
# create-pod.sh [HOSTNAME [PASSWD]]
#
# Where:
#
# HOSTNAME is the domain name to serve. If not provided it will
# look for an existing configuration and if one is not found it
# will use the $HOSTNAME environment variable.
#
# PASSWD is the password that the component and the XMPP server
# will use to identify each other. If not provided, a random
# password will be generated (requires that uuidgen be installed
# in the host machine).
#
# Note that the container will take over the following ports:
#
# - TCP/80   (HTTP)
# - TCP/443  (HTTPS)
# - TCP/3001 (Component HTTP/HTTPS API)
# - TCP/5222 (XMPP c2s)
# - TCP/5269 (XMPP s2s)
#
# This means that you cannot run this on a server that already
# hosts an HTTP or an XMPP server.
#
# The TLS certificates are requested as needed, and according to
# the ejabberd documentation the renewals should be automatic too,
# but that hasn't been tested.
#
# All data is persisted to the ./var subdirectory.
#

CWD="$(dirname "$0")"

# These are the configuration templates
EJABBERDTPL="$CWD/etc/ejabberd/ejabberd.template.yml"
WEBHOOKSTPL="$CWD/etc/xmpp-webhooks/settings.template.yaml"

# And these are the actual configuration files
EJABBERDCFG="$CWD/etc/ejabberd/ejabberd.yml"
WEBHOOKSCFG="$CWD/etc/xmpp-webhooks/settings.yaml"

# These are the details of our component (hostname + component password)
DOMAIN=${1:-$HOSTNAME}
PASSWD=${2:-$XMPP_COMPONENT_PASSWD}

if [ ! -f "$EJABBERDCFG" ] || [ ! -f "$WEBHOOKSCFG" ]; then
	echo "Configuration files not found"

	if [ -z "$PASSWD" ] && uuidgen >/dev/null; then
		# We generate a random password if we can
		PASSWD=$(uuidgen)
	else
		echo "No password given and I cannot generate one for you. Aborting." >/dev/stderr
		exit 1
	fi

	if [ -z "$DOMAIN" ]; then
		echo "No hostname given and I could not figure it out by myself. Aborting." >/dev/stderr
		exit 2
	fi

	if [ ! -f "$EJABBERDTPL" ] || [ ! -f "$WEBHOOKSTPL" ]; then
		echo "Could not find configuration templates:" >/dev/stderr
		echo -e "\t$EJABBERDTPL" >/dev/stderr
		echo -e "\t$WEBHOOKSTPL" >/dev/stderr
		echo "Aborting." >/dev/stderr
		exit 3
	fi

	echo "Using hostname: $DOMAIN"

	# Replace the relevant bits in the templates to get the actual
	# configuration files.

	DOMAINRX=$(echo "$DOMAIN" |sed -r 's/\./\\\\\\\\\./g')
	REPLRX='example\\\\\.net'

	sed "s/example\.net/$DOMAIN/g;s/@@@PASSWD@@@/$PASSWD/g;s/$REPLRX/$DOMAINRX/g" \
		<"$EJABBERDTPL" >"$EJABBERDCFG"

	sed "s/example\.net/$DOMAIN/g;s/@@@PASSWD@@@/$PASSWD/g" \
		<"$WEBHOOKSTPL" >"$WEBHOOKSCFG"

	echo "Configuration has been created"

else
	CFGDOMAIN=$(sed -nr 's/^\s*server: ([^\s]+)/\1/p' <"$WEBHOOKSCFG")

	if [ -n "$1" ] && [ "$1" != "$CFGDOMAIN" ]; then
		echo "Found configuration for $CFGDOMAIN, but you have requested $1"
		echo "I will now recreate your configuration as $1"
		rm "$EJABBERDCFG" "$WEBHOOKSCFG" &&
		"$0" "$1" "$PASSWD"
		exit 0
	else
		echo "Found configuration for $CFGDOMAIN"
		DOMAIN="$CFGDOMAIN"
	fi
fi

# See if we can find an existing TLS certificate for webhooks.$DOMAIN that
# we can pass on to the NodeJS API.
PEMFILE=""
for f in "$CWD/var/lib/ejabberd/database/ejabberd@xmpp-webhooks-pod/acme/live/"*; do
	if openssl x509 -noout -subject -ext subjectAltName -in "$f" |grep -q webhooks.$DOMAIN; then
		PEMFILE="$(realpath "$f")"
		break
	fi
done

# From here on, it is about creating the pod and launching the containers.
# Note that if the pod had already been created we will be zapping it. It
# shouldn't really matter, since we persist the important data.

# Build our own image
podman build -f ../../Containerfile -t navlost.eu/xmpp/components/webhooks

podman pod stop xmpp-webhooks-pod   2>/dev/null
podman container rm xmpp-webhooks   2>/dev/null
podman container rm ejabberd        2>/dev/null
podman pod rm xmpp-webhooks-pod     2>/dev/null

podman pod create --name xmpp-webhooks-pod \
	-p 3001:3001 \
	-p 5222:5222 -p 5269:5269 \
	-p 80:5280 -p 443:3001

podman run --pod xmpp-webhooks-pod -d --rm --name ejabberd \
	-v "$CWD/etc/ejabberd/ejabberd.yml:/home/ejabberd/conf/ejabberd.yml" \
	-v "$CWD/var/lib/ejabberd/database:/home/ejabberd/database" \
	ejabberd/ecs

# Give ejabberd enough time to start, else the component will just
# fail to connect.
sleep 10

if [ -z "$PEMFILE" ]; then
	# Running in HTTP mode
	podman run --pod xmpp-webhooks-pod -d --rm --name xmpp-webhooks \
		-v "$CWD/etc/xmpp-webhooks/settings.yaml:/usr/local/share/xmpp-webhooks/settings.yaml" \
		-v "$CWD/etc/xmpp-webhooks/templates.yaml:/usr/local/share/xmpp-webhooks/templates.yaml" \
		navlost.eu/xmpp/components/webhooks

	sleep 6

	# Running on the metal ejabberd detects the component correctly and gets the
	# ACME certificate the first time around, but it doesn't seem to do that in the
	# containerised version, so we request the certificate explicitly (the component
	# needs to be connected to the server first for this to work).
	podman exec -it ejabberd bin/ejabberdctl request_certificate webhooks.$DOMAIN

	sleep 3
	echo "Generated certificate for webhooks.$DOMAIN. Now I will restart"
	"$0" "$DOMAIN" "$PASSWD"
	exit
else
	# Running in HTTPS mode
	podman run --pod xmpp-webhooks-pod -d --rm --name xmpp-webhooks \
		-v "$CWD/etc/xmpp-webhooks/settings.yaml:/usr/local/share/xmpp-webhooks/settings.yaml" \
		-v "$CWD/etc/xmpp-webhooks/templates.yaml:/usr/local/share/xmpp-webhooks/templates.yaml" \
		-v "$PEMFILE:/usr/local/share/xmpp-webhooks/server.pem" \
		-e "HTTPS_KEY_PEM=/usr/local/share/xmpp-webhooks/server.pem" \
		-e "HTTPS_CERT_PEM=/usr/local/share/xmpp-webhooks/server.pem" \
		navlost.eu/xmpp/components/webhooks
fi

echo "$DOMAIN is up and running"
