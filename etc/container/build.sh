#!/bin/bash
#
# Build a container image.
#

CWD=$(dirname "$0")

cd "$CWD"/../..

podman build . -t navlost.eu/xmpp/components/webhooks

cd -
