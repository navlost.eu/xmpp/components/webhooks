# Actions

*Actions* are commands that are sent to an external system for processing; for instance, API requests. They enable two-way interaction between the component and the external system.

These are known as *slash commands* in other contexts. In this component, all actions may be invoked as [XEP-0050](https://xmpp.org/extensions/xep-0050.html "Ad-Hoc Commands") ad hoc commands and, when so configured, directly from a chat message in the form of a so-called slash command (even though there won't necessarily be any slashes involved!)

Example, as a slash command:

![](media/110-action-example.png)

The difference between an action and a webhook is that the former causes something to happen whereas the latter notifies you when something has happened.

## Invocation

### Via chat messages

#### Structure

An action consists of a *prefix*, followed by a *command*, optionally followed by arguments. Unlike traditional slash commands, the prefix can be any arbitrary string, not necessarily a slash character. Commands are divided into *sets*, each of which will usually have their own distinct prefix.

Example:

<span style="color:blue;">/gitlab</span> <span style="color:green;">comments for issue</span> <span style="color:red;">1</span>

Where:

<table>
<tbody>
<tr>
<td>
<span style="color:blue;">/gitlab</span>
</td>
<td>Prefix</td>
</tr>
<tr>
<td>
<span style="color:green;">comments for issue</span></td>
<td>Command</td>
</tr>
<tr>
<td>
<span style="color:red;">1</span></td>
<td>Arguments</td>
</tr>
</tbody>
</table>

#### Discovering available commands

Sending a message consisting of just the prefix will cause the endpoint to reply with a listing of available commands for that prefix.

![](media/115-command-listing.png)

#### Discovering available prefixes

The user may get a listing of available prefixes by sending the literal string `/help`. Note that this is not itself a prefix.

![](media/120-prefix-listing.png)

### Via ad hoc commands

All actions are available as [XEP-0050 ad hoc commands](https://xmpp.org/extensions/xep-0050.html "XEP-0050: Ad-Hoc Commands") (although the opposite is not necessarily the case: some commands may be available *only* as ad hoc). They sit under the “Action sets” node of the associated endpoint.

![](media/125-gajim-repo-actions.png)

Ad hoc commands are run via the usual mechanism in your client and arguments, if any, will be provided via [XEP-0004 forms](https://xmpp.org/extensions/xep-0004.html "XEP-0004: Data Forms").

![](media/127-command-execution-dialogue.png)

The results of the command will be sent to the user as a message.

## Registration

Some commands may require the user to provide some information prior to first use. For instance, a command that files an issue in the bug tracker may require an authorisation token from the user to give access to the relevant API.

Registration may be compulsory or facultative, depending on the definition of the specific collection of actions.

In order to register with an “action set” (a collection of actions), the user must run the “Register with an action set” ad hoc command on the relevant endpoint.

![](media/130-action-set-registration-command.png)

The user will be asked to choose the action set he wishes to register with.

![](media/131-action-set-registration-selecttion.png)

And the necessary details will be requested and saved by the component.

![](media/132-action-set-registration-details.png)

Where registration is optional, the command may fall back to using credentials provided by the endpoint owner instead.

To unregister, the user must run the “Unregister from an action set” command and choose the action set he no longer wishes to be registered with.

Registration *must* be done via ad hoc command. There is no possibility to register via regular chat messages.

## Defining action sets (endpoint owners)

![](media/135-add-action-set.png)

Action sets are based on templates provided by the administrator of your component instance. A list of those is available under the “Action set templates” node at the root of the component.

To add an action set to an endpoint, the endpoint owner runs the “Add action sets” ad hoc command and fills out the relevant details, which depend on the action set being added. For instance, GitLab actions will ask for the URL of the GitLab server, the ID of the target project (found in the project's information page) and possibly an authorisation token.

![](media/137-project-id-information-page.png)

![](media/136-add-gajim-actions.png)

After the action set specific configuration parameters, the command will ask for a regular expression to designate who is authorised to use these commands. The default value allows everyone with access to the endpoint to also use actions.

![](media/138-action-set-permissions.png)

The “Allow MUC commands” option is not implemented as of this writing, but it will allow the endpoint to respond to actions posted to chatrooms of which it is a member.

### Updating the definition of an existing action set

Use the same “Add action sets” command. If you select the same template and provide the same prefix, configuration parameters will default to their current values and user registrations will be kept.

### Adding the same template more than once

It is perfectly possible to add the same template more than once, presumably with different parameters, provided that the chosen prefix is different from any other prefix already configured for that template. For instance, the following image shows two different action sets based on the same template (GitLab read-only actions) pointing to two different projects:

![](media/139-multiple-action-sets.png)

In that example, users would issue the command `/kaidan issue list` to retrieve the list of Kaidan project issues or `/gajim issue list` to list Gajim issues. Likewise, they can register with neither, either or both action sets.

### Adding different templates under the same prefix

That is also possible and supported, provided that the command names do not clash. For instance, it is possible to have both the GitLab read-only and GitLab read/write action sets under the common prefix `/gitlab`. Sending the bare prefix will list commands from all sets associated with that prefix.

![](media/140-common-prefix.png)

## Adding templates (component administrators)

![](media/141-action-set-templates-list.png)

The component administrators are responsible for maintaining the list of action templates available to endpoint owners. On a new installation, no action templates are installed by default.

![](media/142-add-action-set-templates-command.png)

To add templates, the administrator must run the “Add action set templates” ad hoc command and provide the template definition, which may be in YAML or JSON. A small number of definitions are shipped with the component and may be found under [`data/templates/actions`](/data/templates/actions). To use them, copy the file contents and paste them in the command input form.

![](media/143-add-action-set-templates-form.png)

The file [`data/templates/actions/gitlab.yaml`](data/templates/actions/gitlab.yaml) is amply commented describing the format and serves as a base for creating new templates.

Note that multiple action sets may be defined in a single YAML / JSON object.

### Updating templates

Use the “Add action set templates” command and provide the new definition(s). Note that this does not affect the configuration of existing endpoints. In order to use the new template definition, endpoint owners must update their existing action sets as described above.

### Action templates

Actions are grouped in sets having something in common, such as interacting with the same API or having the same authorisation level.

These actions may declare variables that apply to all its commands. A typical use of these variables would be to configure the remote server URL or access tokens.

Furthermore, action sets may allow, or require, users to *‘register’* in a way that is similar but not exactly the same as described in [XEP-0077](https://xmpp.org/extensions/xep-0077.html "In-Band Registration") (in-band registration is not supported at the node level). This is used for instance so that the user can provide his own access token to a service.

Templates are defined in YAML (or JSON) files. The structure of a template file is:

```yaml
---
actionset-id: # Must be unique across an installation
    name: "Descriptive name for the action set (e.g., ‘GitLab commands’)"
    description: |
        A longer description of this action set.
    version: 0.0.0 # A version number. Currently unused

    variables:
        # Variable declarations

    registration:
        # User registration parameters

    commands:
        # Command definitions

    aliases:
        # Map of command aliases
```

Refer to the [bundled action set definitions](/data/templates/actions/) for examples of the syntax of each section.

#### Form data

The following sections consist of [XEP-0004](https://xmpp.org/extensions/xep-0004.html "Data Forms") form definitions and have a common syntax:

* `registration.variables`
* `variables`
* `commands.*.parameters`

The syntax is as follows:

```yaml
variables: # The same applies to registration.variables, or to commands.*.parameters
    variable-id0: # The name of the variable being declared
        type: "text-single" # Or any of the types defined in XEP-0004 (text-single is the default)
        label: "Variable label"
        desc: "Variable description"
        value: "Default value" # Optional
        required: false # Or true (default is false)
    variable-id1:
        type: text-multi
        join: "\n" # By default, text-multi results are returned as an array of lines.
        # label, desc, required, etc. as above
    variable-id2:
        type: list-multi
        options:
            - label: Option label A
              value: option-value-a
            - label: Option label B
              value: option-value-b
    value: option-value-b # The value to be initially selected
    # label, desc, required, etc. as above
```

#### Command templates

Command definitions have the following structure:

```yaml
actionset-id:

    # …other definitions…

    commands:
        command-id:
            type: REST
            name:
            description:
            parameters:
                # Form data, as discussed above

            url: "" # EJS template to build the remote endpoint URL
            method: "" # EJS template that returns an HTTP verb (GET, POST, etc.)
            headers:
                header-name: "" # EJS template that returns a value for the corrresponding HTTP header

            payload: "" # EJS template that returns a request body, where the HTTP method accepts one

            response: ""
                # EJS template to turn the response into an XMPP message.
                # The response details are in the `res` variable and the response body in `body`.
                # `body` may be either a JSON object, a text string or an ArrayBuffer object,
                # depending on the MIME type or the value of `decodeResponseAs`, if provided.

            decodeResponseAs: "" # One of "json", "text" or "binary"
                # Defaults to checking the MIME type very cursorily,
                # so it's best to indicate it explicitly for anything
                # that's not "application/json".
```

#### Aliases

These are alternative trigger strings. For example, the definition below would allow the same action to be invoked using either of the following slash commands:

* `/issue new`
* `/create issue`

```yaml
actionset-id:

    # …other definitions…

    commands:
        "issue new":
            # Etc.

    aliases:
        "create issue": "issue new"
```
