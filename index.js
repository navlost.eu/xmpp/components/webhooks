#!/usr/bin/node

const fs = require('fs');
const path = require('path');
const YAML = require('yaml');
const ejs = require('ejs');
const { readTemplates } = require('~/lib/utils/templates');

process.env.NODE_PATH = __dirname;

const logger = require('./lib/logger');
const WebhookHttpApi = require('./lib/WebhookHttpApi');
const WebhookComponent = require('./lib/WebhookComponent');

function parseCommandLine () {
	let options = {};
	let argv = process.argv.slice(2);
	while (argv.length) {
		const opt = argv.shift();
		switch (opt) {
			case "--load-file":
			case "-l":
				options.json = JSON.parse(fs.readFileSync(argv.shift()));
				break;
			case "--load-json":
			case "-j":
				options.json = JSON.parse(argv.shift());
				break;
			case "--save-file":
			case "-s":
				options.save = argv.shift();
				break;
			default:
				console.error("Unknown option:", opt);
		}
	}
	return options;
}

logger.log(logger.main, '⏼', 'starting up');

const options = parseCommandLine();

// Read configuration
const settings = YAML.parse(fs.readFileSync(path.join(__dirname, "settings.yaml")).toString("utf8"));
logger.log(logger.main, '📖', 'read configuration');

const templates = readTemplates(path.join(__dirname, "data/templates/webhooks"));
logger.log(logger.main, '📖', 'read templates');

// See if we've been given PEM credentials, to run in HTTPS mode
if (process.env.HTTPS_KEY_PEM) {
	const options = settings.http.options ?? {};
	options.key = fs.readFileSync(process.env.HTTPS_KEY_PEM);
	settings.http.options = options;
}

if (process.env.HTTPS_CERT_PEM) {
	const options = settings.http.options ?? {};
	options.cert = fs.readFileSync(process.env.HTTPS_CERT_PEM);
	settings.http.options = options;
}

const api = new WebhookHttpApi(settings.http);
logger.log(logger.main, '🔖', 'HTTP API created');

const component = new WebhookComponent({settings});
logger.log(logger.main, '🔖', 'XMPP component created');

api.component = component;
api.on('message', async (data) => {
	console.log("MESSAGE", Buffer.isBuffer(data?.payload) ? data : JSON.stringify(data, null, 4));
	const tpl = templates?.[data.type];
	if (tpl) {
		const project = component.items.find(i => i.uuid == data.id);
		if (project) {

			// This is a bit more complex than other keys, so we put the code in its
			// own function for readability. It is only used once when defining `message`
			// a few lines below.
			function renderHints () {

				// As of XEP-0334 Version 0.3.0 (2018-01-25)
				const validHints = [
					"no-permanent-store",
					"no-store",
					"no-copy",
					"store"
				];

				const hints =
					Object.keys(tpl?.["urn:xmpp:hints"] ?? {})
					.filter(hint => validHints.includes(hint))
					.filter(hint =>
						ejs.render(tpl["urn:xmpp:hints"][hint], {...data, project}) == "true"
					);

				return hints.length ? hints : undefined;
			}

			const message = {
				subject: ejs.render(tpl?.subject ?? "", {...data, project}),
				body: ejs.render(tpl?.body ?? "", {...data, project}),
				url : ejs.render(tpl?.url ?? "", {...data, project}),
				"urn:xmpp:attention:0": ejs.render(tpl?.attention ?? "", {...data, project}) == "true",
				"urn:xmpp:hints": renderHints()
			};
			let upload = tpl?.upload
				? {
					body: data.payload,
					filename: ejs.render(tpl.upload.filename ?? "", {...data, project}),
					mimetype: ejs.render(tpl.upload.mimetype ?? "", {...data, project})
				}
				: null;

			const hasUpload = !!(upload?.body && upload?.filename && upload?.mimetype);

			if (hasUpload) {
				console.log("UPLOAD", project.toJSON(), upload.filename, upload.mimetype);
			} else {
				upload = null;
			}

			message.upload = upload;

			if (message.subject || message.body || message.url || hasUpload) {
				console.log("NOTIFICATION", project.toJSON(), message);
				await component.sendWebhookNotification(project, message);
			}
		}
	}
});

// If an authenticator is present in the instance, all requests
// will be passed to it for approval before being processed any
// further.
api.authenticator = (req) => {
	const project = component.items.find(i => i.uuid == req.params.id);
	if (project) {
		return project.authorise(req);
	}
}


async function start () {

	const startupHandlers = [];

	if (options.json) {
		logger.info(logger.main, "💾", "Load persisted items from JSON");
		await component.readPersisted(options.json);
		startupHandlers.push(async function () {
			await this.discover();
			await this.savePersisted(options.json);
			await this.readPersisted();
		});
	}

	if (options.save) {
		logger.info(logger.main, "💾", "Request saving of persisted items to", options.save);
		const fn = async function () {
			const canSave = this && Object.keys(this.discovery).length;
			if (canSave) {
				const json = this.toJSON();
				fs.writeFileSync(options.save, JSON.stringify(json, null, 4));
				logger.info(logger.main, "💾", "Saved persisted items to", options.save);
			} else {
				logger.debug("⏱", "Deferred saving of persistent items until discovery has run");
				// FIXME We give it a generous delay and hope not to have to deal with the
				// race condition between this call and GenericComponent.discover() too often.
				// Another and probably better option would be to do the save on shutdown, as
				// opposed to start-up, since that can be controlled by the user.
				setTimeout(fn.bind(this), 2000);
			}
		}
		startupHandlers.push(fn);
	}

	await component.start(startupHandlers);
	logger.log(logger.main, '⏯', 'component initialised');

	api.start();
	logger.log(logger.main, '⏯', 'API initialised');

	logger.info(logger.main, '⏻', 'system started');
}

process.on('SIGINT', async () => {
	const t = setTimeout(() => {
		logger.warn("⚠", logger.c.red("Forcing exit!"));
		process.exit();
	}, 5000);

	logger.log(logger.main, '⏾', 'shutting down API');
	await api.stop();

	logger.log(logger.main, '⏾', 'shutting down component');
	await component.stop();

	clearTimeout(t);
	logger.info(logger.main, '⏾', 'shut down complete');
});

process.on('SIGUSR1', async () => {
	logger.debug(logger.main, 'SIGUSR1 received. Dumping state');
	console.log(JSON.stringify(component, null, 4));
});

process.on('SIGUSR2', async () => {
	logger.debug(logger.main, 'SIGUSR2 received. Saving state');
	await component.savePersisted();
});

start();
