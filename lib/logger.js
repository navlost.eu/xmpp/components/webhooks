const clr = require('ansi-colors');

clr.theme({
	main: clr.bold.italic,
	xmpp: clr.bold,
	mqtt: clr.italic,

	log:   clr.dim.blue,
	debug: clr.dim.white,
	info:  clr.cyan,
	warn:  clr.bold.underline.yellow,
	error: clr.bold.underline.red
});

const main = Symbol();
const xmpp = Symbol();
const mqtt = Symbol();
const http = Symbol();
const db   = Symbol();

function logger(type, args) {

	let prefix = clr;

	if (typeof args[0] === 'symbol') {
		args = [...args];
		const channel = args.shift();
		switch (channel) {
			case main:
				prefix = clr.main;
				break;
			case xmpp:
				prefix = clr.xmpp;
				break;
			case mqtt:
				prefix = clr.mqtt;
				break;
		}
	}
	console[type](prefix[type]([...args].join(" ")));
}

function level () {
	const l = ("NODE_LOG" in process.env)
		? process.env.NODE_LOG
		: ("NODE_ENV" in process.env)
			? process.env.NODE_ENV == "production"
				? "info"
				: "log"
			: "log";

	switch (l) {
		case "log":
			return 5;
		case "debug":
			return 4;
		case "info":
			return 3;
		case "warn":
			return 2;
		case "error":
			return 1;
		case "quiet":
		case "none":
		case "":
			return 0;
		default:
			return 3;
	}
}

module.exports = {
	log () { level() > 4 && logger("log", arguments) },
	debug () { level() > 3 && logger("debug", arguments) },
	info () { level() > 2 && logger("info", arguments) },
	warn () { level() > 1 && logger("warn", arguments) },
	error () { level() > 3 && logger("error", arguments) },

	xmpp,
	mqtt,

	c: clr
}
