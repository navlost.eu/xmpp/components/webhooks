const { exec } = require("child_process");


/** Return software name.
 *
 */
function name () {
	const pkg = require("../package.json");
	return pkg.name ?? pkg.description ?? "Unknown";
}

/** Return software version.
 *
 */
async function version () {
	return new Promise( (resolve, reject) => {
		if (process.env.NODE_ENV == "production") {
			const pkg = require("../package.json");
			if (pkg.version) {
				return resolve(pkg.version);
			}
			// …and otherwise fall back to Git tag / revision.
		}

		const cmd = `git describe || echo git+$(git describe --always);`;
		exec(cmd, {cwd: __dirname}, (error, stdout, stderr) => {
			if (error) {
				reject(error);
			}

			if (stdout) {
				resolve(stdout.trim());
			} else {
				// Most likely not installed from Git, use the
				// version number in package.json.
				const pkg = require("../package.json");
				resolve(pkg.version ?? "Unknown")
			}
		})
	});
}

module.exports = { name, version }
