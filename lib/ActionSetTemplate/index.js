const crypto = require('crypto')
const {xml} = require('@xmpp/component')

class ActionSetTemplate {

	static node = "http://navlost.eu/xmpp/components/webhooks/actions/sets/template";

	constructor (params = {}) {
		this.parent = params.parent;
		this.jid = this.parent.jid;

		this.id = params.id ?? crypto.randomBytes(8).toString("hex");
		this.name = params.name;
		this.description = params.description;
		this.version = params.version;
		this.registration = params.registration;
		this.variables = params.variables;
		this.commands = params.commands;
		this.aliases = params.aliases;
	}

	get node () {
		return `${ActionSetTemplate.node}#${this.id}`;
	}

	discoInfo (ctx) {
		return xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'hierarchy',
					type: 'leaf',
					name: this.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
		);
	}

	discoItem (ctx) {
		return xml('item', {jid: this.parent.jid, name: this.name, node: this.node});
	}

	toJSON () {
		return {
			node: ActionSetTemplate.node,
			id: this.id,
			name: this.name,
			description: this.description,
			version: this.version,
			registration: this.registration,
			variables: this.variables,
			commands: this.commands,
			aliases: this.aliases
		};
	}
}

module.exports = ActionSetTemplate;
