const { createHash } = require("crypto");
const parse = require("@xmpp/xml/lib/parse");
const {xml} = require('@xmpp/component');

function xmppCapabilities (node, discoInfo) {
	const res = xml("c", {
		xmlns: "http://jabber.org/protocol/caps",
		hash: "sha-1",
		node: node,
		ver: ""
	});

	const identities = discoInfo.getChildren("identity").map(i => {
		return [
			i.attrs.category ?? "",
			i.attrs.type ?? "",
			i.attrs["xml:lang"] ?? "",
			i.attrs.name ?? ""
		].join("/");
	});
	res.attrs.ver += identities.sort().join("<") + "<";
	const features = discoInfo.getChildren("feature").map(i => i.attrs.var);
	res.attrs.ver += features.sort().join("<") + "<";
	const form_data = discoInfo
	.getChildren("x", "jabber:x:data")
	.filter(i => i.attrs.type == "result");
	if (form_data) {
		for (const form of form_data) {
			const fields = form.getChildren("field");
			const form_type = fields.find(field =>
				field.attrs.var == "FORM_TYPE"
			)?.getChild("value")?.text();
			if (form_type) {
				res.attrs.ver += form_type + "<";
				res.attrs.ver += fields
				.filter(field => field.attrs.var != "FORM_TYPE")
				.sort( (a, b) => a.attrs.var < b.attrs.var ? -1 : a.attrs.var > b.attrs.var ? 1 : 0 )
				.map(field => {
					const values = field.getChildren("value").map(value => value.text()).sort();
					return field.attrs.var + "<" + values.join("<");
				}).join("<") + "<";
			}
		}
	}

	const sha1 = createHash("sha1");
	sha1.update(res.attrs.ver);
	res.attrs.ver = sha1.digest("base64");

	return res;

}

module.exports = xmppCapabilities;
