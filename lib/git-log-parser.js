
/**
 * Try to parse a Git commit message
 *
 * The commit message is assumed to have been produced by
 * git log -n 1 --format=medium
 *
 */
function GitLogParser (text) {
	const result = {};

	const lines = text.split(/\r?\n/);

	const cRx = /^commit ([a-f0-9]+)$/;
	const aRx = /^Author: (.*)$/;
	const dRx = /^Date: (.*)$/;
	const sRx = /^    (.*)$/

	result.commit = (lines.map(l => l.match(cRx)).filter(l => l)[0] || [])[1];
	result.author = (lines.map(l => l.match(aRx)).filter(l => l)[0] || [])[1];
	result.tstamp = (lines.map(l => l.match(dRx)).filter(l => l)[0] || [])[1];
	result.subject = (lines.map(l => l.match(sRx)).filter(l => l)[0] || [])[1];
	result.message = lines
		.slice(lines.findIndex(l => l.match(sRx))+2)
		.map(l => l.replace(/^    /, ""))
		.join("\n");

	if (result.tstamp) {
		result.tstamp = (new Date(result.tstamp)).toISOString();
	}

	return result;
}

module.exports = GitLogParser;
