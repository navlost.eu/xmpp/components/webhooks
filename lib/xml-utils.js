const {xml, jid} = require('@xmpp/component');
const IncomingContext = require('@xmpp/middleware/lib/IncomingContext');

function toXML (el) {
	return el.name ? xml(el.name, el.attrs, (el.text ? el.text : el.children?.map(i => toXML(i)))) : el;
}

function toJSON (el) {
	if (el instanceof xml.Element) {
		return {
			name: el.name,
			attrs: el.attrs,
			children: el.children?.map(child => toJSON(child))
		}
	} else {
		return el;
	}
}

/** Get JIDs from ctx.
 *
 * @a ctx It may be an IncomingContext, or an Element or
 * any other object having `to` and/or `from` attributes.
 *
 * @returns {to: JID(), from: JID()}
 *
 * The idea is to simplify the getting of addresses when
 * we're not sure if we're being passed a context or a
 * stanza or something else.
 */
function getJID (ctx) {
	if (ctx instanceof IncomingContext) {
		return {
			to: ctx.to,
			from: ctx.from
		};
	} else if (ctx instanceof xml.Element) {
		return {
			to: jid(ctx.attrs.to),
			from: jid(ctx.attrs.from)
		}
	} else {
		return {
			to: ctx?.to && (ctx.to instanceof jid.JID ? ctx.to : jid(ctx.to)),
			from: ctx?.from && (ctx.from instanceof jid.JID ? ctx.from : jid(ctx.from))
		}
	}
}

/** Get stanza from an object of uncertain type.
 *
 */
function getStanza (ctx) {
	if (ctx instanceof IncomingContext) {
		return ctx.stanza;
	} else if (ctx instanceof xml.Element) {
		return ctx;
	} else if (ctx?.stanza) {
		return ctx.stanza;
	} else if (typeof ctx == "string") {
		return toXML(ctx);
	} else {
		return ctx;
	}
}


module.exports.toXML = toXML; // JSON to xml()
module.exports.toJSON = toJSON; // xml() to JSON
module.exports.serialize = require("ltx/lib/stringify"); // xml() to string
module.exports.parse = require('@xmpp/xml/lib/parse'); // string to xml()

module.exports.getJID = getJID; // Get a JID from whatever we've been fed
module.exports.getStanza = getStanza; // Get a stanza from whatever we've been fed
