const {xml} = require('@xmpp/component')
const ActionSetCommand = require('~/lib/ActionSetCommand');
const {getSender, getComponent} = require('~/lib/utils/component');

class ActionSet {

	static node = "http://navlost.eu/xmpp/components/webhooks/actions/set";

	constructor (params = {}) {
		this.parent = params.parent;
		this.jid = this.parent.jid;
		this.root = getComponent(this);

		this.id = params.id;
		this.prefix = params.prefix;
		this.name = params.name;
		this.description = params.description;
		this.allowed = new RegExp(params.allowed, "i");
		this.allowMUC = params.allowMUC;

		this.version = params.version;
		this.registration = params.registration;
		this.variables = params.variables;
		this.commands = params.commands;
		this.aliases = params.aliases;

		this.users = params.users ?? {};

		this.nodes = [];
		this.loadNodes(params.commands)

		// NOTE This is pointless since you cannot register with
		// a node (it has to be an entity addressable as a JID),
		// but leaving it here for reference.
// 		if (this.registration) {
// 			this.registrationHandler = new XMPPRegister({
// 				parent: this,
// 				jid: this.jid,
// 				node: this.node,
// 				name: this.name, // FIXME not used?
// 				instruction: "Fill in the requested values to use these commands",
// 				fields: this.registration.variables
// 			}, {
// 				isUserAllowed: this.isUserAllowed,
// 				isUserRegistered: this.isUserRegistered,
// 				onUserRegistration: this.registerUser,
// 				onUserDeregistration: this.removeUser
// 			});
//
// 			// Key: User JID, Value: registration form values
// 			this.users = {}
// 		}
// 		console.log("NEW ACTION SET", params);
	}

	get kind () {
		return ActionSet.node;
	}

	get node () {
		const url = new URL(ActionSet.node);
		url.searchParams.set("id", this.id);
		url.searchParams.set("prefix", this.prefix);
		return url.href;
	}

	loadNodes (nodes = []) {
		if (!Array.isArray(nodes)) {
			// Transform object → array
			const arr = Object.entries(nodes).map(i => ({id: i[0], ...i[1]}));
			return this.loadNodes(arr);
		}

		for (const definition of nodes) {
			const nodeItem = new ActionSetCommand({
				...definition,
				parent: this
			});
			const replaceIndex = this.nodes.findIndex(n => n.id == nodeItem.id);
			if (replaceIndex != -1) {
				this.nodes[replaceIndex] = nodeItem;
			} else {
				this.nodes.push(nodeItem);
			}
		}
		// TODO We probably should send a <message> with the new version of this node.
		// Check XEPs for that.
	}

	/** Get the information we hold for a given user.
	 *
	 * Returns undefined if the user is not registered.
	 */
	getUser(ctx) {
		const sender = getSender(ctx);

		if (sender in this.users) {
			return this.users[sender];
		}
	}

	isUserAllowed (ctx) {
		// We do always need the bare sender here,
		// at the moment, because of the default
		// regexp not taking into account the
		// resource part.
		const sender = ctx.from.bare().toString();
		// FIXME Broken:
		// If allowMUC is false but this.allowed.test
		// matches, it will allow private messages via
		// chatrooms.
		// Hmm… broken or feature? 🤔
		return this.allowed.test(sender) ||
			this.isGroupchatAllowed(ctx);
	}

	isUserRegistered (ctx) {
		const sender = getSender(ctx);
		return sender in this.users;
	}

	registerUser (ctx, form, opts={}) {
		const sender = getSender(ctx);

		if (this.isUserAllowed(ctx)) {
			if (opts.overwrite || !this.isUserRegistered(ctx)) {
				this.users[sender] = form;
				return true;
			} else {
				// TODO Should send back user registration data
				return "conflict";
			}
		} else {
			return "forbidden";
		}
	}

	removeUser (ctx) {
		const sender = getSender(ctx);

// 		console.log("removeUser", sender);
		if (this.isUserRegistered(ctx)) {
// 			console.log("Deleting", sender);
			delete this.users[sender];
		}
// 		console.log("users", JSON.stringify(this.users, null, 4));
		return true; // Whether the user existed or not
	}

	/** Is this group chat message, or this private message sent
	 * from a group chat, allowed?
	 *
	 * NOTE that a private message sent via a group chat will have
	 * a message type of "chat" and a bare JID corresponding to the
	 * group chat. This is why we need to check both isUserAllowed()
	 * and isGroupchatAllowed().
	 */
	isGroupchatAllowed(ctx) {
		const sender = ctx.from.bare().toString();
		return this.allowMUC && this.root.hasSubscription({
			from: sender,
			to: this.jid,
			type: "groupchat"
		});
	}

	/** Return registration data for JID.
	 *
	 * If JID has been registered (XEP-????) with this node,
	 * return whatever registration data we have collected.
	 *
	 * If the JID is not registered, return undefined.
	 */
// 	registration (jid) {
// 		// TODO we don't handle registration yet
// 		console.log("REGISTRATION CALLED");
// 		return;
// 	}

	discoInfo (ctx) {
		const response = xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'hierarchy',
					type: 'branch',
					name: this.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#items' }
			),
		);

		if (this.version) {
			response.append(xml(
				'feature',
				{ 'var': 'jabber:iq:version' }
			));
		}

// 		console.log("DO WE HANDLE REGISTRATION", this.node, this.registration, this.isUserAllowed(ctx));
		if (this.registration && this.isUserAllowed(ctx)) {
			response.append(xml(
				'feature',
				{ 'var': 'jabber:iq:register' }
			));
		}

		return response;
	}

	discoItem (ctx) {
		return xml('item', {jid: this.parent.jid, name: this.name, node: this.node});
	}

	toJSON () {
		return {
			node: ActionSet.node,
			id: this.id,
			prefix: this.prefix,
			name: this.name,
			description: this.description,
			allowed: this.allowed.source,
			allowMUC: this.allowMUC,
			version: this.version,
			registration: this.registration,
			variables: this.variables,
			commands: this.commands,
			aliases: this.aliases,
			users: this.users
		};
	}
}

module.exports = ActionSet;
