const {xml} = require('@xmpp/component')
const commands = require('./commands');
const DefaultNodeFactories = require('./nodes');
const {parse, serialize, getJID} = require('~/lib/xml-utils');
const {verifyStripeSignature, verifyGiteaSignature, verifyNextcloudSignature} = require('~/lib/utils');

class Project {
	constructor (params = {}) {

		this.parent = params.parent;
		this.owner = params.owner;
		this.jid = params.jid;
		this.name = params.name;
		this.url = params.url;
		this.email = params.email;
		this.description = params.description;
		this.notifications = params.notifications;
		this.autojoin = params.autojoin;
		this.nick = params.nick;
		this.username = params.username;
		this.password = params.password;
		this.tokenName = params.tokenName;
		this.tokenValue = params.tokenValue;
		this.allowed = params.allowed;
		this.uuid = params.uuid;
		this.lastActivity = params.lastActivity ?? (Date.now()/1000);
		this.commands = commands.map(c =>
			new c({parent: this, jid: this.jid})
		);
		this.notificationsQueue = (params.notificationsQueue || []).map( i => parse(i))
		this.nodes = [];
		this.#createDefaultNodes(params.nodes);
	}

	#createDefaultNodes (nodes = []) {
		for (const DefaultNodeFactory of DefaultNodeFactories) {
			if (!this.nodes.find(n => n.node == DefaultNodeFactory.node)) {
				const params = nodes.find(n => n.node == DefaultNodeFactory.node);
				this.nodes.push( new DefaultNodeFactory({...params, parent: this}) );
			}
		}
	}

	/** Return true if the requesting user is allowed access to this endpoint.
	 */
	isUserAllowed (ctx) {
		const pair = {from: getJID(ctx).from, to: this.jid};
		return this.parent.subscriptionAllowed(pair) ||
			[ this.owner, this.parent.owner ].includes(getJID(ctx)?.from.toString());
	}

	discoInfo (ctx) {
		return xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'headline',
					type: 'rss',
					name: this.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
			// This JID may have child items, notably action sets, so we
			// must announce disco#items (other than for node="commands")
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#items' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#items', node: "commands" }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/commands' }
			),
			// FIXME Capabilties which are not provided directly by us but
			// by the component should be added to the advertisement list
			// by the component itself – either GenericComponent or
			// WebhookComponent.
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/caps' }
			),
			// FIXME See previous fixme.
			xml(
				'feature',
				{ 'var': 'urn:xmpp:ping' }
			),
			xml(
				'feature',
				{ 'var': 'vcard-temp' }
			),
			xml(
				'feature',
				{ 'var': 'jabber:iq:last' }
			),
			xml(
				'feature',
				{ 'var': 'urn:xmpp:receipts' }
			),
			xml(
				'feature',
				{ 'var': 'urn:xmpp:chat-markers:0' }
			)
		);
	}

	discoItem (ctx) {
		// Items are visible by:
		// - those who can subscribe to it
		// - the item's owner
		// - the instance's owner
		if (this.isUserAllowed(ctx)) {
			return xml('item', {jid: this.jid, name: this.name});
		}
	}

	vCardTemp (ctx) {
		const url = (this.parent.settings?.http?.url || "{id}").replace('{id}', this.uuid);
		let desc = "";
		if (this.description) {
			desc += `\n\n${this.description}`;
		}
		desc += `\n\n• Webhook endpoint:\n    ${url}`;
		if (this.notifications) {
			desc += `\n• Incoming XMPP messages are accepted.`;
		}
		if (this.autojoin) {
			desc += `\n• Chatroom invitations are accepted.`;
			if (this.nick) {
				desc += `\n    Will join with nick: ${this.nick}`;
			}
		}
		desc += `\n\nWebhook XMPP Component © Navlost\nhttps://gitlab.com/navlost.eu/xmpp/components/webhooks`;

		return xml("vCard", "vcard-temp",
			xml("FN", {}, this.name),
			xml("URL", {}, this.url),
			xml("EMAIL", {},
				xml("INTERNET"),
				xml("PREF"),
				xml("USERID", {}, this.email)
			),
			xml("DESC", {}, desc),
			xml("AGENT", {},
				xml("vCard", "vcard-temp",
					xml("JABBERID", {}, this.owner)
				)
			)
		);
	}

	async flushNotificationsQueue (component) {
		if (!component) return;

		for (const stanza of this.notificationsQueue) {
			component.chatMarkers(stanza);
		}

		this.notificationsQueue.length = 0;
	}

	/** Query if this endpoint requires authentication
	 */
	isAuthenticationRequired () {
		return (this.username || this.tokenName);
	}

	/** Accept or reject a request based on available authorisation information
	 *
	 * Returns:
	 *
	 * null if the project does not require authentication
	 * true if authentication was successful
	 * false if authentication failed
	 */
	authorise (req) {
		if (!this.isAuthenticationRequired()) {
			// This endpoint does not require authentication
			return true;
		}

		if (this.username && this.password) {
			// Attempt HTTP Basic Authentication
			const authorization = req.get("Authorization")
			if (authorization) {
				const parts = authorization.split(" ");
				if (parts && /^Basic$/i.test(parts[0])) {
					const credentials = Buffer.from(parts[1], "base64").toString().split(":")
					if (credentials) {
						return (credentials[0] == this.username && credentials[1] == this.password);
					}
				}
			}
			// No successful authorisation,
			// allow fallback to token-based auth
		}

		if (this.tokenName && this.tokenValue) {
			// Check if the tokenName is well-known
			if (this.tokenName.toLowerCase() == "stripe-signature") {
				// We might be dealing with a Stripe signature
				// (https://stripe.com/docs/webhooks/signatures).
				// In this case, tokenValue should be the endpoint
				// secret.
				return verifyStripeSignature(req, this.tokenValue);
			} else if (this.tokenName.toLowerCase() == "x-gitea-signature") {
				// This might be a Gitea signature
				// (https://docs.gitea.io/en-us/webhooks/)
				// In this case, tokenValue should be the endpoint
				// secret.
				return verifyGiteaSignature(req, this.tokenValue, req.get("X-Gitea-Signature"));
			} else if (this.tokenName.toLowerCase() == "x-nextcloud-webhooks") {
				// This might be a Nextcloud flow webhooks signature
				// (https://github.com/kffl/nextcloud-webhooks#authenticating-requests)
				// In this case, tokenValue should be the endpoint
				// secret.
				return verifyNextcloudSignature(req, this.tokenValue, req.get("X-Nextcloud-Webhooks"));
			}

			// Attempt token-based authentication, e.g.:
			// X-Some-Token: somesecret
			const token = req.get(this.tokenName) || null;
			return (token == this.tokenValue);
		}

		// No successful authorisation
		return false;
	}

	toJSON () {
		return {
			owner: this.owner,
			jid: this.jid,
			name: this.name,
			url: this.url,
			email: this.email,
			description: this.description,
			notifications: this.notifications,
			autojoin: this.autojoin,
			nick: this.nick,
			username: this.username,
			password: this.password,
			tokenName: this.tokenName,
			tokenValue: this.tokenValue,
			allowed: this.allowed,
			uuid: this.uuid,
			lastActivity: this.lastActivity,
			notificationsQueue: this.notificationsQueue.map(i => serialize(i)),
			nodes: this.nodes.map(n => n.toJSON())
		}
	}
}

module.exports = Project;
