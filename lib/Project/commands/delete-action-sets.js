const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const ActionSets = require('~/lib/ActionSets');
const ActionSet = require('~/lib/ActionSet');
const {getSender, getComponent} = require('~/lib/utils/component');


function getActionSets (self, ctx, id) {

	const actionSetsNode = self.parent.nodes.find(n => n.node == ActionSets.node);
	if (actionSetsNode) {
		const actionSets = actionSetsNode.nodes.filter(n => n instanceof ActionSet);

		if (actionSets && id) {
			return actionSets.find(n => n.id == id);
		}

		return actionSets;
	}

	return [];
}

function select (self, ctx, session) {

	const actionSets = getActionSets(self, ctx);

	if (!actionSets?.length) {
		return self.complete(session, "There are no action sets to delete");
	}

	const fields = Object.fromEntries(actionSets.map(i => [
		i.node, {
			type: "boolean",
			label: `${i.name} [Prefix: ${i.prefix}]`,
			desc: i.description
		}
	]));

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('next', {})
		),
		XMPPForm.form({
			title: "Delete action sets",
			instructions: `Select the action sets to delete.\nWARNING! This action is irreversible.\nAll configuration for the deleted actions will be lost.`,
			fields
		})
	);
}

function execute (self, ctx, session) {
	const sender = getSender(ctx);

	if (sender != self.parent.owner) {
		// Not authorised
		return xmppError(ctx.stanza, "forbidden");
	}

	const actionSets = getActionSets(self, ctx);
	const newSet = actionSets.filter(i => !session.data.form[i.node]);

	const actionSetsNode = self.parent.nodes.find(n => n.node == ActionSets.node);
	if (actionSetsNode) {
		actionSetsNode.nodes = newSet;
		self.commit(); // Save state
	}

	return self.complete(session, `Action sets deleted:\n${actionSets.filter(i => session.data.form[i.node]).map(i => `• ${i.name} (${i.id}${i.prefix})`).join("\n")}`);
}

class DeleteActionSetsCommand extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "https://navlost.eu/xmpp/components/webhooks/actions/sets#delete";
		if (!settings.name) settings.name = "Delete action sets";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			select,
			execute
		];

	}
}

module.exports = DeleteActionSetsCommand;

