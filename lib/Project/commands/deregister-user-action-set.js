const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const ActionSets = require('~/lib/ActionSets');
const ActionSet = require('~/lib/ActionSet');
const {getSender, getComponent} = require('~/lib/utils/component');

function getActionSets (self, ctx, node) {

	const actionSetsNode = self.parent.nodes.find(n => n.node == ActionSets.node);
	if (actionSetsNode) {
		// TODO Filter actionSets by access rights
		const actionSets = actionSetsNode.nodes.filter(n => n instanceof ActionSet && n.isUserRegistered(ctx));

		if (actionSets && node) {
			return actionSets.find(n => n.node == node);
		}

		return actionSets;
	}

	return [];
}


function selectActionSet (self, ctx, session) {

	const actionSets = getActionSets(self, ctx);
	const fields = Object.fromEntries(actionSets.map(i => [ i.node, {
		type: "boolean",
		label: i.name,
		desc: i.description
	}]));

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('next', {})
		),
		XMPPForm.form({
			title: "Select action set",
			instructions: `Select actions sets to unregister from`,
			fields
		})
	);
}

function saveRegistration (self, ctx, session) {
// 	console.log("session", session.data.form);

	const actionSets = getActionSets(self, ctx) ?? {};
	const selectedActionSets = actionSets.filter(i =>  session.data.form[i.node]);
// 	console.log("actionSets", actionSets.map(i => {return {id: i.id, name: i.name, users: i.users};}));
// 	console.log("selectedActionSets", selectedActionSets.map(i => ({id: i.id, name: i.name, users: i.users})));

	for (const actionSet of selectedActionSets) {
		actionSet.removeUser(ctx);
		self.commit(); // Save state
	}

	const msg = `Unregistered from:\n${selectedActionSets.map(i => `  • ${i.name} (${i.id}${i.prefix})`).join("\n")}`;

	return self.complete(session, msg);
}

class WebhookComponentCommandUnregisterUserActionSet extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "http://navlost.eu/xmpp/components/webhooks/actions/sets#unregister";
		if (!settings.name) settings.name = "Unregister from an action set";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			selectActionSet,
			saveRegistration
		];
	}

	isUserAllowed (ctx) {
		// Anyone that can access this node can run the command.
		// This doesn't mean that they will get to register –
		// that depends on the configuration of the specific action
		// set they choose.
		return true;
	}
}

module.exports = WebhookComponentCommandUnregisterUserActionSet;

