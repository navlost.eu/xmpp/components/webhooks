const {xml, jid} = require('@xmpp/component')
const { clone } = require('~/lib/utils');
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const ActionSetTemplate = require('~/lib/ActionSetTemplate');
const ActionSetTemplates = require('~/lib/ActionSetTemplates');
const ActionSets = require('~/lib/ActionSets');
const ActionSet = require('~/lib/ActionSet');
const {getSender, getComponent} = require('~/lib/utils/component');


function getActionSetTemplates (self, templateId) {
	const templates = self.parent.parent.nodes.find(n => n.node == ActionSetTemplates.node);

	if (templates && templateId) {
		return templates.nodes.find(n => n.id == templateId);
	}

	return templates?.nodes ?? [];
}

function selectTemplate (self, ctx, session) {

	const actionSetTemplates = getActionSetTemplates(self).map(t => {
		return {
			label: t.name,
			value: t.id
		}
	});

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('next', {})
		),
		XMPPForm.form({
			title: "Add actions",
			instructions: `Select an actions template and proceed to the next dialogue to configure it for your endpoint`,
			fields: {
				name: {
					label: "Actions set name",
					desc: "The name used to distinguish this set from others.",
					required: true
				},
				templateId: {
					type: "list-single",
					label: "Actions template",
					options: actionSetTemplates,
					required: true
				},
				prefix: {
					label: "Actions set prefix",
					desc: "Prefix to use when invoking the action from a message stanza.",
					required: true
				}
			}
		})
	);
}

function configureActionSet (self, ctx, session) {

	const actionSet = clone(getActionSetTemplates(self, session.data.form.templateId) ?? {});
	session.data.actionSet = actionSet;

	// Check if this actionSet has already been added to this endpoint. If that is the
	// case, we prefill the variable values in the configuration form.
	const actionSetsNode = self.parent.nodes.find(n => n.node == ActionSets.node);
	if (actionSetsNode) {
		const existingActionSet = actionSetsNode.nodes.find(n => n.kind == ActionSet.node && n.id == actionSet.id && n.prefix == session.data.form.prefix)

		if (existingActionSet) {
			session.data.existingActionSet = existingActionSet;

			// Rescue the variables and user registrations (which might be invalid if the schema has changed)

			if (actionSet.variables) {
				const currentVariables = existingActionSet.variables ?? {};

				for (const key of Object.keys(actionSet.variables)) {
					if (Object.hasOwn(currentVariables, key)) {
						actionSet.variables[key].value = currentVariables[key];
					}
				}
			}

			if (existingActionSet.users) {
				actionSet.users = existingActionSet.users;
			}
		}
	}

	// Append a `$` at the beginning of each variable definition, so we can later distinguish
	// it from other form values.
	const variables = Object.fromEntries(Object.entries(actionSet?.variables??{}).map(i => [ "$"+i[0], i[1] ]));


	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('prev', {}),
			xml('next', {})
		),
		XMPPForm.form({
			title: "Configure variables",
			instructions: `Set the values that correspond to your endpoint.`,
			fields: {
				...variables,
			}
		})
	);
}

function configureAccess (self, ctx, session) {
	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('prev', {}),
			xml('next', {})
		),
		XMPPForm.form({
			title: "Configure access",
			instructions: `You can restrict access to a subset of users. In order to use actions you need to be pass both the endpoint and the actions authorisation restrictions.`,
			fields: {
				allowed: {
					label: "Allowed subscribers",
					desc: "Only JIDs matching this regular expression will be allowed to subscribe to this node. Leave the default to allow all users of this endpoint to subscribe.",
					value: session.data.existingActionSet?.allowed?.source ?? "^.*$"
				},
				allowMUC: {
					type: "boolean",
					label: "Allow MUC commands",
					desc: "Allow chatroom participants to send commands?",
					value: session.data.existingActionSet?.allowMUC ?? false
				}
			}
		})
	);
}

function saveActionSet (self, ctx, session) {
	const addressee = ctx.to.bare().toString();
	const sender = getSender(ctx);

	// Only the owner of an endpoint can add actions to it.
	// TODO Maybe the component administrator also could?
	if (sender != self.parent.owner) {
		// Not authorised
		return xmppError(ctx.stanza, "forbidden");
	}

	// We build an object suitable for passing to an ActionSet constructor.
	// FIXME This of course screams of refactoring, but hey.
	const actionSet = session.data.actionSet;
	const definition = {
		variables: {}
	};
	for (const key in session.data.form) {
		const value = session.data.form[key];
		if (key.startsWith("$")) {
			definition.variables[key.slice(1)] = value;
		} else if (key == "templateId") {
			definition.id = value
		} else {
			definition[key] = value;
		}
	}
	// NOTE We do not allow editing commands so it's OK to use direct assignment
	// as opposed to cloning.
	definition.commands = actionSet.commands;
	definition.aliases = actionSet.aliases;
	definition.registration = actionSet.registration;
	definition.version = actionSet.version;
	definition.users = actionSet.users; // Try to preserve user registrations

	const parentNode = self.parent.nodes.find(n => n.node == ActionSets.node);
	if (parentNode) {
		try {
			parentNode.loadNodes([definition]);
			self.commit(); // Save state
		} catch (err) {
			console.error(err);
			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				xml(
					'actions',
					{ execute: "prev" },
					xml('prev'),
					xml('cancel')
				),
				xml(
					'note',
					{ type: "error" },
					// We really want to be more explicit than this.
					`The actions set has invalid definitions.`
				)
			);
		}
	} else {
		// TODO Can we have components not supporting actions? Why not
		return xmppError(ctx.stanza, "item-not-found");
	}

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'completed'
		},
		// We add this because of a bug in Psi+
		// (https://github.com/psi-im/psi/issues/340)
		// but not sure if I would like to keep both the
		// form and the note. TODO check on other clients.
		XMPPForm.form({
			title: "Command results",
			instructions: "Actions added."
		}),
		xml(
			'note',
			{ type: "info" },
			"Actions added."
		)
	);

}

class WebhookComponentCommandAddActions extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "http://navlost.eu/xmpp/components/webhooks/actions/sets#add";
		if (!settings.name) settings.name = "Add action sets";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			selectTemplate,
			configureActionSet,
			configureAccess,
			saveActionSet
		];
	}
}

module.exports = WebhookComponentCommandAddActions;

