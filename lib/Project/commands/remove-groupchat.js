const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');

function removeGroupchatForm (self, ctx, session) {

	const addressee = ctx.to.bare().toString();
	const options = self.parent.parent.subscriptions.filter( s =>
		s.type == "groupchat" && s.to == addressee
	)
	.map(i => {
		return {
			label: jid(i.from).setResource(i.nick).toString(),
			value: JSON.stringify(i)
		}
	});

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "complete" },
			xml('complete', {})
		),
		XMPPForm.form({
			title: "Remove groupchat",
			instructions: `The selected groupchat subscription will be cancelled. The room will no longer receive notifications from ${ctx.to}.`,
			fields: {
				groupchatJID: {
					type: "list-single",
					label: "Groupchat subscription",
					desc: "The subscription to remove",
					options,
					required: true
				}
			}
		})
	);
}

async function removeGroupchat (self, ctx, session) {
	const addressee = ctx.to.bare().toString();
	const sender = ctx.from.bare().toString();

	const cmd = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
	if (!cmd) {
		return xmppError(ctx.stanza, "bad-request");
	}
	const x = cmd.getChild("x", "jabber:x:data");
	if (x && x.attrs.type == "submit") {
		const form = XMPPForm.submit(x);
		session.data.form = form;
		if (form.groupchatJID) {
			form.groupchatJID = JSON.parse(form.groupchatJID);
			const item = self.parent.parent.items.find(i => i.jid == addressee);
			if (item && item.owner == sender) {

				const subscription = self.parent.parent.subscriptions.find(s =>
					Object.keys(form.groupchatJID).every(k => s[k] == form.groupchatJID[k])
				);
				if (subscription) {

					await self.parent.parent.removeSubscription(subscription);

					// Remove ourselves from the groupchat
					self.parent.parent.xmpp.send(xml('presence',
						{
							from: addressee,
							to: `${subscription.from}/${subscription.nick}`,
							type: "unavailable"
						}
					));

					return xml(
						'command', {
							xmlns: 'http://jabber.org/protocol/commands',
							node: self.info.node,
							sessionid: session.id,
							status: 'completed'
						},
						// TODO Offer "prev" action
						xml(
							'note',
							{ type: "info" },
							`${form.groupchatJID} has been subscribed.`
						)
					);

				}
			} else if (!item) {
				return xmppError(ctx.stanza, "item-not-found");
			} else {
				return xmppError(ctx.stanza, "not-authorized");
			}
		} else {
			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				// TODO Offer "prev" action
				xml(
					'note',
					{ type: "info" },
					"Command aborted."
				)
			);
		}
	} else {
		return xmppError(ctx.stanza, "bad-request");
	}
}

class WebhookComponentCommandRemoveGroupchat extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "groupchat-remove";
		if (!settings.name) settings.name = "Remove groupchat subscription";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			removeGroupchatForm,
			removeGroupchat
		];
	}

	discoItem (ctx) {
		if (this.parent.parent.registeredUser(ctx)) {
			return super.discoItem(ctx);
		}
	}

}

module.exports = WebhookComponentCommandRemoveGroupchat;

