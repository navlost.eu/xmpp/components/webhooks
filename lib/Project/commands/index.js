
module.exports = [
// 	require('./edit'),
	require('./remove'),
	require('./add-groupchat'),
	require('./remove-groupchat'),
	require('./add-actions'),
	require('./delete-action-sets'),
	require('./register-user-action-set'),
	require('./deregister-user-action-set')
]
