const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const ActionSets = require('~/lib/ActionSets');
const ActionSet = require('~/lib/ActionSet');
const {getSender, getComponent} = require('~/lib/utils/component');


function getActionSets (self, ctx, node) {

	const actionSetsNode = self.parent.nodes.find(n => n.node == ActionSets.node);
	if (actionSetsNode) {
		const actionSets = actionSetsNode.nodes.filter(n => n instanceof ActionSet && n.isUserAllowed(ctx));

		if (actionSets && node) {
			return actionSets.find(n => n.node == node);
		}

		return actionSets;
	}

	return [];
}


function selectActionSet (self, ctx, session) {

	const actionSets = getActionSets(self, ctx).map(t => {
		return {
			label: t.name,
			value: t.node
		}
	});

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('next', {})
		),
		XMPPForm.form({
			title: "Select action set",
			instructions: `Select an action set to register with`,
			fields: {
				node: {
					type: "list-single",
					label: "Action set",
					options: actionSets,
					required: true
				}
			}
		})
	);
}

function configureVariables (self, ctx, session) {

	const actionSet = getActionSets(self, ctx, session.data.form.node) ?? {};
	const form = JSON.parse(JSON.stringify(actionSet?.registration?.variables??{}));
// 	console.log(JSON.stringify({id: actionSet.id, name: actionSet.name, registration: actionSet?.registration, users: actionSet.users}, null, 4))

	// If the user was already registered, append the values that he has
	// previously supplied.
	if (actionSet.isUserRegistered(ctx)) {
// 		console.log("isUserRegistered says true");
		const values = actionSet.getUser(ctx);
		if (values) {
			for (const key in form) {
				form[key].value = values[key];
			}
		}
// 		console.log("values", values);
	}
// 	console.log("Form", JSON.stringify(form, null, 4));

	// Append a `$` at the beginning of each variable definition, so we can later distinguish
	// it from other form values.
	const variables = Object.fromEntries(Object.entries(form).map(i => [ "$"+i[0], i[1] ]));
// 	console.log("Variables", variables);


	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('prev', {}),
			xml('next', {})
		),
		XMPPForm.form({
			title: "Configure variables",
// 			instructions: `Set the values that you will be using with this endpoint.`,
			fields: {
				...variables,
			}
		})
	);
}

function saveRegistration (self, ctx, session) {

	const actionSet = getActionSets(self, ctx, session.data.form.node) ?? {};
	const variables = {};
	for (const key in session.data.form) {
		const value = session.data.form[key];
		if (key.startsWith("$")) {
			variables[key.slice(1)] = value;
		}
	}

	const result = actionSet.registerUser(ctx, variables, {overwrite:true});
	if (result === true) {
		self.commit(); // Save state
		return self.complete(session, "Registration successful");
	} else {
		return self.complete(session, `Registration failed: ${result}`, "error");
	}

}

class WebhookComponentCommandRegisterUserActionSet extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "http://navlost.eu/xmpp/components/webhooks/actions/sets#register";
		if (!settings.name) settings.name = "Register with an action set";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			selectActionSet,
			configureVariables,
			saveRegistration
		];
	}

	isUserAllowed (ctx) {
		// Anyone that can access this node can run the command.
		// This doesn't mean that they will get to register –
		// that depends on the configuration of the specific action
		// set they choose.
		return true;
	}
}

module.exports = WebhookComponentCommandRegisterUserActionSet;

