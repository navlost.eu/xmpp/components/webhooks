# Project – Representation of an entity that can be subscribed to and issues notifications

The `Project` class defines entities that when instantiated become items in an XMPP component. These entities have the following properties:

Property        | Description
----------------|------------
`parent`        | A [`GenericComponent`](../GenericComponent/README.md) or derived class instance that a `Project` item is associated with.
`owner`         | JID of the user who created this entity and therefore has administrative privileges such as being able to edit or delete it.
`jid`           | This entity's JID.
`name`          | A descriptive name for this entity (used in service discovery)
`url`           | An URL associated with this entity (e.g., a homepage)
`email`         | An email associated with this entity.
`notifications` | *Not used.* ~~Whether messages sent to this entity are sent as webhook responses.~~
`username`      | *Not used.* ~~HTTP Basic Authentication username.~~
`password`      | *Not used.* ~~HTTP Basic Authentication password.~~
`allowed`       | If not empty, a regular expression that must be matched by a candidate subscriber JID in order for the subscription to be accepted. For instance `^(.*@example.net¦bloggs@example.com)$` allows subscriptons from everyone at `example.net` or the JID `bloggs@example.com`.
`uuid`          | A UUID associated with each instance.

## Methods

The class provides methods associated with service discovery `discoInfo()` and `discoItem()` as well as a custom JSON serialiser.

## XMPP Commands

There are ad hoc commands to remove an instance from its parent and to manage group chat subscriptions. These commands are only available to the `owner` JID.

Command            | Description
-------------------|------------
`remove`           | Remove this item from its parent. Effectively it deletes the JID.
`groupchat-add`    | Add the JID of a group chat. Notifications will be sent to this JID with a message type of `groupchat`.
`groupchat-remove` | Remove a previously added group chat JID.

