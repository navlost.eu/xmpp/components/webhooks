const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');
const logger = require('~/lib/logger');

async function presenceUnavailable (stanza) {
	if (stanza.is('presence') && stanza.attrs.type == "unavailable") {
		const sender = jid(stanza.attrs.from).bare().toString();
		const addressee = jid(stanza.attrs.to).bare().toString();

		const mucUser = stanza.getChild("x", "http://jabber.org/protocol/muc#user");
		if (mucUser) {
			const mucItems = mucUser.getChildren("item");
			for (const mucItem of mucItems) {
				if (mucItem.attrs.role == "none") {
					// We've been kicked or the room has been destroyed
					const subscription = this.subscriptions.find(i => i.from == sender && i.to == addressee);
					this.removeSubscription(subscription);

					const msg = `${addressee} has been unsubscribed from ${sender}.\nReason: ${mucItem.getChild("reason")?.text() || "N/A"}.\nActor: ${mucItem.getChild("actor")?.attrs?.nick || "N/A"}.\nNo more messages will be sent to ${sender}.`;

					logger.info("🦶", msg);

					const item = this.items.find(i => i.jid == addressee);
					if (item) {
						await this.notify(msg, item);
					}
				}
			}
		}
	}
}

module.exports = presenceUnavailable;
