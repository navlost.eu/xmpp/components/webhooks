const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

async function presenceUnsubscribe (stanza) {
	if (stanza.is('presence') && stanza.attrs.type == "unsubscribed") {
		const sender = jid(stanza.attrs.from).bare().toString();
		const addressee = jid(stanza.attrs.to).bare().toString();

		const subscription = this.subscriptions.find(i => i.from == sender && i.to == addressee);
		this.removeSubscription(subscription);
		await this.xmpp.send(xml('presence', {
			from: addressee,
			to: sender,
			type: 'unsubscribed'
		}));
		await this.xmpp.send(xml('presence', {
			from: addressee,
			to: sender,
			type: 'unavailable'
		}));
	}
}

module.exports = presenceUnsubscribe;
