const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

async function presenceSubscribe (stanza) {
	if (stanza.is('presence') && stanza.attrs.type == "probe") {
		const sender = jid(stanza.attrs.from).bare().toString();
		const addressee = jid(stanza.attrs.to).bare().toString();
		const item = this.items.find(i => i.jid == addressee);
		if (item) {
			// The item exists…
			if (this.subscriptions.find(i => i.from == sender && i.to == addressee)) {
				// …and the user is already subscribed to it
				const presence = xml('presence', {
					from: addressee,
					to: sender,
				});
// 				presence.append(await this.caps({stanza}));
				await this.xmpp.send(presence);
			}
		} else {
			// This item is gone, so remove the subscription
			await this.removeSubscription({from: addressee, to: sender});
			await this.xmpp.send(xml('presence', {
				from: addressee,
				to: sender,
				type: 'unsubscribed'
			}));

			await this.xmpp.send(xmppError(stanza, "item-not-found"));
		}
	}
}

module.exports = presenceSubscribe;
