const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

/** Handle a chatroom invitation message.
 *
 * If the chatroom is known to us, we accept,
 * otherwise we decline the invitation.
 */
async function messageInvite (stanza) {
	if (stanza.is('message')) {
		const x = stanza.getChild("x", "http://jabber.org/protocol/muc#user");
		if (x) {
			const invite = x.getChild("invite");
			if (invite) {
				const sender = jid(stanza.attrs.from).bare().toString();
				const addressee = jid(stanza.attrs.to).bare().toString();
				const item = this.items.find(i => i.jid == addressee);
				if (item) {

					const groupchat = this.subscriptions.find( i => i.type == "groupchat" && i.from == sender );

					const autojoin = item.autojoin &&
						this.userAllowed(stanza) && { nick: (item.nick || jid(item.jid).local) };

					if (autojoin) {
						// We've got an invitation from an authorised sender. Let's add the MUC
						// to the endpoint's list of subscribers.
						await this.addSubscription({
							from: sender,
							to: addressee,
							type: "groupchat",
							nick: autojoin.nick
						});

						// Let the endpoint owner know that it has been invited into a chatroom.
						const inviter = invite.attrs.from;
						const inviteReason = invite.getChild("reason");
						this.notify(`${item.jid} has accepted an invitation from ${inviter} to join the chatroom ${sender}.\nReason: ${ inviteReason ?? "N/A" }`, item);
					}

					if (groupchat || autojoin) {
						// The groupchat is known to us or we have been configure to accept all invitations and this one comes from an authorised sender, so accept the invitation.
						const roomnick = jid(sender);
						roomnick.setResource(groupchat?.nick ?? autojoin.nick);
						await this.xmpp.send(xml('presence',
							{
								from: addressee,
								to: roomnick
							},
							xml("x", "http://jabber.org/protocol/muc"),
						));
					} else {
						// We haven't heard of this groupchat, so we ignore the invitation,
						// although we could also send a message to the endpoint owner to
						// let him know about the request, for instance.
					}
				} else {
					await this.xmpp.send(xmppError(stanza, "item-not-found"));
				}
			}
		}
	}
}

module.exports = messageInvite;
