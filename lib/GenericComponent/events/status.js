const logger = require('~/lib/logger');

module.exports = async function (status) {
	logger.debug('ℹ', 'status', status)
}
