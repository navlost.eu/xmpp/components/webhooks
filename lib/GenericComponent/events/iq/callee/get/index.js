
module.exports = [
	require('./disco-info'),
	require('./disco-items'),
	require('./vcard-temp'),
	require('./last'),
	require('./ping'),
	require('./version'),
	require('./register')
]
