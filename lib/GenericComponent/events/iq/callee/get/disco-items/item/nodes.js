
// Send list of item children

module.exports = function (ctx, res) {
	if (ctx.to && ctx.to.local && ctx.element.attrs.node) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		const node = ctx.element.attrs.node;

// 		console.log("DISCO#ITEMS (NODE) sender, addresse, node", sender, addresse, node);

		function findRecursive (parent) {
// 			console.log("Recursing into", parent.jid, parent.node);

			if (parent.jid == addresse && parent.node == node) {
// 				console.log("FOUND", parent);

				for (const child of parent.items ?? []) {
					if (child.discoItem) {
						res.append(child.discoItem(ctx));
					}
				}

				for (const child of parent.nodes ?? []) {
					if (child.discoItem) {
						res.append(child.discoItem(ctx));
					}
				}
			} else {
				if (parent.items) {
					for (const child of parent.items) {
						if (findRecursive(child)) {
							return res;
						}
					}
				}

				if (parent.nodes) {
					for (const child of parent.nodes) {
						if (findRecursive(child)) {
							return res;
						}
					}
				}
			}
		}

		if (node == "http://jabber.org/protocol/commands") {
			// At present and in theory this is taken care of by './commands.js', though
			// there is scope for refactoring.
			return res;
		}

		findRecursive(this);
	}

	return res;
}
