
// Send list of item children

module.exports = function (ctx, res) {
	if (ctx.to && ctx.to.local && !ctx.element.attrs.node) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();

// 		console.log("DISCO#ITEMS (ITEM) sender, addresse, node", sender, addresse);

		const item = this.items.find(i => i.jid == addresse);
		if (item) {
			for (const child of item.items ?? []) {
				if (child.discoItem) {
					res.append(child.discoItem(ctx));
				}
			}

			for (const child of item.nodes ?? []) {
				if (child.discoItem) {
					res.append(child.discoItem(ctx));
				}
			}
		}
	}

	return res;
}
