
// Send list of component nodes

module.exports = function (ctx, res) {
	if (ctx.to && !ctx.to.local && !ctx.element.attrs.node) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();

		for (const item of this.items) {
			if (item.discoItem) {
				res.append(item.discoItem(ctx));
			}
		}

		for (const item of this.nodes) {
			if (item.discoItem) {
				res.append(item.discoItem(ctx));
			}
		}

	}

	return res;
}
