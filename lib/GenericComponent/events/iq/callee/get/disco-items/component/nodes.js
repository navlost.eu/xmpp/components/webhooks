
// Send list of component items matching a node attribute

module.exports = function (ctx, res) {
	if (ctx.to && !ctx.to.local && ctx.element.attrs.node) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		const node = ctx.element.attrs.node;

		const branch = this.nodes.find(p => p.jid == addresse && p.node == node);

		for (const item of branch?.items ?? []) {
			if (item.discoItem) {
				res.append(item.discoItem(ctx));
			}
		}

		for (const item of branch?.nodes ?? []) {
			if (item.discoItem) {
				res.append(item.discoItem(ctx));
			}
		}

	}

	return res;
}
