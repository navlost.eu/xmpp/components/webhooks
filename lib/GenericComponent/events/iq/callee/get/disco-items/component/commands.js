
// Send list of component commands

const node = "http://jabber.org/protocol/commands";
module.exports = function (ctx, res) {
	if (ctx.to && !ctx.to.local && ctx.element.attrs.node == node) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();

		for (const command of this.commands) {
			if (command.discoItem) {
				res.append(command.discoItem(ctx));
			}
		}

	}

	return res;
}
