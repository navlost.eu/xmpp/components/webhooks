const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

// Send an item's last activity, if the item supports it

module.exports = function (ctx, res) {
	if (ctx.to && ctx.to.local) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		const item = this.items.find(p => p.jid == addresse);

		if (!item) {
			return xmppError(ctx.stanza, "item-not-found");
		} else if (ctx.to && item.lastActivity) {
			return xml("query", {
				xmlns: "jabber:iq:last",
				seconds: Math.floor(Date.now()/1000 - item.lastActivity)
			});
		}

	}

	return res;
}
