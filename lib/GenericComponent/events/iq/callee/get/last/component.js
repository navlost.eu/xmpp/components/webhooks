const {xml, jid} = require('@xmpp/component')

// Send the component's uptime

module.exports = function (ctx, res) {
	if (ctx.to && !ctx.to.local) {

		return xml("query", {
			xmlns: "jabber:iq:last",
			seconds: Math.floor(process.uptime())
		});

	}

	return res;
}
