const {xml, jid} = require('@xmpp/component')

module.exports = {
	namespace: "jabber:iq:register",
	tag: "query",
	handler: async function(ctx) {

		if (!ctx.to.local && this.register) {
			return this.register(ctx);
		}
	}
}
