const {xml, jid} = require('@xmpp/component');
const {name, version} = require('~/lib/version');
const xmppError = require('~/lib/xmpp-error');

function recurseOver (ctx, fn) {

	const sender = ctx.from.bare().toString();
	const addresse = ctx.to.bare().toString();
	const node = ctx.element.attrs.node;

	fn = fn.bind(this);

	return function findRecursive (parent) {
// 		console.log("Recursing into", parent.jid, parent.node, addresse, node);

		if (parent.jid == addresse && parent.node == node) {
			const result = fn(parent);
			if (result) {
				return result;
			}
		}

		if (parent.items) {
			for (const child of parent.items) {
				const result = findRecursive(child);
				if (result) {
					return result;
				}
			}
		}

		if (parent.nodes) {
			for (const child of parent.nodes) {
				const result = findRecursive(child);
				if (result) {
					return result;
				}
			}
		}
	}
}


module.exports = {
	namespace: "jabber:iq:version",
	tag: "query",
	handler: async function(ctx) {

		if (!ctx.to.local) {
			return xml("query", "jabber:iq:version",
				xml("name", {}, name()),
				xml("version", {}, await version())
			);
		} else {
// 			console.log("DISCO#VERSION");
			const fn = (parent) => {
				if (parent.version) {
					const name = parent.name
						? typeof parent.name === "function"
							? parent.name()
							: parent.name
						: "";
					const version = parent.version
						? typeof parent.version === "function"
							? parent.version()
							: parent.version
						: undefined;

					return xml("query", "jabber:iq:version",
						xml("name", {}, name),
						xml("version", {}, version)
					);
				}
			};
			return recurseOver(ctx, fn)(this) ?? xmppError(ctx.stanza, "item-not-found");
		}
	}
}
