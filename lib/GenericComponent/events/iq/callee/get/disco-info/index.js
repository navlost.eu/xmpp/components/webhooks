const {xml, jid} = require('@xmpp/component')
const {getStanza} = require('~/lib/xml-utils');

const handlers = [
	require('./component'),
	require('./item'),
	require('./node')
];

module.exports = {
	namespace: "http://jabber.org/protocol/disco#info",
	tag: "query",
	handler: async function (ctx) {

		function appendNode (res) {
			// For capabilities discovery queries
			// NOTE: Yes we're just reflecting the node attribute
			// back, even though it might no longer be correct.
			res.attrs.node = getStanza(ctx).getChild("query")?.attrs?.node;
		}

		let res;

		for (const handler of handlers) {
			res = await handler.call(this, ctx, res);
			if (res) {
				appendNode(res);
				break;
			}
		}

		return res;
	}
}
