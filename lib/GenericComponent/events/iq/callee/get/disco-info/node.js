const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

// Send information about component nodes (items with a `node` attribute)

module.exports = function (ctx, res) {
	if (ctx.to && ctx.element.attrs.node) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		const node = ctx.element.attrs.node;

		// The idea here is that a matching node may not be a direct child of the
		// component but a more distant descendant, so we search recursively and
		// return the first matching item.

		function findRecursive (parent) {
// 			console.log("Recursing into", parent.jid, parent.node);

			if (parent.jid == addresse && parent.node == node) {
				if (parent.discoInfo) {
					return parent.discoInfo(ctx);
				}
			}

			if (parent.items) {
				for (const child of parent.items) {
					const discoInfo = findRecursive(child);
					if (discoInfo) {
						return discoInfo;
					}
				}
			}

			if (parent.nodes) {
				for (const child of parent.nodes) {
					const discoInfo = findRecursive(child);
					if (discoInfo) {
						return discoInfo;
					}
				}
			}
		}

// 		console.log("DISCO#INFO (NODE)");
// 		console.log("sender, addressee, node", sender, addresse, node);

		return findRecursive(this) ?? xmppError(ctx.stanza, "item-not-found");

	}

	return res;
}
