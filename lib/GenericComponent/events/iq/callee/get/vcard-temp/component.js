const {xml, jid} = require('@xmpp/component')

// Send the component's own vCard

module.exports = function (ctx, res) {
	if (ctx.to && !ctx.to.local && this.vCardTemp) {

		return this.vCardTemp(ctx);

	}

	return res;
}
