const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

// Send an item's vCard, if the item supports it

module.exports = function (ctx, res) {
	if (ctx.to && ctx.to.local) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		const item = this.items.find(p => p.jid == addresse);

		if (!item) {
			return xmppError(ctx.stanza, "item-not-found");
		} else if (item.vCardTemp) {
			return item.vCardTemp(ctx);
		}

	}

	return res;
}
