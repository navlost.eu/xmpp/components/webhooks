const {xml, jid} = require('@xmpp/component')

// Send the component's uptime

module.exports = function (ctx, res) {
	if (ctx.to && !ctx.to.local) {

		// See commit 315fd300 as to why we return true
		return true;

	}

	return res;
}
