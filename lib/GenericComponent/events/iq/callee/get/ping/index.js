const {xml, jid} = require('@xmpp/component')

const handlers = [
	require('./component'),
	require('./item')
];

module.exports = {
	namespace: "urn:xmpp:ping",
	tag: "ping",
	handler: async function (ctx) {

		let res;

		for (const handler of handlers) {
			res = await handler.call(this, ctx, res);
			if (res) {
				break;
			}
		}

		return res;
	}
}
