const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');

// Get a node command

module.exports = function (ctx, res) {
	if (ctx.to && ctx.element.attrs.node) {

		const sender = ctx.from.bare().toString();
		const addresse = ctx.to.bare().toString();
		const node = ctx.element.attrs.node;

		// The idea here is that a matching node may not be a direct child of the
		// component but a more distant descendant, so we search recursively and
		// return the first matching item.

		function findRecursive (parent) {
// 			console.log("Recursing into", parent.jid, parent.node);

			if (parent.jid == addresse && parent.node == node) {
				if (parent.command) {
					return parent.command.despatch(ctx);
				}
			}

			if (parent.items) {
				for (const child of parent.items) {
					const returnValue = findRecursive(child);
					if (returnValue) {
						return returnValue;
					}
				}
			}

			if (parent.nodes) {
				for (const child of parent.nodes) {
					const returnValue = findRecursive(child);
					if (returnValue) {
						return returnValue;
					}
				}
			}
		}

// 		console.log("DISCO#COMMAND (NODE)");
// 		console.log("sender, addressee, node", sender, addresse, node);

		return findRecursive(this) ?? xmppError(ctx.stanza, "item-not-found");

	}

	return res;
}
