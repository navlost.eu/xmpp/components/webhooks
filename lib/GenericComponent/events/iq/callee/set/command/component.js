const {xml, jid} = require('@xmpp/component')

// Process component commands

module.exports = function (ctx, res) {
	if (ctx.to && !ctx.to.local) {

		const commandElement = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
		const command = this.commands.find(i => i.info.node == commandElement.attrs.node);
		if (command) {
			return command.despatch(ctx);
		}

	}

	return res;
}
