const {xml, jid} = require('@xmpp/component')

const handlers = [
	require('./component'),
	require('./item'),
	require('./node')
];

module.exports = {
	namespace: "http://jabber.org/protocol/commands",
	tag: "command",
	handler: async function (ctx) {

		let res;
		for (const handler of handlers) {
			res = await handler.call(this, ctx, res);
			if (res) {
				break;
			}
		}

		return res;
	}
}
