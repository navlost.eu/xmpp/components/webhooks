const handlers = require('./stanzas');
const logger = require('~/lib/logger');

module.exports = async function (stanza) {
	logger.debug('ℹ', 'stanza', stanza)

	for (const handler of handlers) {
		await handler.call(this, stanza);
	}

}
