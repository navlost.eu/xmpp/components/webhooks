const logger = require('~/lib/logger');

module.exports = (output) => {
	logger.info('📤', logger.c.yellow(output))
}
