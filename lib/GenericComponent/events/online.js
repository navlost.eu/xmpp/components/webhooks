const {xml, jid} = require('@xmpp/component')
const logger = require('~/lib/logger');

module.exports = async function (address) {
	logger.debug('▶', logger.c.green('online as ' + address.toString()))

	// Run a server discovery query to see what capabilities
	// we can use.
	await this.discover();

	// Try to find useful services such as PubSub (so we can read
	// our own configuration) and HTTP upload, in case those
	// have not been given in settings.yaml.
	await this.autoconfigure();

	// Load the endpoint configuration.
	await this.readPersisted()

	// Tell everyone we're online
	this.presence();
	// Join chatrooms we're members of
	this.joinMUC()

	// TODO
	// Deliver messages that might have been added to the incoming
	// database but not yet sent to recipients
}
