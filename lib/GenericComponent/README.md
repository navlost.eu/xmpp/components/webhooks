# GenericComponent – A bare-bones XMPP component

This offers a basic implementation of an [XMPP component](https://xmpp.org/extensions/xep-0114.html "XEP-0114: Jabber Component Protocol") upon which to build a more functional service.

## Instantiation

```javascript
const GenericComponent = require('GenericComponent');

const settings = {
  // XMPP options
  xmpp: {
    // The JID of this component
    domain: "component.example.net",
    // A descriptive name, used during service discovery
    name: "My Generic Component",
    // Where to connect to, must match the configuration in the server
    service: "xmpp://example.net:5678",
    // Secret shared with the server
    password: "xxxxxxxx"
  },
  // Optional persistence settings
  persistence: {
    // Save the state of the component to this file
    path: "/path/to/data.nedb"
  }
}
const component = new GenericComponent({settings});

component.start().then( () => console.log("Component started") );
```

### Data properties

#### `items`

A list of children items of this component. They can be anything as long as they can take a `jid` property and provide the following methods:

* `discoInfo (ctx)`, returning a `<query xmlns: "http://jabber.org/protocol/disco#info"/>` response.
* `discoItem (ctx)`, optional. If present it is expected to return either an `<item/>` response or `undefined`; in the latter case this item will not be listed in the discovery response.
* `toJSON ()`, required only if the object cannot be converted into JSON natively.

Use the methods `async addItem (item)` and `async removeItem (item)` to manipulate the list of items, as that takes care of saving the modifications to disk if persistence is enabled.

#### `registrations`

A list of JIDs registered to use this component via the standard [XMPP registration mechanism](https://xmpp.org/extensions/xep-0077.html "XEP-0077: In-Band Registration").

This can be queried via the `registeredUser (ctx)` function (or accessed directly).

#### `subscriptions`

A list of subscription pairs, of the form `{ from: "someone@example.com", to: "component.example.net" }`. If a subclass implements ‘from’ subscriptions, it may also contain the reciprocal pair `{ from: "component.example.net", to: "someone@example.com" }`.

Use the methods `async addSubscription({from: "…", to: "…"})` and `async removeSubscription({from: "…", to: "…"})` to manage subscriptions, as that takes care of saving the modifications to disk if persistence is enabled.

### Methods

This is not an exhaustive list, but these are the methods you will most likely want to use.

#### `async start ()`

Instantiates an [XMPP component](https://github.com/xmppjs/xmpp.js/tree/master/packages/component) and connects to the server. If persistence is enabled, it will try to read the previous state from disk before launching.

#### `async stop ()`

Saves state to disk (if persistence is enabled), sets presence to `unavailable`, disconnects from the server, and removes the XMPP component from the instance.

#### `async sendToItemSubscribers (item, stanza)`

Given an item (any object having a `jid` property) and an [XMPP stanza](https://github.com/xmppjs/xmpp.js/tree/master/packages/xml), it sends a copy of the stanza to each JID which is subscribed to receive presence information from `item.jid`.

## Subclassing

This class by itself is not very useful. It handles the boring component stuff such as discovery and presence but little else. It is intended to be subclassed into something that provides some specific functionality by adding or replacing hooks as needed.

## Minimum Node.js version

This code uses the [optional chaining (`?.`)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining) and [nullish coalescing (`??`)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operators [only available](https://nodejs.medium.com/node-js-version-14-available-now-8170d384567e) in Node.js version 14 and above (`--harmony` flag may be required).

## Security

The component opens an unencrypted connection to the server, so either run it on the same host, in a secure network or over an encrypted tunnel.

Subscriber and registered JIDs are saved to disk.

Logging output may capture the content of stanzas. Consider disabling logging.
