
const {xml, jid} = require('@xmpp/component')

const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');


async function exportState (self, ctx, session) {
	const sender = ctx.from.bare().toString();
	const store = self.parent.toJSON();

	if (sender == self.parent.owner) {

		const upload = {
			body: Buffer.from(JSON.stringify(store, null, 4)),
			filename: `${self.parent.jid}-${(new Date()).toISOString()}-state.json`,
			mimetype: "application/json"
		};
		const url = await self.parent.httpUpload.upload(upload.body, upload.filename, upload.mimetype);

		// No await
		self.parent.xmpp.send(
			xml("message", {from: self.info.jid, to: self.parent.owner},
				xml("body", "Component state"),
				xml("x", "jabber:x:oob", xml("url", {}, url))
			)
		);

		return xml(
			'command', {
				xmlns: 'http://jabber.org/protocol/commands',
				node: self.info.node,
				sessionid: session.id,
				status: 'completed'
			},
			xml("note", {type: "info"}, "State exported"),
			XMPPForm.form({
				fields: {
					url: {label: "URL", value: url}
				}
			}),
			xml("x", "jabber:x:oob", xml("url", {}, url))
		);

	} else {
		return xmppError(ctx.stanza, "service-unavailable");
	}
}

class ComponentCommandExportConfig extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "export-state";
		if (!settings.name) settings.name = "Export current state";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			exportState
		];
	}

	discoItem (ctx) {
		if (this.parent.componentOwner(ctx)) {
			return super.discoItem(ctx);
		}
	}

}

module.exports = ComponentCommandExportConfig;

