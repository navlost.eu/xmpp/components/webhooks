const {component, xml, jid} = require('@xmpp/component');
const clone = require("ltx/lib/clone");

const events = require('./events');
const iq = require('./events/iq');
const commands = require('./commands');
const userAllowed = require('./user-allowed');
const XMPPPubSub = require('~/lib/XMPPPubSub');
const XMPPHTTPUpload = require('~/lib/XMPPHTTPUpload');
const xmppCapabilities = require('~/lib/xmppCapabilities');
const {getJID} = require("~/lib/xml-utils");
const logger = require("~/lib/logger");

class GenericComponent {

	PUBSUB_NODE = undefined // Virtual, must be defined in derived classes

	persistedItems = ["registrations", "subscriptions", "items", "nodes"];

	constructor({settings}) {
		this.settings = settings;

		// Our own (i.e., this component's) JID
		this.jid = settings.xmpp.domain;
		// The JID of whoever is responsible for this component (not for individual endpoints)
		this.owner = settings.owner;
		// The JID of the server we're registered with. We need this for presence stanzas.
		this.server = settings.xmpp.server || settings.xmpp.domain.split(".").slice(1).join(".");
		// List of registered users (users who have registered with the component in
		// order to be able to create endpoints).
		this.registrations = [];
		// List of subscriptions (JIDs that have subscribed to one of our endpoints' presence
		// or MUCs that an endpoint has been registered with.
		this.subscriptions = [];
		// List of endpoints.
		this.items = [];
		// List of node items (e.g., <item jid="…" name="…" node="some-id"/>)
		this.nodes = [];
		// Discovery info
		this.discovery = {};

		// Event callbacks
		// Expected structure is {eventName: function (), anotherEventName: function (), …}
		this.events = events;

		// Info/query (IQ) callbacks
		// Expected structure is {callee: {get: […], set: […]}}
		this.iq = iq;

		// Component-level commands
		this.commands = commands.map(c =>
			new c({parent: this, jid: settings.xmpp.domain})
		);

		this.userAllowed = userAllowed.bind(this);
	}

	/** If the component will be uninstalled or otherwise
	 * cleared from the system, we delete the PubSub
	 * node containing the configuration.
	 */
	async uninstall () {
		await this.pubsub.del(this.PUBSUB_NODE);
		logger.info("Deleted storage node", this.PUBSUB_NODE);
	}

	/** Remove the item containing this component's configuration
	 * from PubSub.
	 */
	async clearConfiguration () {
		await this.pubsub.retract(this.PUBSUB_NODE, this.jid);
		logger.info("Retracted node", this.PUBSUB_NODE, this.jid);
	}

	/** Copy the configuration from another JID.
	 *
	 * Useful for instance if the component is renamed.
	 */
	async copyConfiguration (oldJID) {
		const store = await this.pubsubReadPersisted(oldJID);
		await this.pubsubSavePersisted(store);
		logger.info(`Configuration copied from ${oldJID} to ${this.jid}`);
	}

	async readPersisted (store) {
		if (!store) {
			store = await this.pubsubReadPersisted();
		}

		if (store) {
			for (const key of this.persistedItems) {
				if (store && (key in store)) {
					this[key] = store[key];
				}
			}
		}
	}

	async pubsubReadPersisted (jid) {
		if (!jid) {
			jid = this.jid;
		}

		if (!this.pubsub) {
			throw new Error("No Publish-Subscribe service has been defined");
		}

		try {
			const res = await this.pubsub.items(this.PUBSUB_NODE, [jid]);
			const items = res.getChild("items").getChildren("item");
			const item = items.find(item => item.attrs.id == jid);
			if (item) {
				logger.debug("Fetch configuration for", item.attrs.id);
				const json = item.getChild("json", "urn:xmpp:json:0").text();
				if (json) {
					return JSON.parse(json);
				} else {
					logger.debug("🐞", `Found empty JSON node for ${this.PUBSUB_NODE} ${jid}`);
				}
			}
		} catch (err) {
			if (err.name == "StanzaError" && err.condition == "item-not-found") {
				logger.info(`Storage node ${this.PUBSUB_NODE} does not yet exist. It will be created when saving state`);
			} else {
				throw err;
			}
		}
	}

	async savePersisted (store) {
		if (store) {
			await this.readPersisted(store);
		}

		store = this.toJSON();
		await this.pubsubSavePersisted(store);
	}

	async pubsubSavePersisted (store) {
		if (!this.PUBSUB_NODE) {
			logger.warn("No PubSub node name defined");
			return;
		}

		try {
			await this.pubsub.create(this.PUBSUB_NODE);
			logger.info("Created storage node", this.PUBSUB_NODE);
		} catch (err) {
			if (err.name == "StanzaError" && err.condition == "conflict") {
				// We're OK, the node already exists
				logger.info("Found existing storage node", this.PUBSUB_NODE)
			} else {
				throw err;
			}
		}

		const content = xml("json", "urn:xmpp:json:0", JSON.stringify(store));
		await this.pubsub.publish(this.PUBSUB_NODE, content, this.jid);
		logger.info("Saved configuration", this.PUBSUB_NODE, this.jid);
	}

	async start (runOnce = []) {
		if (!this.xmpp) {
			this.xmpp = component(this.settings.xmpp);
			if (this.settings?.persistence?.pubsub) {
				this.pubsub = new XMPPPubSub(this.xmpp, this.settings.persistence.pubsub);
			}
			if (this.settings?.services?.upload) {
				this.httpUpload = new XMPPHTTPUpload(this.xmpp, this.settings.services.upload);
			}

			// Set handlers for iq events
			for (const type of ["get", "set"]) {
				for (const event of this.iq.callee[type]) {
					this.xmpp.iqCallee[type](event.namespace, event.tag, event.handler.bind(this));
				}
			}

			// Set handlers for other events
			for (const event in this.events) {
				if (Array.isArray(this.events[event])) {
					for (const handler of this.events[event]) {
						this.xmpp.on(event, handler.bind(this));
					}
				} else {
					this.xmpp.on(event, events[event].bind(this));
				}
			}

			// Set listeners for runOnce events
			if (Array.isArray(runOnce)) {
				for (const fn of runOnce) {
					this.xmpp.once("online", fn.bind(this));
				}
			}

			return await this.xmpp.start();
		}
	}

	async stop () {
		if (this.xmpp) {
			await this.savePersisted();
			await this.presence("unavailable");
			await this.xmpp.stop();
			delete this.xmpp;
		}
	}

	/** Send stanza to subscribers.
	 *
	 * @a stanza The stanza to send (the "to" attribute will be rewritten).
	 * @a item An item to whose subscribers we will address the stanza.
	 * - If `item` is a string, it will be interpreted as a JID and
	 *   `stanza` will be addressed to its subscribers.
	 * - If `item` is undefined, `stanza` will be addressed to all subscribers.
	 */
	async sendToItemSubscribers (stanza, item) {
		// TODO Use https://xmpp.org/extensions/xep-0033.html instead
		const subscribers = typeof item === "string"
			? this.subscriptions.filter(s => s.to == item)
			: item
				? this.subscriptions.filter(s => s.to == item.jid)
				: this.subscriptions;

		const sender = typeof item === "string"
			? item
			: item
				? item.jid
				: this.jid;

		for (const subscriber of subscribers) {
			const payload = clone(stanza);
			payload.attrs.from = sender;
			payload.attrs.to = subscriber.from;
			if (subscriber.type == "groupchat") {
				const subject = payload.getChild("subject");
				payload.remove(subject);
				if (subject) {
					// Try to change the MUC's subject
					const topic = xml("message");
					topic.attrs = payload.attrs;
					topic.attrs.type = "groupchat";
					topic.append(subject);
					await this.xmpp.send(topic);
				}
			}

			await this.xmpp.send(payload);
		}

		// Update last activity
		if ((typeof item === "object") && ("lastActivity" in item)) {
			item.lastActivity = Date.now()/1000;
		}
	}


	/** Make a discovery request.
	 *
	 * @a jid the JID to query. If left blank, the
	 * server will be queried and the results saved
	 * under this.discovery[this.server].
	 */
	async discover (jid) {
		if (!jid) {
			jid = this.server;
		}

		try {
		const info = await this.xmpp.iqCaller.get(
			xml("query", "http://jabber.org/protocol/disco#info"),
			jid
		);
		if (info) {
			this.discovery[jid] = {
				info: {
					identity: [],
					feature: []
				},
				items: []
			};

			for (const identity of info.getChildren("identity")) {
				this.discovery[jid].info.identity.push({...identity.attrs});
			}

			for (const feature of info.getChildren("feature")) {
				this.discovery[jid].info.feature.push(feature.attrs.var);
			}
		}
		} catch (err) {
			if (err.name == "StanzaError") {
				// Do not fail. Assume we're just not allowed to query this JID.
				logger.warn("⚠", `StanzaError: (${err.condition}) ${err.text}`);
			} else {
				throw err;
			}
		}

		try {
			const items = await this.xmpp.iqCaller.get(
				xml("query", "http://jabber.org/protocol/disco#items"),
				jid
			);
			if (items) {
				for (const item of items.getChildren("item")) {
					if ("node" in item.attrs) {
						// We're not interested in these.
						continue;
					}

					if (!(item.attrs.jid in this.discover)) {
						// Get info for the item itself
						await this.discover(item.attrs.jid);
					}
					this.discovery[jid].items.push(item.attrs.jid);
				}
			}
		} catch (err) {
			if (err.name == "StanzaError") {
				// Do not fail. Assume we're just not allowed to query this JID.
				logger.warn("⚠", `StanzaError: (${err.condition}) ${err.text}`);
			} else {
				throw err;
			}
		}

	}

	/** Try to fill in missing configuration items
	 * from discovery info.
	 *
	 * await this.discover() must have been called first.
	 */
	async autoconfigure () {
		for (const jid of Object.keys(this.discovery)) {

			if (jid == this.server) {
				// Not interested in our parent server
				continue;
			}
			if (jid == this.domain) {
				// Not interested in ourselves
				continue;
			}

			const item = this.discovery[jid];

			if (item?.info?.feature?.includes("urn:xmpp:http:upload:0")) {
				// We've got an HTTP upload service

				if (!this.settings.services) {
					this.settings.services = {};
				}

				if (!this.settings.services.upload) {
					this.settings.services.upload = jid;
					this.httpUpload = new XMPPHTTPUpload(this.xmpp, this.settings.services.upload);
					logger.info("ℹ️", `HTTP upload service set to ${jid}`);
				}
			}

			if (item?.info?.feature?.includes("http://jabber.org/protocol/pubsub")) {
				// We've got a PubSub service

				if (!this.settings.persistence) {
					this.settings.persistence = {};
				}

				if (!this.settings.persistence.pubsub) {
					this.settings.persistence.pubsub = jid;
					this.pubsub = new XMPPPubSub(this.xmpp, this.settings.persistence.pubsub);
					logger.info("ℹ️", `Publish-Subscribe service set to ${jid}`);
				}
			}
		}
	}

	async caps (ctx, item) {
		const discoInfo = await (item ?? this).discoInfo(ctx);
		return xmppCapabilities(this.PUBSUB_NODE, discoInfo);
	}

	/** Service discovery information we provide in
	 * response to a request.
	 */
	async discoInfo (ctx) {

		const res = xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'component',
					type: 'generic',
					name: this.settings.xmpp.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#items' }
			),
			xml(
				'feature',
				{ 'var': 'jabber:iq:version' }
			),
			xml(
				'feature',
				{ 'var': 'jabber:iq:last' }
			),
			xml(
				'feature',
				{ 'var': 'urn:xmpp:ping' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/caps' }
			)
		);

		if (ctx === true || this.userAllowed(ctx)) {
			res.append(
				xml(
					'feature',
					{ 'var': 'jabber:iq:register' }
				)
			);
		}

		if (this.commands.some(i => i.discoItem && (ctx === true || i.discoItem(ctx)))) {
			res.append(
				xml(
					'feature',
					{ 'var': 'http://jabber.org/protocol/commands' }
				)
			);
		}

		return res;
	}

	async register (ctx) {
		if (this.userAllowed(ctx)) {
			const sender = ctx.from.bare().toString();

			if (ctx.type == "get") {
				const user = this.registrations.find(i => i.jid == sender);
				if (user) {
					return xml(
						'query', 'jabber:iq:register',
						xml('registered'),
						xml('username', {}, user.nick),
						xml('instructions', {},
							`You are already registered to this service (nick: ${user.nick})`
						)
					);
				} else {
					const stanza = xml("query", "jabber:iq:register");
					stanza.append(xml('instructions', {}, `You are about to register as ${sender}`))
					if (this.settings.registration.banner) {
						stanza.append(xml("instructions", {}, this.settings.registration.banner));
					}
					stanza.append(xml("username"));
					return stanza;
				}
			} else if (ctx.type == "set") {
				const index = this.registrations.findIndex(i => i.jid == sender);
				if (ctx.element.getChild('remove')) {
					// This is a request to unregister…
					if (index != -1) {
						// …and the user is registered…
						// …but not for much longer.
						await this.removeUser(sender);

						// There needs to be a non-falsey return value, judging
						// by https://github.com/xmppjs/xmpp.js/blob/c41313b1a2f57b62ecf8e763ec188c75e17026bc/packages/iq/callee.js#L81
						return true;
					}
				} else {
					// This is a request to register…
					if (index == -1) {
						// …and the user is not already registered…
						// …we check if the nick is in use…
						const nick = ctx.element.getChild("username").text();
						if (this.registrations.find(i => i.nick == nick && i.jid != sender)) {
							// …which it is, so we reject the registration…
							// …but we'll let the user know why
							setImmediate( () =>
								this.xmpp.send(xml("message", {type: "headline", to: sender},
									xml("body", {}, "The nick you have chosen cannot be used.")
								))
							);
							return xml("error", {code: 409, type: "cancel"},
								xml("conflict", "urn:ietf:params:xml:ns:xmpp-stanzas")
							);
						} else {
							// …the nick is not taken…
							// …so we add the user to our registration list…
							this.registrations.push({jid: sender, nick});
							await this.savePersisted();
							// …let the user know he's been successful…
							return true;
							// …and we send the list of commands now available
							// to the user.
							setImmediate( () => this.pushCommands(ctx) );
						}
					} else {
						return xml("error", {code: 409, type: "cancel"},
							xml("conflict", "urn:ietf:params:xml:ns:xmpp-stanzas")
						);
					}
				}
			}
		}
	}

	registeredUser (ctx) {
		const sender = getJID(ctx).from.bare().toString();
		return this.registrations.find(i => i.jid == sender);
	}

	componentOwner (ctx) {
		const sender = getJID(ctx).from.bare().toString();
		return this.owner == sender;
	}

	/** Remove a user.
	 *
	 * @a jid JID of the user to remove.
	 *
	 * All of the user's endpoints will be deleted!
	 */
	async removeUser (jid) {
		const userIndex = this.registrations.findIndex(i => i.jid == jid);
		if (userIndex != -1) {
			const userItems = this.items.filter(i => i.owner == jid);
			for (const userItem of userItems) {
				await this.removeItem(item);
			}
			this.registrations.splice(userIndex, 1);
			await this.savePersisted();
		}
	}

	pushCommands (ctx) {
		this.xmpp.send(xml(
			'message', {from: ctx.to, to: ctx.from},
			xml('subject', "Service commands"),
			xml('query',
				{
					xmlns: 'http://jabber.org/protocol/disco#items',
					node: 'http://jabber.org/protocol/commands'
				},
				...this.commands.map(i => i.discoItem && i.discoItem(ctx))
			)
		))
	}

	async addItem (item) {
		this.items.push(item);
		await this.savePersisted();
		// If anyone is already subscribing, tell them we're online
		await this.presence(undefined, item);
	}

	async removeItem (item) {
		const i = (typeof item === "object")
			? this.items.findIndex(i => i == item)
			: this.items.findIndex(i => i.uuid == item);
		if (i != -1) {
			this.items.splice(i, 1);
			// Let people know we're gone
			await this.presence("unavailable", item);
			// And remove all subscriptions
			// mentioning us.
			await this.removeSubscriptions(item.jid);
		}
		await this.savePersisted();
	}

	getItem ({jid, uuid}) {
		return this.items.find(i => i.jid == jid || i.uuid == uuid);
	}

	hasSubscription (subscription) {
		const s = subscription;
		return this.subscriptions.find(i =>
			i.from == s.from &&
			i.to == s.to &&
			i.type == s.type);
	}

	async addSubscription (subscription) {
		this.subscriptions.push(subscription);
		await this.savePersisted();
	}

	async removeSubscription (subscription) {
		const i = this.subscriptions.findIndex(i => i == subscription);
		if (i != -1) {
			this.subscriptions.splice(i, 1);
		}
		await this.savePersisted();
	}

	/** Remove all subscriptions for a specific JID.
	 *
	 * @a jid the JID to search for. Any subscription having
	 * `jid` in its `from` or `to` fields will be removed.
	 *
	 * Note that presence updates are not sent by this method.
	 */
	async removeSubscriptions (jid) {
		this.subscriptions = this.subscriptions.filter(i => i.to != jid && i.from != jid);
		await this.savePersisted();
	}

	receipts (stanza) {

		const sender = jid(stanza.attrs.from).bare().toString();
		const addressee = jid(stanza.attrs.to).bare().toString();
		const id = stanza.attrs.id;
		const type = stanza.attrs.type || "chat";

		const receipt = (stanza?.getChild("request", "urn:xmpp:receipts")) &&
			xml("received",
				{
					xmlns: "urn:xmpp:receipts",
					id
				}
			);

		const marker = (stanza?.getChild("markable", "urn:xmpp:chat-markers:0")) &&
			xml("received",
				{
					xmlns: "urn:xmpp:chat-markers:0",
					id
				}
			);

		if (receipt || marker) {

			const ack = xml("message",
				{
					xmlns: "jabber:client",
					from: addressee,
					to: sender,
					type
				},
				receipt,
				marker,
				xml("store", "urn:xmpp:hints")
			);

			this.xmpp.send(ack);

		}
	}

	chatMarkers (stanza) {
		if (stanza?.getChild("markable", "urn:xmpp:chat-markers:0")) {

			const sender = jid(stanza.attrs.from).bare().toString();
			const addressee = jid(stanza.attrs.to).bare().toString();
			const id = stanza.attrs.id;
			const type = stanza.attrs.type || "chat";

			const ack = xml("message",
				{
					xmlns: "jabber:client",
					from: addressee,
					to: sender,
					type
				},
				xml("displayed",
					{
						xmlns: "urn:xmpp:chat-markers:0",
						id
					}
				),
				xml("store", "urn:xmpp:hints")
			);
			this.xmpp.send(ack);
		}
	}


	/** Send presence updates.
	 *
	 * @a type is the value of the `type` attribute as defined in
	 * RFC6121 § 4.7.1 (https://xmpp.org/rfcs/rfc6121.html#presence-syntax-type)
	 * or undefined to send the implicit ‘available’ presence status (this is
	 * the default).
	 *
	 * @a item is an optional ‘item’ (i.e., an object containing a "jid" attribute
	 * representing a JID value. If specified, presence is sent to subscribers of
	 * that item only. If not specified (the default), presence is sent to
	 * subscribers of all items.
	 *
	 * Examples:
	 *
	 * - Component going online:  presence()
	 * - Component going offline: presence("unavailable")
	 * - One item going online:   presence(undefined, item)
	 * - One item going offline:  presence("unavailable", item);
	 */
	async presence(type, item) {
		const stanza = xml("presence", {from: this.jid, type});
		if (!type) {
			// Add capabilities
			// For this, we need to get our own discoInfo but that
			// is based on who is asking. Given that we know that we
			// are sending to subscribers, we assume that they pass
			// allowedUser() test (for items) and the userRegistered()
			// test (for component presence).
			// FIXME If a registered user / subscriber has been banned
			// in the meanwhile, they'll still be able to receive
			// presence updates. This will need to be dealt with.
			if (!item) {
				// Our own capabilities, as seen by subscribed users
				stanza.append(await this.caps(true));
			} else {
				// This item's capabilities, as seen by subscribed users
				stanza.append(await this.caps(true, item));
			}
		}

		if (item) {
			// We are spending presence from one specifc item
			// to its subscribers
			await this.sendToItemSubscribers(stanza, item);
		} else {
			// We are sending presence to everyone…
			for (const item of this.items) {
				// …from each item to its subscribers…
				await this.sendToItemSubscribers(stanza, item);
			}
			// …and from the component itself, if anyone is
			// subscribing to it.
			await this.sendToItemSubscribers(stanza, this.jid);

		}

	}

	async joinMUC(JID, asJID) {
		if (JID) {
			await this.xmpp.send(xml("presence", {from: asJID, to: JID}, xml("x", "http://jabber.org/protocol/muc")));
		} else {
			const groupchats = this.subscriptions.filter(
				subscription => subscription.type == "groupchat"
			);

			for (const subscription of groupchats) {
				const asJID = jid(subscription.to);
				const JID = jid(subscription.from);
				JID.resource = subscription.nick;
				await this.joinMUC(JID, asJID);
			}
		}
	}

	/** Send a stanza, taking care to tweak the recipient if replying
	 * to a MUC message.
	 *
	 * The weird promise thing is so that it can be sent from handlers
	 * without crashing xmppjs.
	 */
	sendStanza (stanza) {
		if (stanza.is("message")) {
			if (stanza.attrs.type == "groupchat") {
				stanza.attrs.to = jid(stanza.attrs.to).bare().toString();
			}
		}

		return new Promise((resolve, reject) => {
			setTimeout( async () =>
				this.xmpp.send(stanza).then(resolve).catch(reject),
				1
			)
		});
	}

	/** Send a message to an item's owner or to the component administrator
	 */
	async notify(msg, item) {
		const sender = item?.jid ?? this.jid;
		const recipient = item?.owner ?? this.owner;
		if (msg && sender && recipient) {
			return await this.xmpp.send(
				xml("message", { from: sender, to: recipient, type: "headline" }, xml("body", {}, msg))
			);
		}
	}

	toJSON() {
		const store = {};
		for (const key of this.persistedItems) {
			const item = this[key];
			store[key] = item.toJSON
				? item.toJSON()
				: Array.isArray(item)
					? item.map(i => i.toJSON ? i.toJSON() : i)
					: item;
		}
		store.jid = this.jid;
		return store;
	}
}

module.exports = GenericComponent;
