const {xml} = require('@xmpp/component')
const logger = require('~/lib/logger');

class XMPPPubSub {

	constructor (xmpp, addressee) {
		this.xmpp = xmpp;
		this.addressee = addressee; // PubSub service to use
	}

	async get (req) {
		try {
			return await this.xmpp.iqCaller.get(req, this.addressee);
		} catch (err) {
			if (err.name == "StanzaError") {
				logger.error("PubSub", err.condition, err.text);
			}
			throw err;
		}
	}

	async set (req) {
		try {
			return await this.xmpp.iqCaller.set(req, this.addressee);
		} catch (err) {
			if (err.name == "StanzaError") {
				logger.error("PubSub", err.condition, err.text);
			}
			throw err;
		}
	}

	/** Create a node, with optional configuration
	 *
	 * https://xmpp.org/extensions/xep-0060.html#owner-create
	 */
	async create (name, configuration) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub")
		req.append(xml("create", {node: name}));

		if (configuration) {

			const configure = xml("configure");
			const form = xml("x", {xmlns: "jabber:x:data", type: "submit"});
			form.append(xml("field", {"var": "FORM_TYPE", type: "hidden"},
				xml("value", {}, "http://jabber.org/protocol/pubsub#node_config")
			));
			configure.append(form);

			for (key in configuration) {
				const field = xml("field", {"var": key}, xml("value", {}, configuration[key]));
				form.append(field);
			}

			req.append(configure);
		}

		return await this.set(req);
	}

	/** Configure a node
	 *
	 * https://xmpp.org/extensions/xep-0060.html#owner-configure
	 */
	async configure (name, configuration) {
		throw this.error("not supported");
// 		if (this.isOnline()) {
// 			let res;
// 			if (name) {
// 				res = await this.xmpp.iqCaller.get(xml("pubsub",
// 					"http://jabber.org/protocol/pubsub#owner",
// 					xml("configure", {node: name})
// 				);
//
// 				if (!configuration) {
// 					// Cancel the configuration form and return the current values
// 					// TODO send cancel
// 				} else {
// 					// Set the new values (we do not resubmit the whole lot but only
// 					// what the user has provided in `configuration`. I hope this will
// 					// work. XEP-0060 is not clear in this aspect.
// 					// TODO
// 				}
// 			} else {
// 				// Request the default configuration
// 				res = await this.xmpp.iqCaller.get(xml("pubsub",
// 					"http://jabber.org/protocol/pubsub#owner",
// 					xml("default")
// 				);
// 			}
//
// 			if (res.is("error")) {
// 				throw this.error("request failed", res)
// 			} else {
// 				return res;
// 			}
// 		} else {
// 			throw this.error("not online");
// 		}
	}

	/** Delete a node
	 *
	 * https://xmpp.org/extensions/xep-0060.html#owner-delete
	 */
	async del (name, redirect) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub#owner");
		const del = xml("delete", {node: name});
		req.append(del);
		if (redirect) {
			del.append(xml("redirect", {uri: redirect}));
		}

		return await this.set(req);
	}

	/** Purge all items from a node
	 *
	 * https://xmpp.org/extensions/xep-0060.html#owner-purge
	 */
	async purge (name) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub#owner");
		req.append(xml("purge", {node: name}));

		return await this.set(req);
	}

	/** Get or set affiliations
	 *
	 * @a name is the name of the node to operate on.
	 *
	 * @a affiliations is an object of the form
	 * {jid0: affiliation, jid1:affiliation, …}
	 *
	 * @return the list of current affiliations for all
	 * nodes  if neither `name` nor `affiliations` are
	 * provided.
	 *
	 * @return the list of current affiliations for a
	 * specific node if `name` is provided and `affiliations`
	 * is not.
	 *
	 * @return the result of modifying the affiliations for
	 * a node if both `name` and `affiliations` are provided.
	 * To remove an affiliation, set it to "none".
	 *
	 * https://xmpp.org/extensions/xep-0060.html#entity-affiliations
	 */
	async affiliations (name, affiliations) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub#owner");
		const cmd = xml("affiliations", {node: name});
		req.append(cmd);
		if (affiliations) {
			for (jid in affiliations) {
				const affiliation = affiliations[jid];
				cmd.append(xml("affiliation", {jid, affiliation}));
			}

			res = await this.set(req);
		} else {
			// NOTE different method if only retrieving list
			res = await this.get(req);
		}
	}

	/** Subscribe JID to node
	 *
	 * https://xmpp.org/extensions/xep-0060.html#subscriber-subscribe
	 */
	async subscribe (name, jid) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub")
		const cmd = xml("subscribe", {node: name, jid});
		req.append(cmd);

		return await this.set(req);
	}

	/** Unsubscribe JID from node
	 *
	 * https://xmpp.org/extensions/xep-0060.html#subscriber-unsubscribe
	 */
	async unsubscribe (name, jid) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub");
		const cmd = xml("unsubscribe", {node: name, jid});
		req.append(cmd);

		return await this.set(req);
	}

	/** Return items for a node
	 *
	 * @a name is the name of the node.
	 *
	 * @a items is an optional list of item IDs to
	 * retrieve, or a number indicating the maximum
	 * number of most recent items to return.
	 *
	 * @returns all items if `ids` is not specified.
	 * @note that the item list may be truncated.
	 *
	 * @returns the specific items requested if `ids`
	 * is provided.
	 *
	 * https://xmpp.org/extensions/xep-0060.html#subscriber-retrieve-requestall
	 */
	async items (name, items) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub")
		const cmd = xml("items", {node: name});
		req.append(cmd);

		if (items) {
			if (typeof items === "number") {
				cmd.attrs.max_items = items;
			} else if (Array.isArray(items)) {
				for (const id of items) {
					cmd.append(xml("item", {id}));
				}
			}
		}

		return await this.get(req);
	}

	/** Publish one or more items
	 *
	 * @a name is the name of the node to publish to.
	 *
	 * @a content is the content to publish. It may be
	 * an xml() element, or an array thereof, or an object
	 * of the form {id0: xml, id1: xml, …}.
	 *
	 * @a id is an optional ID to assign to `content` in
	 * the event that `content` is an xml() element. In all
	 * other cases, `id` is ignored.
	 *
	 * @a options is an optional list of publishing options
	 * of the form {option0: value0, option1: value1, …}.
	 * See https://xmpp.org/extensions/xep-0060.html#publisher-publish-options
	 *
	 * https://xmpp.org/extensions/xep-0060.html#publisher-publish
	 */
	async publish (name, content, id, options) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub");
		const cmd = xml("publish", {node: name});
		req.append(cmd);

		if (content instanceof xml.Element) {
			cmd.append(xml("item", {id}, content));
		} else if (Array.isArray(content)) {
			for (payload of content) {
				cmd.append(xml("item", {}, payload));
			}
		} else if (content instanceof Object) {
			for (id in content) {
				cmd.append(xml("item", {id}, content[id]));
			}
		}

		if (options) {
			const form = xml("x", {xmlns: "jabber:x:data", type: "submit"});
			form.append(xml("field", {"var": "FORM_TYPE", type: "hidden"},
				xml("value", {}, "http://jabber.org/protocol/pubsub#publish-options")
			));

			for (key in options) {
				const field = xml("field", {"var": key});
				field.append(xml("value", {}, options[key]))
				form.append(field);
			}
		}

		return await this.set(req);
	}

	async retract (name, id, notify = false) {
		const req = xml("pubsub", "http://jabber.org/protocol/pubsub");
		const cmd = xml("retract", {node: name});
		req.append(cmd);

		if (notify) {
			cmd.attrs.notify = "true";
		}

		if (!Array.isArray(id)) {
			id = [id];
		}

		for (const key of id) {
			cmd.append(xml("item", {id: key}));
		}

		return await this.set(req);
	}

}

module.exports = XMPPPubSub;
