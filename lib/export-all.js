const fs = require('fs');
const path = require('path');

function entries (dir) {
	const files = fs.readdirSync(dir, {withFileTypes: true}) // Only take…
		.filter(f => (f.isFile() || f.isSymbolicLink()) && // …files and symlinks…
			path.extname(f.name) == '.js' && // …with the .js extension…
			f.name[0] != '.' && // …which are not hidden…
			f.name != 'index.js' // …and exclude index.js.
		).sort((a, b) =>
			a.name < b.name
				? -1
				: a.name > b.name
					? 1
					: 0
		);

	return files;
}

function asArray (dir) {
	return entries(dir).map(file => require(path.join(dir, file.name)));
}

function asObject (dir) {
	const list = {};
	for (const file of entries(dir)) {
		list[path.basename(file.name, '.js')] = require(path.join(dir, file.name));
	}
	return list;
}

asObject.asArray = asArray;

module.exports = asObject;
