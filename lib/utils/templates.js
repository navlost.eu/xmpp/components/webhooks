const fs = require('fs');
const Path = require('path');
const YAML = require('yaml');

function readTemplates (path) {
	let templates = {};
	const dir = fs.opendirSync(path);
	let entry;
	while (entry = dir.readSync()) {
		if (entry.isDirectory()) {
			continue;
		}
		if (entry.name.endsWith(".yaml") || entry.name.endsWith(".yml")) {
			const data = fs.readFileSync(Path.join(path, entry.name))
			const obj = YAML.parse(data.toString("utf-8"));

			templates = {...templates, ...obj};
		}
	}
	return templates;
}

module.exports = {
	readTemplates
};
