

function getSender (ctx) {
	if (ctx?.from) {
		const sender = ctx.stanza.attrs.type == "groupchat"
			? ctx.from.toString()
			: ctx.from.bare().toString();
		return sender;
	}
}

/** Find all the descendants of child for which fn() returns true.
 */
function filterNodes (child, fn) {
	function iter (root, fn, nodeSet) {
		if (root) {
// 			if (root.id) console.log("NODE ID", root.id, root.constructor.name);
			if (fn(root)) {
// 				console.log("FOUND MATCHING NODE");
				nodeSet.add(root);
			}

			if (root.items) {
// 				console.log("FOUND", root.items?.length, "CHILD ITEMS");
				root.items.forEach(item => iter(item, fn, nodeSet));
			}

			if (root.nodes) {
// 				console.log("FOUND", root.nodes?.length, "CHILD NODES");
				root.nodes.forEach(node => iter(node, fn, nodeSet));
			}
		}
		return nodeSet
	}
	return [...iter(child, fn, new Set())];
}

/** Crawl up an implementation's hierarchy until
	* reaching the component object.
	*/
function getComponent (child) {
	if (!child) {
		return;
	} else if (child.constructor?.name == "GenericComponent") {
		return child;
	} else if (Object.getPrototypeOf(child.constructor)?.name == "GenericComponent") {
		// NOTE It only goes one level up the class hierarchy.
		return child;
	} else if (child.parent) {
		return getComponent(child.parent);
	}
}

/** Send a stanza from the component object associated
	* with `child`.
	*/
function sendStanza (child, stanza) {
	return new Promise((resolve, reject) => {
		const component = getComponent(child);
		if (component) {
			component.sendStanza(stanza).then(resolve).catch(reject)
		} else {
			reject(new Error("No component found"));
		}
	});
}

module.exports = {
	getSender,
	filterNodes,
	getComponent,
	sendStanza
};
