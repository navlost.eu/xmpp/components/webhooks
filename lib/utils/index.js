const { createHmac, createHash } = require('crypto');
const stripe = require('stripe')();

function verifyStripeSignature (req, secret) {
	const payload = req.rawBody;
	const signature = req.get("Stripe-Signature");
	try {
		const event = stripe.webhooks.constructEvent(payload, signature, secret);

		// Check for replay attacks. Reject anything older than five minutes
		const tstamp = Object.fromEntries(signature.split(",").map(s => s.split("=")))["t"] * 1000
		if ((Date.now() - tstamp) < 5*60000) {
			req.body = event;
			return true;
		}
		return false;
	} catch (err) {
		console.error(err);
		return false;
	}
}

function verifyGiteaSignature (req, secret, signature) {
	let payload;
	if (req.is("application/x-www-form-urlencoded")) {
		// Gitea sends this as a single field named `payload` which has the
		// text representation of a JSON object.
		if (req.body.payload) {
			payload = req.body.payload;
		} else {
			payload = "";
		}
	} else {
		payload = req.rawBody;
	}
	const hmac = createHmac("sha256", secret);
	hmac.update(payload);
	const digest = hmac.digest("hex");
// 	console.log("SIGNATURE", signature);
// 	console.log("DIGEST   ", digest);
	return signature == digest;
}

function verifyNextcloudSignature (req, secret, signature) {
	const payload = req.rawBody.toString()+secret;
	const hash = createHash('sha256');
	hash.update(payload);
	const digest = hash.digest("hex");
// 	console.log("SIGNATURE", signature);
// 	console.log("DIGEST   ", digest);
	return signature == digest;
}

/** Cheap man's clone.
 */
function CloneFactory () {
	if (typeof structuredClone === 'function') {
		return structuredClone;
	} else {
		return function clone (obj) {
			return JSON.parse(JSON.stringify(obj));
		}
	}
}

const clone = CloneFactory();

module.exports = {
	verifyStripeSignature,
	verifyGiteaSignature,
	verifyNextcloudSignature,
	clone
};
