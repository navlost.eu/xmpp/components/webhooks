const {xml} = require('@xmpp/component')
const ActionSet = require('~/lib/ActionSet');

class ActionSets {

	static node = "http://navlost.eu/xmpp/components/webhooks/actions/sets";
	node = ActionSets.node;
	name = "Action sets";

	constructor (params = {}) {
		this.parent = params.parent;
		this.jid = this.parent.jid;
		this.nodes = [];

		this.loadNodes(params.nodes)
	}

	// FIXME There is a bug here in that we can end up with the same node id
	// for multiple actions. This is the case if the user imports the same
	// template more than once (presumably with different parameters).
	// The same bug may be present elsewhere that loadNodes() is used.
	loadNodes (nodes = []) {
		if (!Array.isArray(nodes)) {
			// Transform object → array
			const arr = Object.entries(nodes).map(i => ({id: i[0], ...i[1]}));
			return this.loadNodes(arr);
		}

		for (const definition of nodes) {
			const nodeItem = new ActionSet({
				...definition,
				parent: this.parent
			});
			const replaceIndex = this.nodes.findIndex(n => n.id == nodeItem.id && n.prefix == nodeItem.prefix);
			if (replaceIndex != -1) {
				this.nodes[replaceIndex] = nodeItem;
			} else {
				this.nodes.push(nodeItem);
			}
		}
		// TODO We probably should send a <message> with the new version of this node.
		// Check XEPs for that.
	}

	discoInfo (ctx) {
		return xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'hierarchy',
					type: 'branch',
					name: this.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#items' }
			),
		);
	}

	discoItem (ctx) {
		return xml('item', {jid: this.parent.jid, name: this.name, node: this.node});
	}

	toJSON () {
		return {
			node: this.node,
			nodes: this.nodes.map(n => n.toJSON())
		};
	}

	// Return the action sets found in `parent`.
	static getFrom (parent, ctx) {
		if (parent?.nodes) {
			const actionSets = parent.nodes
				.filter( n => n.node == ActionSets.node)
				.map( n => n.nodes)
				.flat()
				.filter( n => n instanceof ActionSet)
				.filter( n => !ctx || n.isUserAllowed(ctx))
			return actionSets;
		}
		return []
	}
}

module.exports = ActionSets;
