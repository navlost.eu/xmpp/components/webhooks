const {xml} = require('@xmpp/component');
const id = require("@xmpp/id");
const logger = require("~/lib/logger");

module.exports = function error (stanza, condition, {text, type, url} = {}) {
	if (!stanza) {
		logger.error("!", "No stanza given to xmpp-error function. The condition was ", condition);
		return;
	}
	const xmlns = 'urn:ietf:params:xml:ns:xmpp-stanzas';
	const responseElement = xml(
		'message',
		{
			type: 'error',
			id: stanza.attrs.id || id(),
			from: stanza.attrs.to,
			to: stanza.attrs.from
		}
	);
	const errorElement = xml('error', {type: "cancel"});
	const conditionElement = xml(condition, xmlns);
	switch (condition) {
		case "bad-request":
		case "jid-malformed":
		case "not-acceptable":
			errorElement.attrs.type = "modify";
			break;
		case "conflict":
		case "internal-server-error":
		case "item-not-found":
		case "not-allowed":
		case "remote-server-not-found":
		case "service-unavailable":
			errorElement.attrs.type = "cancel"; // Default
			break;
		case "feature-not-implemented":
			errorElement.attrs.type = type ?? "cancel";
			break;
		case "forbidden":
		case "not-authorized":
		case "registration-required":
		case "subscription-required":
			errorElement.attrs.type = "auth";
			break;
		case "gone":
			errorElement.attrs.type = "cancel"; // Default
			if (url) {
				errorElement.append(xml('gone', xmlns, url));
			}
			break;
		case "policy-violation":
		case "unexpected-request":
			errorElement.attrs.type = ["modify", "wait"].includes(type) ? type : "modify";
			break;
		case "recipient-unavailable":
		case "remote-server-timeout":
		case "resource-constraint":
			errorElement.attrs.type = "wait";
			break;
		case "redirect":
			if (!url) {
				return error(stanza, "internal-server-error", {text: "Offered <redirect/> without URL"});
			} else {
				errorElement.attrs.type = "modify";
				errorElement.append(xml('redirect', xmlns, url));
			}
			break;
		case "undefined-condition":
			// Requires more involved handling, so we do not accept it
			// in this function.
			console.error("'error()' helper cannot handle", condition);
			return error(stanza, "internal-server-error");
			break;
		default:
			console.warning("Unrecognised error condition", condition);
	}

	return responseElement.append(errorElement.append(conditionElement));
}
