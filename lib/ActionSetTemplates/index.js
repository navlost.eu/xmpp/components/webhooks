const {xml} = require('@xmpp/component')
const ActionSetTemplate = require('~/lib/ActionSetTemplate');

class ActionSetTemplates {

	static node = "http://navlost.eu/xmpp/components/webhooks/actions/sets/templates";
	node = ActionSetTemplates.node;
	name = "Action set templates";

	constructor (params = {}) {
		this.parent = params.parent;
		this.jid = this.parent.jid;
		this.nodes = [];

		this.loadNodes(params.nodes)
	}

	loadNodes (nodes = []) {
		if (!Array.isArray(nodes)) {
			// Transform object → array
			const arr = Object.entries(nodes).map(i => ({id: i[0], ...i[1]}));
			return this.loadNodes(arr);
		}

		for (const definition of nodes) {
			const nodeItem = new ActionSetTemplate({
				...definition,
				parent: this.parent
			});
			const replaceIndex = this.nodes.findIndex(n => n.id == nodeItem.id);
			if (replaceIndex != -1) {
				this.nodes[replaceIndex] = nodeItem;
			} else {
				this.nodes.push(nodeItem);
			}
		}
		// TODO We probably should send a <message> with the new version of this node.
		// Check XEPs for that.
	}

	discoInfo (ctx) {
		return xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'hierarchy',
					type: 'branch',
					name: this.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#items' }
			),
		);
	}

	discoItem (ctx) {
		return xml('item', {jid: this.parent.jid, name: this.name, node: this.node});
	}

	toJSON () {
		return {
			node: ActionSetTemplates.node,
			nodes: this.nodes.map(n => n.toJSON())
		};
	}
}

module.exports = ActionSetTemplates;
