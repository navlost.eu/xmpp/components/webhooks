
const fs = require("fs/promises");
const fetch = require("node-fetch");
const {xml} = require('@xmpp/component');
const logger = require('~/lib/logger');

class XMPPHTTPUpload {

	constructor (xmpp, addressee) {
		this.xmpp = xmpp;
		this.addressee = addressee;
	}

	async #getSlot (filename, mimetype, size) {
		const req = xml("request", {
			xmlns: "urn:xmpp:http:upload:0",
			filename,
			size,
			"content-type": mimetype
		});
		// NOTE Bug in node-xmpp? The .get() call doesn't work, but .request() does.
// 		const res = await this.xmpp.iqCaller.get(req, this.addressee);
		const iqres = await this.xmpp.iqCaller.request(xml("iq", {type: "get", to: this.addressee}, req));
		const res = iqres.getChild("slot", "urn:xmpp:http:upload:0");

		const slot = {
			put: {
				// url,
				// headers
			},
			get: {
				// url
			}
		};

		slot.put.url = res.getChild("put").attrs.url;
		slot.put.headers = Object.fromEntries(
			res.getChild("put").getChildren("header").map(header => {
				return [ header.attrs.name, header.text() ];
			})
		);
		slot.get.url = res.getChild("get").attrs.url;

		return slot;
	}

	/** Upload a file.
	 *
	 * @a payload {Buffer} contents of the file.
	 * @a filename {String} file name
	 * @a mimetype {String} MIME type
	 *
	 * @returns the GET URL for the file.
	 */
	async upload (payload, filename, mimetype) {

		const size = Buffer.byteLength(payload);

		const slot = await this.#getSlot(filename, mimetype, size);

		const options = {
			method: "PUT",
			headers: {
				"Content-Type": mimetype,
				...slot.put.headers
			},
			credentials: "include",
			redirect: "follow",
			body: payload
		};
		const res = await fetch(slot.put.url, options);

		if (res.ok) {
			return slot.get.url;
		}

	}

	/** Upload a file and append it to a message.
	 *
	 * @a payload {Buffer} contents of the file.
	 * @a filename {String} file name
	 * @a mimetype {String} MIME type
	 * @a message {Element} XML stanza of type `<message>`
	 *
	 * Send `message` after uploading `payload` and
	 * attaching its GET URL to `message`.
	 *
	 */
	async send (payload, filename, mimetype, message) {

		const url = await this.upload(payload, filename, mimetype);

		if (url) {

			message.append(xml("x", "jabber:x:oob", xml("url", {}, url)));

			if (!message.getChild("body")) {
				message.append(xml("body", {}, url));
			}

			return await this.xmpp.send(message);
		}
	}
}

module.exports = XMPPHTTPUpload;
