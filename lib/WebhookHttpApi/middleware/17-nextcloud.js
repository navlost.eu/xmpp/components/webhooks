
module.exports = function (req, res, next) {
	if (req.get("X-Nextcloud-Webhooks") && req.is("application/json") && req.body) {
		res.status(202).end();
		const data = {id: req.params.id, ip: req.ip, payload: req.body, path: req.params.path, mimetype: req.get("Content-Type")};
		// We send messages with three different types, so the administrator can define
		// templates matching more general or more specific events as necessary.
		req.app.locals.parent.emit('message', {...data, type: "nextcloud"});
		req.app.locals.parent.emit('message', {...data, type: `nextcloud.${req.body.eventType}`});
		req.app.locals.parent.emit('message', {...data, type: `nextcloud.${req.body.eventName}`});
	}
	next();
}
