const {serialize, toJSON} = require('~/lib/xml-utils');

// The notifications queue will *not* be flushed unless
// we see no-store or no-cache in Cache-Control.
function flush (req) {
	const preserveDirectives = ["no-store", "no-cache"];
	return (req.get("Cache-Control")??"").split(",").some(kw => preserveDirectives.includes(kw.trim()));
}

function json (req, res, next, item) {
	res.send(item.notificationsQueue.map(i => toJSON(i)));
	if (flush(req)) {
		item.flushNotificationsQueue(req.app.locals.parent.component);
		req.app.locals.parent.component.savePersisted();
	}
	next();
}


function xml (req, res, next, item) {
	let body = "<?xml version='1.0'?>";
	body += `<stream:stream version="1.0" xmlns="jabber:component:accept" xmlns:stream="http://etherx.jabber.org/streams" to="${req.app.locals.parent.component.jid}">`;

	item.notificationsQueue.forEach( notification => {
		body += serialize(notification);
	});

	body += "</stream:stream>";

	res.send(body);
	if (flush(req)) {
		item.flushNotificationsQueue(req.app.locals.parent.component);
		req.app.locals.parent.component.savePersisted();
	}
	next();
}

function text (req, res, next, item) {
	const body = item.notificationsQueue.filter(notification => notification.is("message"))
	.map( message => message.getChildText("body") )
	.filter( i => i )
	.join("\n");

	res.send(body.length ? (body+"\n") : "");
	if (flush(req)) {
		item.flushNotificationsQueue(req.app.locals.parent.component);
		req.app.locals.parent.component.savePersisted();
	}
	next();
}

module.exports = function (req, res, next) {
	if (req.method == "GET") {
		const item = req.app.locals.parent.component.getItem({uuid:req.params.id});

		if (item?.notifications) {
			try {
				const handlers = {
					"application/json": json,
					"application/xml": xml,
					"text/xml": xml,
					"text/plain": text
				};

				const mimetype = (handlers[req.query.mime] && req.query.mime) || req.accepts(Object.keys(handlers));

				if (mimetype) {
					res.set("Via", `XMPP ${item.jid}`);
					res.set("Content-Type", mimetype);
					handlers[mimetype](req, res, next, item);
				} else {
					res.status(406).send();
					next();
				}
			} catch (err) {
				next(err);
			}
		} else {
			res.status(404).end();
		}

	} else {
		next();
	}
}
