
module.exports = function (req, res, next) {
	if (req.get("Stripe-Signature") && req.is("application/json") && req.body) {
		res.status(202).end();
		const type = "stripe."+req.body.type;
		req.app.locals.parent.emit('message', {id: req.params.id, type, payload: req.body});
	}
	next();
}
