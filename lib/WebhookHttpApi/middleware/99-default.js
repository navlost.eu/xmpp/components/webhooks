
module.exports = function (req, res, next) {
	if (req.body && !res.headersSent) {

		res.status(202).end();
		req.app.locals.parent.emit('message', {id: req.params.id, type: req.get("Content-Type"), payload: req.body, path: req.params.path});
	}
	next();
}
