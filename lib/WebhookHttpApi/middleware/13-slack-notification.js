
module.exports = function (req, res, next) {
	if (req.body && (typeof req.body.payload === "string")) {
		// This might just be a Slack-type notification?
		// Ref.: https://api.slack.com/messaging/webhooks
		// WARNING This has only been cursorily tested with
		// GitLab's Slack integrations. It most certainly does
		// not cover all use cases.
		// NOTE It could be made so that only channels matching
		// our JID local part are processed. For instance, a
		// message to #development will cause development@webhooks.example.com
		// to react, but not a message to #general.

		let payload;
		try {
			payload = JSON.parse(req.body.payload);
		} catch (err) {
			if (err instanceof SyntaxError) {
				console.info("Not a JSON payload");
			} else {
				throw err;
			}
		}

		if (payload && payload.hasOwnProperty("text") && payload.hasOwnProperty("fallback")) {
			res.status(202).end();
			req.app.locals.parent.emit('message', {
				id: req.params.id,
				type: "slack.notification",
				payload: payload
			});
		}
	}
	next();
}
