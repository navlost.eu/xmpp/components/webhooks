
module.exports = function (req, res, next) {
	if (req.get("X-Gitlab-Event") && req.is("application/json") && req.body) {

		res.status(202).end();
		req.app.locals.parent.emit('message', {id: req.params.id, type: req.body.object_kind, payload: req.body});
	}
	next();
}
