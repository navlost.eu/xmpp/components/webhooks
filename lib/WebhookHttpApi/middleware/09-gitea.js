
module.exports = function (req, res, next) {
	if (req.get("X-Gitea-Event") && req.body) {
		if (req.is("application/x-www-form-urlencoded")) {
			// Gitea sends this as a single field named `payload` which has the
			// text representation of a JSON object. This being the case, we
			// reassign req.body and req.rawBody so that things work downstream.
			if (req.body.payload) {
				req.rawBody = req.body.payload;
				req.body = JSON.parse(req.rawBody);
			}
		}
		const type = "gitea."+req.get("X-Gitea-Event");
		res.status(202).end();
		req.app.locals.parent.emit('message', {id: req.params.id, type, payload: req.body});
	}
	next();
}
