/** Handle data from ‘SMS to URL Forwarder’ app
 *
 * https://github.com/bogkonstantin/android_income_sms_gateway_webhook
 * https://f-droid.org/packages/tech.bogomolov.incomingsmsgateway/
 */
module.exports = function (req, res, next) {
	if (req.get("User-Agent")?.match(/^SMS Forwarder App/) && req.body.hasOwnProperty("from")) {
		res.status(202).end();
		req.app.locals.parent.emit('message', {id: req.params.id, type: "sms/tech.bogomolov.incomingsmsgateway", ip: req.ip, payload: req.body, path: req.params.path, mimetype: req.get("Content-Type")});
	}
	next();
}
