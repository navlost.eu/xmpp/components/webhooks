
const HttpApi = require('../HttpApi');
const middleware = require('./middleware');

class WebhookHttpApi extends HttpApi {

	/** Returns a middleware that deals with checking
	 * if requests are authorised.
	 */
	authenticate () {
		if (this.authenticator) {
			return (req, res, next) => {
				const authState = this.authenticator(req)
				if (authState === true) {
					// The request is approved
					next();
				} else if (authState === false) {
					// The request has been rejected

					// Did the user send an Authorization header?
					// Return a 401 or 403 depending on the case
					if (req.get("Authorization")) {
						res.status(403).send({message: "unauthorised"});
					} else {
						res.status(401).send({message: "must authenticate"});
					}
				} else {
					// If authState is anything else, either authentication
					// is not required or not available. Just continue and
					// hope that someone takes care of things later on.
					next();
				}
			}
		} else {
			return (req, res, next) => {
				next();
			}
		}
	}

	/** Returns a middleware that translates endpoint URLs
	 * to a canonical form.
	 *
	 */
	translate () {
		if (this.settings.enableAliases) {
			return (req, res, next) => {
				if (req.params.id) {
					next();
				} else {
					if (req.params.nick && req.params.handle) {
						// Look up the nick in the registrations
						req.params.owner = req.app.locals.parent.component.registrations.find(reg => {
							return req.params.nick == reg.nick;
						})?.jid;

						// We now drop through to the next if () block.
					}

					if (req.params.owner && req.params.handle) {
						// Look up the relevant endpoint in the database
						const uuid = req.app.locals.parent.component.items.find(item => {
							return item.owner == req.params.owner &&
								item.jid.substring(0, item.jid.indexOf("@")) == req.params.handle;
						})?.uuid;

						if (uuid) {
							req.params.id = uuid;
						}
					}
					next();
				}
			}
		} else {
			return (req, res, next) => {
				next();
			}
		}
	}

	start () {

		const handlers = [ this.authenticate(), ...middleware ];
		const translate = this.translate();

		const endpoints = {
			"/~:nick/:handle/:path(*)?": {
				post: [ translate, ...handlers],
				put: [ translate, ...handlers],
				get: [ translate, ...handlers]
			},
			"/:owner(*@[^/]+)/:handle/:path(*)?": {
				post: [ translate, ...handlers],
				put: [ translate, ...handlers],
				get: [ translate, ...handlers]
			},
			"/:id/:path(*)?": {
				post: handlers,
				put: handlers,
				get: handlers
			}
		};

		super.start(endpoints);
	}

}

module.exports = WebhookHttpApi;
