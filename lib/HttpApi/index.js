
const EventEmitter = require('events');
const http = require('http');
const https = require('https');
const express = require('express');

function map (a, route, app, verbose) {
	route = route || '';
	for (var key in a) {
		switch (typeof a[key]) {
			// { '/path': { ... }}
			case 'object':
				if (!Array.isArray(a[key])) {
					map(a[key], route + key, app, verbose);
					break;
				} // else drop through
				// get: function(){ ... }
			case 'function':
				if (verbose) console.log('%s %s', key, route);
				app[key](route, a[key]);
				break;
		}
	}
};

class HttpApi extends EventEmitter {
	constructor (settings = {}) {
		super();

		this.settings = Object.assign({
			host: "127.0.0.1",
			port: 3000
		}, settings);

	}

	start (endpoints) {

		if (this.app) {
			// start() has already been called without
			// an intervening stop()
			return;
		}

		if (!endpoints) {
			endpoints = this.defaultEndpoints();
		}

		this.verbose = process.env.NODE_ENV != 'test';
		this.app = express();

		this.app.locals.parent = this;
		this.app.disable('x-powered-by');
		this.app.enable('trust proxy');
		console.log('trust proxy is ' + (this.app.get('trust proxy')? 'on' : 'off'));

		this.app.use(express.json({type: "application/json", strict: false, limit: '10mb', verify: (req, res, buf, encoding) => {
			req.rawBody = buf;
		}}));
		this.app.use(express.urlencoded({ type: "application/x-www-form-urlencoded", extended: true }));
		this.app.use(express.text({type: "text/*", limit: '10mb'}));
		this.app.use(express.raw({type: ["image/*", "application/*", "binary/*"], limit: '10mb'}));
		this.app.use((req, res, next) => {
			res.set("Access-Control-Allow-Origin", "*");
			res.set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, PATCH, DELETE");
			res.set("Access-Control-Allow-Headers", "Content-Type");
			next();
		});

		if (process.env.NODE_ENV != "production" && process.env.NODE_ENV != "test") {
			// Log request bodies
			this.app.use((req, res, next) => {
				console.log(`${(new Date).toISOString()}\t${req.method}  ${req.url}`);
				if (["put", "post", "patch"].includes(req.method)) console.log(req.body);
				next();
			});
		}

		// Adds arbitrary information to the request object
		const meta = (key, value) => {
			return (req, res, next) => {
				if (!req.meta) {
					req.meta = {};
				}

				if (key) {
					req.meta[key] = value;
				}

				next();
			}
		};

		// Short for adding meta to all methods
		const allMeta = (key, value) => {
			return { all: [ meta(key, value) ] };
		};

		map(endpoints, undefined, this.app, this.verbose);
		this.app.use(HttpApi.genericErrorHandler);

		this.root = this.app;
		if (this.settings.path) {
			this.root = express();
			['x-powered-by', 'trust proxy'].forEach(k => this.root.set(k, this.app.get(k)));
			this.root.use(this.settings.path, this.app);
		}

		const httpsMode = this.settings?.options?.cert;

		this.server = httpsMode
			? https.createServer(this.settings.options, this.root).listen(this.settings.port, this.settings.host)
			: http.createServer(this.settings?.options, this.root).listen(this.settings.port, this.settings.host);

		if (this.server) {
			console.log(`API started on port ${this.settings.port}, prefix: ${this.settings.path || "<none>"}, mode: ${httpsMode? "HTTPS" : "HTTP"}`);
		}

		return this.server;
	}

	async stop () {
		const shutdown = () => {
			return new Promise ((resolve, reject) => {
				if (this.server) {
					this.server.close(resolve);
				} else {
					process.nextTick(resolve);
				}
			});
		}

		await shutdown();
		delete this.server;
		delete this.root;
		delete this.app;
	}

	defaultEndpoints () {
		return {
			"/": {
				get: (req, res, next) => { res.json("HTTP API"); next(); },
			},
			"*": {
				all: (req, res, next) => {
					this.emit('request', req);
					res.status(202).end();
					next();
				}
			}
		}
	}

	// Generic error handler. Stops stack dumps
	// being sent to clients.
	static genericErrorHandler (err, req, res, next) {
		const title = `HTTP backend error at ${req.method} ${req.originalUrl}`;
		const description = err.message;
		const message = err.message;
		const alert = {title, message, description, error: err};

		console.error("Error:", err);

		if (err instanceof Error && err.name != "UnauthorizedError") {
			console.error(err.stack);
			res.status(500).send('General internal error');
		} else if (typeof err === 'string') {
			res.status(500).send({message: err});
		} else {
			res.status(err.status || 500).send({message: err.message || (err.inner && err.inner.message) || "Internal error"});
			if (!res.status) {
			}
		}
	};

}

module.exports = HttpApi;
