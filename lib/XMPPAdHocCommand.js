const {xml} = require('@xmpp/component')
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const {getSender, getComponent} = require('~/lib/utils/component');

const defaults = {
	lifetime: 60*3600*1000,
	throttle: 1000, // Minimum interval between purges
	maxSessions: 1000
}

class Sessions {
	constructor (settings = {}) {
		settings = Object.assign({}, defaults, settings);

		this.sessions = {};
		this.lastPurge = Date.now();
		this.lifetime = settings.lifetime;
		this.throttle = settings.throw;
		this.maxSessions = settings.maxSessions;
		this.jid = settings.jid;
		this.node = settings.node;
		this.name = settings.name;
	}

	get (sessionid) {
		this.purge();

		if (!(sessionid in this.sessions)) {
			if (sessionid === undefined) {
				if (Object.keys(this.sessions).length < this.maxSessions) {
					do {
						sessionid = Math.random().toString().substring(2);
					} while (sessionid in this.sessions);
				} else {
					return;
				}
			}

			this.sessions[sessionid] = {
				id: sessionid,
				tstamp: Date.now(),
				stage: 0,
				data: {}
			}
		}

		return this.sessions[sessionid];
	}

	save (session) {
		if ("id" in session) {
			this.sessions[session.id] = session;
		}
	}

	destroy (session) {
		delete this.sessions[session.id];
	}

	purge () {
		const now = Date.now();
		if ((now - this.lastPurge) > this.throttle) {
			for (const id in this.sessions) {
				const age = now - this.sessions[id].tstamp;
				if (age > this.lifetime) {
					this.destroy(session);
				}
			}
			this.lastPurge = now;
		}
	}
}

class XMPPAdHocCommand {
	constructor ({parent, jid, node, name}, stages = []) {
		this.parent = parent;
		this.stages = stages;
		this.info = {jid, node, name};
		this.sessions = new Sessions();
		this.root = getComponent(this);
	}

	/** Check if the request will be allowed.
	 *
	 * Returns true if the user is allowed to proceed with the
	 * request, false otherwise. By default it denies access to
	 * everyone except parent.owner (if exists). It should be
	 * overriden as appropriate in subclasses.
	 */
	isUserAllowed (ctx) {
		try {
			const sender = getSender(ctx);
			if (this.parent?.owner && this.parent.owner == sender) {
				return true;
			}
			if (this.parent?.isUserAllowed && this.parent.isUserAllowed(ctx)) {
				return true;
			}
			return false;
		} catch (err) {
			console.error(err);
			console.error("CONTEXT");
			console.error(ctx);
			throw err;
		}
	}

	discoItem (ctx) {
		if (this.isUserAllowed(ctx)) {
			return xml('item', this.info);
		}
	}

	async despatch (ctx) {
		if (!this.isUserAllowed(ctx)) {
			console.warn("Forbidden");
			return xmppError(ctx.stanza, "forbidden");
		}

		const session = this.sessions.get(ctx.element.attrs.sessionid);
		const action = ctx.element.attrs.action || "next"

		if(!session) {
			const msg = "Too many commands running at the same time. Try later";
			return xmppError(ctx.stanza, "resource-constraint", msg);
		}

		this.saveFormData(ctx, session);

		switch (action) {
			case "next":
			case "execute":
				return await this.next(ctx, session);
			case "prev":
				return await this.prev(ctx, session);
			case "cancel":
				return await this.cancel(ctx, session);
			case "complete":
				return await this.last(ctx, session);
			default:
				// Error?
		}
	}

	// Save the submitted form data into session.data.form, making it available
	// to the latter stages of the command.
	saveFormData (ctx, session) {

		const cmd = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
		if (cmd) {

			const x = cmd.getChild("x", "jabber:x:data");
			if (x && x.attrs.type == "submit") {

				const form = XMPPForm.submit(x);
				if (!session.data.form) {
					session.data.form = {};
				}
				session.data.form = {...session.data.form, ...form};
			}
		}
	}

	async next (ctx, session) {
		const stage = this.stages[session.stage];

		if (stage) {
			const response = await stage(this, ctx, session);
			session.stage++;
			this.sessions.save(session);
			return response;
		}
	}

	async prev (ctx, session) {
		session.stage = Math.max(session.stage-2, 0);
		return await this.next(ctx, session);
	}

	async last (ctx, session) {
		session.stage = this.stages.length-1;
		return await this.next(ctx, session);
	}

	async cancel (ctx, session) {
		this.sessions.destroy(session);

		const attrs = Object.assign({}, ctx.element.attrs, {status: "canceled"});
		return xml('command', attrs);
	}

	/** Save the components state.
	 *
	 * This commits any changes that the command may have done to
	 * the component or any of its children.
	 */
	async commit () {
		await this.root.savePersisted();
	}

	/** Send a message to the invoker of this command.
	 */
	sendMessage (ctx, session, body, opts = {}) {
		const sender = getSender(ctx);
		const addressee = ctx.to.bare().toString();

		const msg = xml(
			'message',
			{
				type: opts.type ?? "chat",
				from: addressee,
				to: sender
			},
			xml('subject', {}, opts.subject),
			xml('body', {}, body)
		);

		this.root.sendStanza(msg);
	}

	/** Show a note.
	 */
	note (session, msg, type = "info") {
		return xml(
			'command', {
				xmlns: 'http://jabber.org/protocol/commands',
				node: this.info.node,
				sessionid: session.id,
				status: 'executing'
			},
			xml(
				'actions',
				{ execute: "prev" },
				xml('prev'),
				xml('cancel')
			),
			xml(
				'note',
				{ type },
				msg
			)
		);
	}

	/** Complete this command and show a message in a note.
	 */
	complete (session, msg, type = "info", opts = {}) {

		const output = xml(
			'command', {
				xmlns: 'http://jabber.org/protocol/commands',
				node: this.info.node,
				sessionid: session.id,
				status: 'completed'
			}
		);

		if (msg) {
			output.append(xml(
				'note',
				{ type },
				msg
			));
		}

		if (opts.title || opts.instructions || opts.fields) {
			output.append(XMPPForm.form({
				title: opts.title ?? "Command results",
				instructions: opts.instructions,
				fields: opts.fields
			}));
		}

		return output;
	}

}

module.exports = XMPPAdHocCommand;
