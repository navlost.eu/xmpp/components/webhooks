
module.exports = [
	require('./new'),
	require('./add-action-set-templates.js'),
	require('./reapply-action-set-templates.js'),
	require('./download-action-set-templates.js'),
	require('./delete-action-set-templates.js')
]
