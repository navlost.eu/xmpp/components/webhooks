const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const ActionSetTemplate = require('~/lib/ActionSetTemplate');
const ActionSet = require('~/lib/ActionSet');
const {getSender, filterNodes, getComponent} = require('~/lib/utils/component');

function selectTemplates (self, ctx, session) {

	session.data.astNodes = filterNodes(getComponent(self), (i => i instanceof ActionSetTemplate));
// 	console.log("ASTNODES", session.data.astNodes);

	if (!session.data.astNodes?.length) {
		return self.complete(session, "There are no templates");
	}

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('next', {})
		),
		XMPPForm.form({
			title: "Re-apply action set templates",
			instructions: "Use this command to update action sets to "+
				"the latest definition of their respective templates. "+
				"Ensure that the template definitions (especially variables "+
				"and parameters) are actually compatible before running this "+
				"command!",
			fields: Object.fromEntries(session.data.astNodes.map(i => [
				i.id, {
					type: "boolean",
					label: i.name,
					desc: i.description
				} ]))
		})
	);
}

function selectInstances (self, ctx, session) {

	const selectedNodes = session.data.astNodes.filter(i => session.data.form[i.id]);
	const templateIds = selectedNodes.map(i => i.id);
	const fn = (i) => i instanceof ActionSet && templateIds.includes(i.id);
	session.data.asiNodes = filterNodes(getComponent(self), fn);
// 	console.log("ASINODES", session.data.asiNodes.map(i=>i.node));

	if (!selectedNodes.length) {
		return self.complete(session, "No templates have been selected");
	}

	if (!session.data.asiNodes.length) {
		return self.complete(session, "There are no instances of the selected templates");
	}

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('prev', {}),
			xml('next', {})
		),
		XMPPForm.form({
			title: "Select nodes to refresh",
			instructions: "Select the action sets for which you would "+
				"like to re-apply one of the selected templates.",
			fields: Object.fromEntries(session.data.asiNodes.map(i => [
				i.jid+"/"+i.node, {
					type: "boolean",
					label: `${i.jid}: ${i.name}`,
					desc: `${i.jid} ${i.id} ${i.prefix}`
				} ]))
		})
	);
}

function execute (self, ctx, session) {
	const sender = getSender(ctx);

	// Only the component administrator is allowed to add action set templates.
	// In theory, we could also let users (subject to administrator authorisation)
	// upload their own templates, but this is complex enough as it is already.
	if (sender != self.parent.settings.owner) {
		// Not authorised
		return xmppError(ctx.stanza, "forbidden");
	}

	const selectedInstances = session.data.asiNodes.filter(i => session.data.form[i.jid+"/"+i.node]);

	if (!selectedInstances.length) {
		return self.complete(session, "No instances have been selected");
	}

	selectedInstances.forEach( instance => {
		const template = session.data.astNodes.find(i => i.id == instance.id);
		if (template) {
			instance.version = template.version;
			instance.commands = template.commands;
			instance.aliases = template.aliases;
			instance.nodes = []; // FIXME not really a good idea, should remove only commands
			instance.loadNodes(instance.commands);
// 			console.log("UPDATED", instance.jid, instance.node, "→", instance.version);
		}
	});
// 	console.log("FORM DATA", session.data.form);
	self.commit(); // Save state

	return self.complete(session, `Action sets refreshed:\n${selectedInstances.map(i => `• ${i.name} (${i.id} ${i.prefix})`).join("\n")}`);
}

class ReapplyActionSetTemplatesCommand extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "https://navlost.eu/xmpp/components/webhooks/actions/sets/templates#reapply";
		if (!settings.name) settings.name = "Re-apply action set templates";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			selectTemplates,
			selectInstances,
			execute
		];

	}
}

module.exports = ReapplyActionSetTemplatesCommand;

