const YAML = require('yaml');
const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const ActionSetTemplates = require('~/lib/ActionSetTemplates');

function select (self, ctx, session) {

	session.data.astParent = self.parent.nodes.find(n => n.node == ActionSetTemplates.node);
	session.data.astNodes = session.data.astParent?.nodes ?? [];

	if (!session.data.astNodes?.length) {
		return self.complete(session, "There are no templates");
	}

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('next', {})
		),
		XMPPForm.form({
			title: "Download action set templates",
			instructions: `Select the templates to download`,
			fields: {
				...Object.fromEntries(session.data.astNodes.map(i => [
					"$"+i.id, {
						type: "boolean",
						label: i.name,
						desc: i.description
					} ])),
				destinations: {
					type: "list-multi",
					label: "Download methods",
					options: [
						{label: "View in text box", value: "view"},
						{label: "Send as message", value: "message"},
						{label: "Send as attachment", value: "file"}
					],
					required: true
				},
				format: {
					type: "list-single",
					label: "Format",
					options: [
						{label: "YAML", value: "yaml"},
						{label: "JSON", value: "json"}
					],
					required: true
				}
			}
		})
	);
}

function execute (self, ctx, session) {
// 	console.log("SESSION FORM", session.data.form);
	const selected = session.data.astNodes.filter(i => session.data.form["$"+i.id]).map(i => i.toJSON());
	let output = Object.fromEntries(selected.map(i => {
		const id = i.id;
		delete i.node;
		delete i.id;
		return [ id, i ];
	}));

	if (session.data.form.format == "yaml") {
		output = YAML.stringify(output);
	} else {
		output = JSON.stringify(output, null, 4);
	}

	if (session.data.form.destinations.includes("message")) {
		self.sendMessage(ctx, session, output, {type: "headline", subject: "Action set templates"});
	}

	if (session.data.form.destinations.includes("file")) {
// 		console.log("Sending file is not yet supported");
	}

	const opts = {}
	if (session.data.form.destinations.includes("view")) {
		opts.fields = {
			output: {
				type: "text-multi",
				label: "Template",
				value: output
			},
			format: {
				label: "Format",
				value: session.data.form.format
			}
		}
	}

	return self.complete(session, "Download completed", "info", opts);
}

class DownloadActionSetTemplatesCommand extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "https://navlost.eu/xmpp/components/webhooks/actions/sets/templates#download";
		if (!settings.name) settings.name = "Download action set templates";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			select,
			execute
		];

	}
}

module.exports = DownloadActionSetTemplatesCommand;

