const YAML = require('yaml');
const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const ActionSetTemplates = require('~/lib/ActionSetTemplates');
const {getSender, getComponent} = require('~/lib/utils/component');


function showForm (self, ctx, session) {
	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "complete" },
			xml('complete', {})
		),
		XMPPForm.form({
			title: "Add action set template",
			instructions: `Paste a template defining one or more action sets in JSON or YAML format`,
			fields: {
				text: {
					type: "text-multi",
					label: "Template text",
					required: true
				},
				format: {
					type: "list-single",
					label: "Format",
					desc: `The format in which the template definitions are provided`,
					options: [
						{
							label: "YAML",
							value: "YAML"
						},
						{
							label: "JSON",
							value: "JSON"
						}
					],
					required: true
				}
			}
		})
	);
}

async function processForm (self, ctx, session) {
	const addressee = ctx.to.bare().toString();
	const sender = getSender(ctx);

	// Only the component administrator is allowed to add action set templates.
	// In theory, we could also let users (subject to administrator authorisation)
	// upload their own templates, but this is complex enough as it is already.
	if (sender != self.parent.settings.owner) {
		// Not authorised
		return xmppError(ctx.stanza, "forbidden");
	}

	// NOTE: Technically, we should probably check that the addressee is a sensible
	// value.

	const cmd = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
	if (!cmd) {
		return xmppError(ctx.stanza, "bad-request");
	}

	const x = cmd.getChild("x", "jabber:x:data");
	if (x && x.attrs.type == "submit") {
		const form = XMPPForm.submit(x);

		if (form.text && form.format) {
			let definition;

			try {
				if (form.format == "YAML") {
					definition = YAML.parse(form.text);
				} else if (form.format == "JSON") {
					definition = JSON.parse(form.text);
				} else {
					return xml(
						'command', {
							xmlns: 'http://jabber.org/protocol/commands',
							node: self.info.node,
							sessionid: session.id,
							status: 'completed'
						},
						xml(
							'actions',
							{ execute: "prev" },
							xml('prev'),
							xml('cancel')
						),
						xml(
							'note',
							{ type: "error" },
							`The format value supplied is not valid.`
						)
					);
				}
			} catch (err) {
				console.error(err);
				return xml(
					'command', {
						xmlns: 'http://jabber.org/protocol/commands',
						node: self.info.node,
						sessionid: session.id,
						status: 'completed'
					},
					xml(
						'actions',
						{ execute: "prev" },
						xml('prev'),
						xml('cancel')
					),
					xml(
						'note',
						{ type: "error" },
						`The template supplied could not be parsed as ${form.format}.`
					)
				);
			}

			// Having reached this point, we should have a definition which is a valid JSON
			// object. Whether it actually specifies a valid action set template is entirely
			// another matter though.

			const parentNode = self.parent.nodes.find(n => n.node == ActionSetTemplates.node);
			if (parentNode) {
				try {
					parentNode.loadNodes(definition);
					self.commit(); // Save state
				} catch (err) {
					console.error(err);
					return xml(
						'command', {
							xmlns: 'http://jabber.org/protocol/commands',
							node: self.info.node,
							sessionid: session.id,
							status: 'completed'
						},
						xml(
							'actions',
							{ execute: "prev" },
							xml('prev'),
							xml('cancel')
						),
						xml(
							'note',
							{ type: "error" },
							// We really want to be more explicit than this.
							`The template has invalid definitions.`
						)
					);
				}
			} else {
				// TODO Can we have components not supporting actions? Why not
				return xmppError(ctx.stanza, "item-not-found");
			}

			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				// We add this because of a bug in Psi+
				// (https://github.com/psi-im/psi/issues/340)
				// but not sure if I would like to keep both the
				// form and the note. TODO check on other clients.
				XMPPForm.form({
					title: "Command results",
					instructions: "Template loaded successfully."
				}),
				xml(
					'note',
					{ type: "info" },
					"Template loaded successfully."
				)
			);

		} else {
			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				xml(
					'actions',
					{ execute: "prev" },
					xml('prev'),
					xml('cancel')
				),
				xml(
					'note',
					{ type: "error" },
					`You must supply both a template definition and specify its type.`
				)
			);
		}
	} else {
		return xmppError(ctx.stanza, "bad-request");
	}
}

class WebhookComponentCommandAddActionSetTemplates extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "https://navlost.eu/xmpp/components/webhooks/actions/sets/templates#add";
		if (!settings.name) settings.name = "Add action set templates";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			showForm,
			processForm
		];
	}

	discoItem (ctx) {
		const sender = getSender(ctx);
		if (sender == this.parent.settings.owner) {
			// Only show this command to the instance administrator
			return super.discoItem(ctx);
		}
	}

}

module.exports = WebhookComponentCommandAddActionSetTemplates;

