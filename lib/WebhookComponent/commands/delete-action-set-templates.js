const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const ActionSetTemplates = require('~/lib/ActionSetTemplates');
const {getSender, getComponent} = require('~/lib/utils/component');

function select (self, ctx, session) {

	session.data.astParent = self.parent.nodes.find(n => n.node == ActionSetTemplates.node);
	session.data.astNodes = session.data.astParent?.nodes ?? [];

	if (!session.data.astNodes?.length) {
		return self.complete(session, "There are no templates to delete");
	}

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('next', {})
		),
		XMPPForm.form({
			title: "Delete action set templates",
			instructions: `Select the templates to delete`,
			fields: Object.fromEntries(session.data.astNodes.map(i => [
				i.id, {
					type: "boolean",
					label: i.name,
					desc: i.description
				} ]))
		})
	);
}

function execute (self, ctx, session) {
	const sender = getSender(ctx);

	// Only the component administrator is allowed to add action set templates.
	// In theory, we could also let users (subject to administrator authorisation)
	// upload their own templates, but this is complex enough as it is already.
	if (sender != self.parent.settings.owner) {
		// Not authorised
		return xmppError(ctx.stanza, "forbidden");
	}

	const newSet = session.data.astNodes.filter(i => !session.data.form[i.id]);

	session.data.astParent.nodes = newSet;
	self.commit(); // Save state

	return self.complete(session, `Templates deleted:\n${session.data.astNodes.filter(i => session.data.form[i.id]).map(i => `• ${i.name} (${i.id})`).join("\n")}`);
}

class DeleteCommand extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "https://navlost.eu/xmpp/components/webhooks/actions/sets/templates#delete";
		if (!settings.name) settings.name = "Delete action set templates";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			select,
			execute
		];
	}

	discoItem (ctx) {
		const sender = getSender(ctx);
		if (sender == this.parent.settings.owner) {
			// Only show this command to the instance administrator
			return super.discoItem(ctx);
		}
	}
}

module.exports = DeleteCommand;

