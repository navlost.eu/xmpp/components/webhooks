const uuid = require('uuid').v4;
const {xml, jid} = require('@xmpp/component')

const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');

const Project = require('~/lib/Project');

function sendProjectForm (self, ctx, session) {

	if (!session.data.form) {
		session.data.form = {
			allowed: '^.*$'
		};
	}

	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "complete" },
			xml('complete', {})
		),
		XMPPForm.form({
			title: "Add new project",
			instructions: `You are about to create an endpoint for receiving webhook notifications. After filling in the required data (most fields are optional) you will be given the endpoint URL to use in your webhooks. This will create an item with a JID of \`handle\`@${self.info.jid}, where \`handle\` is the value that you will enter in the ‘handle’ field below. Authorised users can then subscribe to that JID to receive event updates. You, as the JID owner, will be able to edit its configuration or delete it altogether. Please note that if you unregister from the service ALL ASSOCIATED PROJECTS WILL BE DELETED.`,
			fields: {
				handle: {
					label: "Project handle",
					desc: "This will be the local part of your project's JID",
					required: true
				},
				name: {
					label: "Project name",
					desc: "A brief, human readable description of your project",
					required: true
				},

				url: {
					label: "Homepage",
					desc: "A URL with a description of the project, presented in the entity's vCard"
				},
				email: {
					label: "Email",
					desc: "An email associated with this project, e.g., where users can email patches in the case of a Git repository"
				},
				description: {
					type: "text-multi",
					label: "Description",
					desc: "An extended free text description of this project. It will be shown in the vCard."
				},

				notifications: {
					type: "boolean",
					label: "Receive chat messages",
					desc: "If enabled, chat messages sent to this JID will be returned by the HTTP API to the caller. It can be used to have a rudimentary form of two-way communications."
				},

				header0: {
					type: "fixed",
					value: "Optional chatroom settings"
				},

				autojoin: {
					type: "boolean",
					label: "Accept chatroom invitations",
					desc: "If enabled, invitations from members to join chatrooms will be automatically accepted, provided that the chatroom JID matches the ‘Allowed subscribers’ field below."
				},
				nick: {
					label: "Nick",
					desc: "Nickname that will be used when automatically joining chatrooms following an invitation."
				},

				header1: {
					type: "fixed",
					value: "Optional authentication parameters"
				},

				username: {
					label: "Auth username",
					desc: "If you want to protect your webhook endpoint using HTTP Basic Authentication, enter a username here"
				},
				password: {
					label: "Auth password",
					desc: "If you want to protect your webhook endpoint using HTTP Basic Authentication, enter a password here"
				},

				tokenName: {
					label: "Secret token name",
					desc: "The name of an HTTP header containing a secret token to validate received payloads, e.g., ‘X-Gitlab-Token’ for GitLab webhooks"
				},
				tokenValue: {
					label: "Secret token value",
					desc: "The value expected in the secret token header"
				},

				header2: {
					type: "fixed",
					value: "Access restrictions"
				},

				allowed: {
					label: "Allowed subscribers",
					desc: "Only JIDs matching this regular expression will be allowed to subscribe to this node. Leave the default to allow everyone to subscribe.",
					value: "^.*$"
				}
			}
		})
	);
}

async function createNewProject (self, ctx, session) {
	const sender = ctx.from.bare().toString();

	const cmd = ctx.stanza.getChild("command", "http://jabber.org/protocol/commands");
	if (!cmd) {
		return; // FIXME Should return malformed or something
	}
	const x = cmd.getChild("x", "jabber:x:data");
	if (x && x.attrs.type == "submit") {
		const form = XMPPForm.submit(x);
		session.data.form = form;
		if (!form.handle || !form.name) {
			return xml(
				'command', {
					xmlns: 'http://jabber.org/protocol/commands',
					node: self.info.node,
					sessionid: session.id,
					status: 'completed'
				},
				// TODO Offer "prev" action
				xml(
					'note',
					{ type: "error" },
					"A required element is missing. Please provide both a handle and a name."
				)
			);
		}

		const projectJID = jid(form.handle, self.parent.jid).toString();
		const existingItem = self.parent.items.find(i => i.jid == projectJID);
		if (existingItem) {
			if (existingItem.owner != sender) {
				return xml(
					'command', {
						xmlns: 'http://jabber.org/protocol/commands',
						node: self.info.node,
						sessionid: session.id,
						status: 'completed'
					},
					// TODO Offer "prev" action
					xml(
						'note',
						{ type: "error" },
						`The handle ${form.handle} is already in use by someone else. Please try a different value.`
					)
				);
			} else {
				// Updating existing project
				// TODO Ask for confirmation of update
			}
		}

		form.jid = projectJID;
		form.owner = sender;
		form.uuid = uuid();
		form.parent = self.parent;
		form.notificationsQueue = [];
		if (Array.isArray(form.description)) {
			form.description = form.description.join("\n");
		}
		if (existingItem) {
			// We are replacing an existing endpoint

			// Save the UUID
			form.uuid = existingItem.uuid;

			if (form.notifications) {
				// Do not throw away any pending notifications
				form.notificationsQueue = existingItem.notificationsQueue;
			}

			// Delete the old item (subscriptions will not be affected)
			await self.parent.removeItem(existingItem.uuid);
		}
		const url = (self.parent.settings?.http?.url || "{id}").replace('{id}', form.uuid);
		const newItem = new Project(form);
		await self.parent.addItem(newItem);

		const msg0 = xml(
			'message',
			{
				type: "headline",
				from: projectJID,
				to: sender
			},
			xml('subject', {}, "Your new project has been created"),
			xml('body', {}, `The new project has been created.\nThe webhook endpoint is:\n${url}\n`)
		);
		const msg1 = xml(
			'message',
			{
				type: "headline",
				from: projectJID,
				to: sender
			},
			xml('subject', {}, "Subscription URL for notifications"),
			xml('body', {}, `xmpp:${projectJID}?roster;name=${encodeURIComponent(form.name)}`)
		);


		setTimeout( ()=> self.parent.xmpp.send([msg0, msg1]), 500);

		return xml(
			'command', {
				xmlns: 'http://jabber.org/protocol/commands',
				node: self.info.node,
				sessionid: session.id,
				status: 'completed'
			},
			xml(
				'note',
				{ type: "info" },
				`The new project has been created. The webhook endpoint is: ${url}`
			)
		);
	} else {
		return xmppError(ctx.stanza, "bad-request");
	}
}

class WebhookComponentCommandNew extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = "new";
		if (!settings.name) settings.name = "Add a project";
		super(settings);

		this.parent = settings.parent;

		this.stages = [
			sendProjectForm,
			createNewProject
		];
	}

	discoItem (ctx) {
		if (this.parent.registeredUser(ctx)) {
			return super.discoItem(ctx);
		}
	}

}

module.exports = WebhookComponentCommandNew;
