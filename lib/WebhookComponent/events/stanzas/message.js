const {xml, jid} = require('@xmpp/component')
const xmppError = require('~/lib/xmpp-error');
const messageHandler = require('~/lib/ActionSetCommand/message-handler');

// The maximum lag that we will accept before ignoring groupchat messages
const MAX_GROUPCHAT_DELAY = 3*60*1000; // 3 minutes

async function message (stanza) {
	if (stanza.is('message') && (stanza.attrs.type == "chat" || stanza.attrs.type == "groupchat")) {

		const delay = stanza.getChild("delay", "urn:xmpp:delay")
		if (delay) {
			const lag = Date.now() - new Date(delay.attrs.stamp)
			if (stanza.attrs.type == "groupchat" && lag > MAX_GROUPCHAT_DELAY) {
				// Ignore archived message
				return;
			}
		}

		const sender = jid(stanza.attrs.from).bare().toString();
		const addressee = jid(stanza.attrs.to).bare().toString();
		const item = this.items.find(i => i.jid == addressee);
		if (item) {
			if (messageHandler(stanza, item)) {
				// Either there are no action handlers or this wasn't
				// recognised as a command (or an attempt to give one).
				return;
			} else {
				// Push to notifications if so configured
				if (item.notifications) {
					const subscription = this.subscriptions.find(i => i.from == sender && i.to == addressee);
					if (subscription) {
						item.notificationsQueue.push(stanza);
						this.savePersisted();
						this.receipts(stanza);
					}
				}
			}
		}
	}
}

module.exports = message;
