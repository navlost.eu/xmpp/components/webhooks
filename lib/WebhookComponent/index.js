const {component, xml, jid} = require('@xmpp/component');
const GenericComponent = require('~/lib/GenericComponent');
const commands = require('./commands');
const nodes = require('./nodes');
const events = require('./events');
const {getJID} = require('~/lib/xml-utils');

const Project = require('~/lib/Project');
const ActionSetTemplates = require('~/lib/ActionSetTemplates');

class WebhookComponent extends GenericComponent {

	PUBSUB_NODE = "http://xmpp.navlost.eu/webhooks";

	constructor ({settings}) {
		super({settings});

		// Component-level commands (wcc = WebhookComponentCommands)
		const wcc = commands.map(c =>
			new c({parent: this, jid: settings.xmpp.domain})
		);
		// Default commands (gcc = GenericComponentCommands)
		// Remove any which are re-implemented by the derived class
		const gcc = this.commands.filter(c =>
			!wcc.some(i => i.info.node == c.info.node && i.info.jid == c.info.jid))
		// Merge it all
		this.commands = [ ...wcc, ...gcc ];

		this.#createDefaultNodes();

		// Component-level events (supplement defaults)
		for (const event in events) {
			if (!Array.isArray(this.events[event])) {
				this.events[event] = [ this.events[event] ];
			}
			this.events[event].push(events[event]);
		}

	}

	#createDefaultNodes () {
		for (const DefaultNodeFactory of nodes) {
			if (!this.nodes.find(n => n.node == DefaultNodeFactory.node)) {
				this.nodes.push( new DefaultNodeFactory({parent: this}) );
			}
		}
	}

	async discoInfo (ctx) {
		const res = await super.discoInfo (ctx);
		res.append(
			xml(
				'feature',
				{ 'var': 'vcard-temp' }
			)
		);
		return res;
	}

	vCardTemp (ctx) {
		const url = "https://gitlab.com/navlost.eu/xmpp/components/webhooks";
		const email = "contact-project+navlost-eu-xmpp-components-webhooks-24572215-issue-@incoming.gitlab.com";

		return xml("vCard", "vcard-temp",
			xml("FN", {}, "Webhook XMPP Component"),
			xml("URL", {}, url),
			xml("EMAIL", {},
				xml("INTERNET"),
				xml("PREF"),
				xml("USERID", {}, email)
			),
			xml("DESC", {}, "This component lets authorised users create “projects” which provide a webhook endpoint to receive notifications on. Other XMPP users can subscribe to the project JID and then receive notifications on webhook events. See the website for more information."),
		);
	}

	async readPersisted (store) {
		await super.readPersisted(store);
		this.items = this.items.map(i => new Project({parent:this, ...i}));
		this.nodes = this.nodes.map(n => {
			const Factory = nodes.find(j => j.node == n.node);
			if (Factory) {
				return new Factory({parent:this, ...n});
			}
		})
		.filter(n => !!n);
		this.#createDefaultNodes(); // In case they were not in the saved configuration
	}

	subscriptionAllowed(ctx) {
		const from = getJID(ctx).from.bare().toString();
		const to = getJID(ctx).to.bare().toString();

		const item = this.items.find(i => i.jid == to);
		if (item) {
			if (item.allowed) {
				const rx = new RegExp(item.allowed);
				return rx.test(from);
			} else {
				// Allow all if item.allowed missing or empty
				return true;
			}
		}
		// Don't allow subscriptions to non-existing projects
		return false;
	}

	async sendWebhookNotification (item, message) {

		const stanza = xml(
			'message', {type: message.type ?? "headline"},
			xml('subject', {}, message.subject)
		);

		if (message["urn:xmpp:attention:0"]) {
			stanza.append(xml("attention", "urn:xmpp:attention:0"));
		}

		if (message["urn:xmpp:hints"]) {
			for (hint in message["urn:xmpp:hints"]) {
				stanza.append(xml(hint, "urn:xmpp:hints"));
			}
		}

		const upload = message.upload;
		if (upload?.body && upload?.filename && upload?.mimetype) {
			const url = await this.httpUpload.upload(upload.body, upload.filename, upload.mimetype);
			if (url) {
				stanza.append(xml("x", "jabber:x:oob", xml("url", {}, url)));
				if (!message.body) {
					message.body = url;
				} else {
					message.body += "\n\n" + url;
				}
			}
		} else if (message.url) {
			stanza.append(xml("x", "jabber:x:oob", xml("url", {}, message.url)));
		}

		stanza.append(xml('body', {}, message.body));

		await this.sendToItemSubscribers(stanza, item);
	}

}

module.exports = WebhookComponent;
