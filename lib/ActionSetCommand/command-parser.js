const fetch = require('node-fetch');
const {xml, jid} = require('@xmpp/component')
const {getComponent} = require('~/lib/utils/component');
const xmppError = require('~/lib/xmpp-error');
const render = require('./render');


class ActionSetCommandParser {
	constructor (settings) {
		if (!settings.node) settings.node = settings.parent.node;
		if (!settings.name) settings.name = settings.parent.name;

		this.parent = settings.parent;
		this.root = getComponent(this);
	}

	isUserAllowed (ctx) {
		return this.parent.isUserAllowed(ctx);
	}


	#sendStanza (stanza) {
		this.root.sendStanza(stanza);
	}

	/** Send a <composing> stanza to the user.
	 *
	 * This is just to give some feedback in case the request takes
	 * a little while to be processed. The compose state will change
	 * to <paused> after ten seconds.
	 *
	 * Returns a cancel() function that the user should use to clear
	 * the <paused> delay timer when the results are available. This
	 * is only done if the received message is of type chat or groupchat.
	 *
	 * TODO This screams of refactoring. Perhaps adding this to the
	 * GenericComponent class.
	 */
	#sendCompose (ctx) {
		if (ctx.stanza.attrs.type == "chat" || ctx.stanza.attrs.type == "groupchat") {
			const type = ctx.stanza.attrs.type ?? "chat";
			const stanza = xml(
				"message",
				{ to: ctx.from, from: ctx.to, type },
				xml("composing", "http://jabber.org/protocol/chatstates")
			);
			this.#sendStanza(stanza);
			const timer = setTimeout( () => {
				const stanza = xml(
					"message",
					{ to: ctx.from, from: ctx.to, type },
					xml("paused", "http://jabber.org/protocol/chatstates")
				);
				this.#sendStanza(stanza);
			}, 10000);
			return () => {
				clearTimeout(timer);
				const stanza = xml(
					"message",
					{ to: ctx.from, from: ctx.to, type },
					xml("active", "http://jabber.org/protocol/chatstates")
				);
				this.#sendStanza(stanza);
			}
		} else {
			return () => {
				// Do nothing
			}
		}
	}

	#sendReply (ctx, text) {
		const msg = xml(
			"message",
			{from: ctx.to, to: ctx.from, type: ctx.stanza.attrs.type ?? "headline"},
			xml("body", {}, text)
		);
		this.#sendStanza(msg);
	}

	async #sendCommand (ctx, parameters) {
		if (this.type && this.type != "REST") {
			const msgText = `Invalid command type: ${this.type}.\n\nAt present we can only handle REST commands.`;
			console.error(msgText);
			return false;
		}

		const url = render(this.parent, ctx, null, this.parent.url, {parameters});
		const options = {};
		options.method = render(this.parent, ctx, null, this.parent.method ?? "GET", {parameters});

		if (this.parent.headers) {
	// 		console.log("HEADERS", this.headers);
			options.headers = {};
			for (const key in this.parent.headers) {
				const tpl = this.parent.headers[key];
	// 			console.log("KEY", key);
	// 			console.log("TPL", tpl);
				const value = render(this.parent, ctx, null, tpl, {parameters});
				options.headers[key] = value;
			}
		}

		if (this.parent.payload) {
			options.body = render(this.parent, ctx, null, this.parent.payload, {parameters});
		}

		console.log("about to call fetch", url, options);
		const stopComposing = this.#sendCompose(ctx);
		const res = await fetch(url, options);
		stopComposing();
	// 	console.log("res §", res);
		let body;
		if (res) {
			const contentType = res.headers.get("Content-Type");
			console.log(res.headers);
			console.log("Content-Type", contentType);
			switch (this.decodeResponseAs) {
				case "json":
					body = await res.json();
					break;
				case "text":
					body = await res.text();
					break;
				case "binary":
					body = await res.arrayBuffer();
					break;
				case "ignore":
					break;
				default:
					if (!contentType) {
						// Ignore the body
						console.warn("No Content-Type header in reponse!");
					} else if (contentType.match(/^(application|text)\/(.*\+)?json/i)) {
						body = await res.json();
					} else if (contentType.match(/^text\/.+/i)) {
						body = await res.text()
					} else if (contentType.match(/^application\/xml/i)) {
						body = await res.text()
					} else {
						body = await res.arrayBuffer();
					}
			}
		}

		const response = render(this.parent, ctx, null, this.parent.response, {res, body, parameters});
		console.log("BODY", body);
		console.log("RESPONSE", response);
		this.#sendReply(ctx, response);
		return response;
	}

	parse (payload, stanza) {
		const ctx = {
			from: jid(stanza.attrs.from),
			to: jid(stanza.attrs.to),
			stanza
		};
		const parameters = Object.fromEntries(Object.keys(this.parent.parameters).map(k => [k, undefined]));
		// TODO.
		// Should parse according to the template's "parser" instructions
		// and return non-false if everything went well.
		// If something goes wrong, consider sending a message to the
		// user with details of the error.
		for (const key in this.parent.parameters) {
			const parameter = this.parent.parameters[key];
			const parser = parameter.parser;
			if (!parser) {
				// This parameter is not used when parsing message stanzas
				// (though it may be used in the ad-hoc command version).
				continue;
			}
			const data = {payload, stanza, parameters};
			const value = render(this.parent, ctx, null, parser, data);
			parameters[key] = value;
			if (parameter.validator) {
				const isValid = render(this.parent, ctx, null, parameter.validator, data);
				console.log("ISVALID", isValid);
				console.log("FOR", parameter, data);
				if (isValid !== "true") {
					this.#sendReply(ctx, `Could not validate parameter "${key}" as "${value}": illegal value in "${payload}".`);
					return false;
				}
			}
		}

		console.log("PARAMETERS", parameters);
		// Let us check that we got all the required parameters
		const missingParameters = Object.keys(this.parent.parameters).filter( key =>
			this.parent.parameters[key].required &&
				(parameters[key] === undefined || parameters[key] === "")
		);
		console.log("MISSING", missingParameters);
		console.log("DEFINITIONS", this.parent.parameters);
		if (missingParameters.length) {
			this.#sendReply(ctx, `Missing required parameters:\n${missingParameters.map(i => "• "+i).join("\n")}\n\nCommand description:\n${this.parent.description.replaceAll("<prefix>", this.parent.parent.prefix)}`);
			return false;
		}

		// If we got to this point, we should have all the information that we need to
		// actually run the command.
		this.#sendCommand(ctx, parameters); // Don't bother awaiting

		// Returning true means that the command was parsed correctly.
		// It doesn't say anything about whether it will run correctly.
		return true;
	}


}

module.exports = ActionSetCommandParser;

