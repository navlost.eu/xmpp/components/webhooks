const fetch = require('node-fetch');
const {xml, jid} = require('@xmpp/component')
const XMPPAdHocCommand = require('~/lib/XMPPAdHocCommand');
const XMPPForm = require('~/lib/XMPPForm');
const xmppError = require('~/lib/xmpp-error');
const render = require('./render');

/** Get command parameters from	 the user.
 *
 * Only used with commands that do have parameters.
 *
 * Shows a form to the user and gets the necessary parameters.
 *
 */
function getParameters (self, ctx, session) {
// 	console.log("ad-hoc getParameters", session);
	return xml(
		'command', {
			xmlns: 'http://jabber.org/protocol/commands',
			node: self.info.node,
			sessionid: session.id,
			status: 'executing'
		},
		xml(
			'actions',
			{ execute: "next" },
			xml('next', {})
		),
		XMPPForm.form({
			title: "Command parameters",
			instructions: `Please fill in the requested details`,
			fields: self.parent.parameters
		})
	);
}


/** Validate entered parameter values.
 *
 * Only used with commands that do have parameters, at least one of which
 * requires validation.
 *
 */
function validateParameters (self, ctx, session) {
// 	console.log("ad-hoc validateParameters", session);
	const required = [];
	const invalid = [];

	for (const key in session.data.form) {
		const value = session.data.form[key];
		const validator = self.parent.parameters?.[key]?.validator;
		if (validator) {
			const isValid = render(self.parent, ctx, session, validator);
			if (isValid != "true") {
				delete session.data.form[key];
				invalid.push(key);
			}
		}

		if (self.parent.parameters?.[key]?.required) {
			if (!session.data.form[key]) {
				required.push(key);
			}
		}
	}

	let error = "";
	if (invalid.length) {
		error += `The following parameters are invalid:\n${invalid.map(k => "• "+k+"\n")}\n\n`;
	}
	if (required.length) {
		error += `The following required parameters are missing:\n${required.map(k => "• "+k+"\n")}\n`;
	}
// 	console.log("invalid, require", invalid, required);
	if (error) {
		console.log("error note", error);
		return self.note(session, error, "error");
	} else {
// 		console.log("Call next");
		session.stage++;
		self.next(ctx, session); // Advance to the next command stage
	}

}

/** Despatch a request to the remote system.
 *
 * Prepares and sends a request to the remote system,
 * waits for the response, passes it through the relevant
 * templates and finally send the results to the user.
 *
 * Results are both presented as a note and sent as a message.
 *
 * At present this can only handle type: REST commands.
 */
async function sendCommand (self, ctx, session) {
// 	console.log("ad-hoc sendCommand", session);
	if (self.parent.type && self.parent.type != "REST") {
		return self.complete(session, `Invalid command type: ${self.parent.type}.\n\nAt present we can only handle REST commands.`, "error");
	}

	const url = render(self.parent, ctx, session, self.parent.url);
	const options = {};
	options.method = render(self.parent, ctx, session, self.parent.method ?? "GET");

	if (self.parent.headers) {
// 		console.log("HEADERS", self.parent.headers);
		options.headers = {};
		for (const key in self.parent.headers) {
			const tpl = self.parent.headers[key];
// 			console.log("KEY", key);
// 			console.log("TPL", tpl);
			const value = render(self.parent, ctx, session, tpl);
			options.headers[key] = value;
		}
	}

	if (self.parent.payload) {
		options.body = render(self.parent, ctx, session, self.parent.payload);
	}

// 	console.log("about to call fetch", url, options);
	const res = await fetch(url, options);
// 	console.log("res §", res);
	let body;
	if (res) {
		const contentType = res.headers.get("Content-Type");
// 		console.log(res.headers);
// 		console.log("Content-Type", contentType);
		switch (self.parent.decodeResponseAs) {
			case "json":
				body = await res.json();
				break;
			case "text":
				body = await res.text();
				break;
			case "binary":
				body = await res.arrayBuffer();
				break;
			case "ignore":
				break;
			default:
				if (!contentType) {
					// Ignore the body
					console.warn("No Content-Type header in reponse!");
				} else if (contentType.match(/^(application|text)\/(.*\+)?json/i)) {
					body = await res.json();
				} else if (contentType.match(/^text\/.+/i)) {
					body = await res.text()
				} else if (contentType.match(/^application\/xml/i)) {
					body = await res.text()
				} else {
					body = await res.arrayBuffer();
				}
		}
	}

	const response = render(self.parent, ctx, session, self.parent.response, {res, body});
	// TODO Send result as a message
// 	console.log("BODY", body);
// 	console.log("RESPONSE", response);
	self.sendMessage(ctx, session, response);
	return self.complete(session, response);

// 	return xml(
// 		'command', {
// 			xmlns: 'http://jabber.org/protocol/commands',
// 			node: self.info.node,
// 			sessionid: session.id,
// 			status: 'completed'
// 		},
// 		// We add this because of a bug in Psi+
// 		// (https://github.com/psi-im/psi/issues/340)
// 		// but not sure if I would like to keep both the
// 		// form and the note. TODO check on other clients.
// // 		XMPPForm.form({
// // 			title: "Command results",
// // 			instructions: "Actions added."
// // 		}),
// 		xml(
// 			'note',
// 			{ type: "info" },
// 			response
// 		)
// 	);

}


class ActionSetCommandAdHoc extends XMPPAdHocCommand {
	constructor (settings) {
		if (!settings.node) settings.node = settings.parent.node;
		if (!settings.name) settings.name = settings.parent.name;
		super(settings);

		this.parent = settings.parent;

		this.stages = [
		];

		if (this.parent.parameters && Object.keys(this.parent.parameters).length) {
			this.stages.push(getParameters);
// 			this.stages.push(validateParameters);
		}

		this.stages.push(sendCommand);
// 		console.log("CONSTRUCTED NEW COMMAND NODE", this);
	}

	isUserAllowed (ctx) {
		// TODO should check that the user has access to
		// the parent action set.
		return true
	}

}

module.exports = ActionSetCommandAdHoc;

