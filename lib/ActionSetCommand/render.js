const {xml, jid} = require('@xmpp/component')
const parse = require("@xmpp/xml/lib/parse");
const { toXML, toJSON } = require('~/lib/xml-utils');
const ejs = require('ejs');

function render (self, ctx, session, tpl, extraData = {}) {
	// Some functions we make available to template writers
	// WARNING Never load EJS templates from untrusted sources
	const $ = {};
	// An extended version of node-xmpp's xml()
	$.xml = Object.assign({}, xml, {parse, toJSON});
	$.path = require('path');

	const data = {
		variables: {...self.parent.variables},
		registration: {
			variables: {...self.parent.getUser(ctx)}
		},
		parameters: {...session?.data?.form},
		ctx,
		...extraData,
		$
	};
// 	console.log("RENDER");
// 	console.log(tpl);
// 	console.log(data);
// 	console.log("→", ejs.render(tpl, data));
	return ejs.render(tpl, data);
}

module.exports = render;
