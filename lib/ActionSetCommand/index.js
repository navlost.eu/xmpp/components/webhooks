const {xml} = require('@xmpp/component')
const AdHocCommand = require('./command-adhoc');
const Parser = require('./command-parser');


class ActionSetCommand {

	static node = "http://navlost.eu/xmpp/components/webhooks/actions/set/command";

	constructor (params = {}) {
		this.parent = params.parent;
		this.jid = this.parent.jid;

		this.id = params.id;
		this.type = params.type ?? "REST";
		this.name = params.name;
		this.description = params.description;
		this.parameters = params.parameters ?? {};
		this.url = params.url;
		this.method = params.method;
		this.headers = params.headers ?? {};
		this.payload = params.payload;
		this.response = params.response;
		this.decodeResponseAs = params.decodeResponseAs;

		// This needs to go at the bottom, as the command's constructor
		// accesses the properties of its parent.
		this.command = new AdHocCommand({parent: this});
		this.parser = new Parser({parent: this});
	}

	get node () {
		const url = new URL(ActionSetCommand.node);
		url.searchParams.set("id", this.parent.id);
		url.searchParams.set("prefix", this.parent.prefix);
		url.hash = '#'+this.id;
		return url.href;
	}

	discoInfo (ctx) {
		return xml('query', 'http://jabber.org/protocol/disco#info',
			xml(
				'identity',
				{
					category: 'automation',
					type: 'command-node',
					name: this.name
				}
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/disco#info' }
			),
			xml(
				'feature',
				{ 'var': 'http://jabber.org/protocol/commands' }
			),
		);
	}

	discoItem (ctx) {
		return xml('item', {jid: this.parent.jid, name: this.name, node: this.node});
	}

	toJSON () {
		return {
			node: ActionSetCommand.node,
			id: this.id,
			type: this.type,
			name: this.name,
			description: this.description,
			parameters: this.parameters,
			url: this.url,
			method: this.method,
			headers: this.headers,
			response: this.response,
			decodeResponseAs: this.decodeResponseAs
		};
	}
}

module.exports = ActionSetCommand;
