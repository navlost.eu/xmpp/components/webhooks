const {xml, jid} = require('@xmpp/component')
const ActionSets = require('~/lib/ActionSets');
const GenericComponent = require('~/lib/GenericComponent');


function sendMessage (component, text, from, to, type = "headline") {
	const message = xml(
		"message",
		{ to, from, type },
		xml("body", {},
			text
		)
	);

	component.sendStanza(message);
}

function showAvailablePrefixes (component, ctx, actionSets) {
	const sender = ctx.stanza.attrs.from;
	const addressee = ctx.stanza.attrs.to;
	const type = ctx.stanza.attrs.type ?? "headline";

	console.log(actionSets);
	const help = actionSets.filter( n => n.prefix).map( n =>
		`• ${n.prefix} – ${n.name}\n` +
		`  version ${n.version}\n` +
		`  ${n.description?.replaceAll("<prefix>", actionSet.prefix) ?? "(No description available)"}\n` +
		`  Requires registration: ${n.registration?.required? "yes" : "no"}` +
		(n.isUserRegistered(ctx)
			? " (you are already registered)."
			: " (you are not registered).")
	).join("\n");

	const message = xml(
		"message",
		{ "xml:lang": "en", to: sender, from: addressee, type },
		xml("body", {},
			`Available action sets:\n\n${help}\n\nType one of the action set prefixes for a list of available commands.`
		)
	);

	component.sendStanza(message);
}

function showAvailableCommands (component, ctx, actionSet) {
	const sender = ctx.stanza.attrs.from;
	const addressee = ctx.stanza.attrs.to;
	const type = ctx.stanza.attrs.type ?? "headline";

	const help = actionSet.nodes.map( n =>
		`• ${n.id} – ${n.name}\n    ${(n.description??"").replaceAll("<prefix>", actionSet.prefix)}\n`
	).join("\n");

	const message = xml(
		"message",
		{ "xml:lang": "en", to: sender, from: addressee, type },
		xml("body", {},
			`Available commands for ${actionSet.prefix} (${actionSet.name}) are:\n\n${help}`
		)
	);

	component.sendStanza(message);
}


/* Handler for message stanzas that tries to recognise action commands.
 *
 * Returns true if the message was handled as an action command, whether
 * an actual action was performed or not. Returns false if the message
 * was not recognised as something to be acted upon.
 */
function handleActions (stanza, item) {
	let handled = false;
	let prefixMatch = null;
	const ctx = {
		from: jid(stanza.attrs.from),
		to: jid(stanza.attrs.to),
		stanza
	};
	console.log("FOR ITEM", item.jid);
	const body = stanza.getChildText("body");
	console.log("WITH BODY", body);
	if (body === null) {
		// This message does not have a body, so ignore it.
		return false;
	}

	// Get item action sets (if any)
	const actionSets = ActionSets.getFrom(item, ctx);
	// See if the body of the incoming stanza matches
	// any of the action set prefixes
	for (const actionSet of actionSets) {
		console.log("CHECKING ACTION", actionSet.prefix);
		if (actionSet.prefix) {
			if (body.startsWith(actionSet.prefix+" ")) {

				// If it does, see what is the user's registration status
				// and whether registration is required.
				if (actionSet.registration?.required && !actionSet.isUserRegistered(ctx)) {
					// TODO Offer the user to register.
					sendMessage(item.parent, "Sorry, these actions require registration and you are not currently registered.", ctx.to, ctx.from, ctx.stanza.attrs.type);
					return false;
				}

				// see if it matches any of the commands
				const cmdText = body.slice(actionSet.prefix.length+1)
				console.log("CMDTEXT", `'${cmdText}'`);

				console.log(actionSet.nodes.map(n => `${n.id}: ${cmdText.startsWith(n.id)}`));
				const command = actionSet.nodes.find(n => cmdText.startsWith(n.id));
				if (command) {
					// Try to parse the stanza according to the command definition
					const payload = cmdText.slice(command.id.length+1)
					console.log("PAYLOAD", `'${payload}'`);
					if (command.parser.parse(payload, stanza)) {
						// This command matched. We stop here.
						// NOTE: Should I allow multiple commands to match?
						// If so, then change "return true" → "handled = true"
						return true;
					}
				}

				// It didn't match any commands, but it may match another
				// actionSet with the same prefix. We make a note of the
				// fact that this prefix was matched though, so we can show
				// a message to the user if in the end we didn't find any
				// commands.
				prefixMatch = actionSet.prefix;

			} else if (body == actionSet.prefix) {
				// If there is no command text, show a list of
				// available commands for this prefix.
				showAvailableCommands(item.parent, ctx, actionSet);
				handled = true;
			}
		}
	}
	// If we got to this point, the command did not match any known prefixes
	// If the message consists solely of the text "/help" and notifications
	// are not enabled, show a list of available command prefixes.
	// NOTE: "help" is also accepted in non-MUC messages as some clients
	// intercept "/help" (with slash).
	if (body == "/help" || (stanza.attrs?.type != "groupchat" && body.trim().toLowerCase() == "help")) {
		showAvailablePrefixes(item.parent, ctx, actionSets);
		return true;
	}

	if (prefixMatch) {
		sendMessage(item.parent, `No such command. Type \`${prefixMatch}\` for a list of available commands.`, ctx.to, ctx.from, ctx.stanza.attrs.type);
	} else if (/^\s/.test(body)) {
		const trimmedBody = body.trim();
		const actionSet = actionSets.find(i => i.prefix && trimmedBody.startsWith(i.prefix))
		if (actionSet) {
			sendMessage(item.parent, `Did you intend to call a ${actionSet.prefix} action? If so, please remove the whitespace from the beginning of the line.`, ctx.to, ctx.from, ctx.stanza.attrs.type);
		}
	}

	return handled;
}

module.exports = handleActions;

