# Webhook XMPP Component – receive notifications on webhook events

[[_TOC_]]

## Description

[![](doc/media/XMPP-Webhooks-diagram.svg)](doc/media/XMPP-Webhooks-diagram.svg)

This component lets authorised users create “projects” which provide a webhook endpoint to receive notifications on. Other XMPP users can subscribe to the project JID and then receive notifications on webhook events.

Its original purpose was to receive notifications from Git, Git foundries and so on, but it can be easily extended for use in many other contexts – see the sections on ‘other integrations’ and ‘templates’ for more information on this.

See [below](#demo-server) for details of a demonstration server.

### Separation of concerns

* *Administrators* are responsible for a component installation. They are able to configure who has access to it and what services are available.
* *Endpoint owners* are users authorised to create their own webhook endpoints, the configuration of which they can manage freely, including deciding who can subscribe to their endpoints.
* *Endpoint subscribers* are the users who subscribe to an endpoint JID and receive notifications when webhooks are triggered.

### Main features

* Customisable: **Messages are template-based.** The instance administrator can change the templates and even add new ones.
* Extensible: all it takes to **add new integrations** is touching three files (see commit 14b5644f49454ea3881d1d41417aeb93a1e9e08d for an example).
* Administrators can **restrict access** to their instance to users whose JID matches a regular expression.
* Normal users can **create their own endpoints** via ad-hoc commands, even on someone else's instance, if so authorised by its administrator.
* Endpoint JIDs support restricting access only to subscribers whose JID matches a regular expression.
* **Webhooks may be secured** via ‘HTTP Basic Authorization’, a ‘secret’ token passed via a custom HTTP header or, for some integrations, via their custom signature schemes.
* **MUC support:** endpoint JIDs may be directed by its owner to join any MUC, subject to the latter's access restrictions; endpoint owners can also configure the endpoint to automatically accept MUC invitations from anyone who is authorised to subscribe to the endpoint's presence.
* Can **send attachments** via [HTTP File Upload](https://xmpp.org/extensions/xep-0363.html).
* Support for advanced XMPP features, such as [Attention](https://xmpp.org/extensions/xep-0224.html) and [Message Processing Hints](https://xmpp.org/extensions/xep-0334.html).
* [Many integrations](#recognised-webhook-events) supported out of the box.

## Installation

Clone this repository, then:

```bash
cd webhooks
npm install
```

## Configuration

Site-specific configuration is specified in two files in the project's top directory: `settings.yaml` and `templates.yaml`.

Example `settings.yaml`:

```yaml
---
xmpp:
    # Service name, used in service discovery
    name: Git Notifications
    # Host and port in which to reach the XMPP server.
    # Must match the configuration on the server.
    service: xmpp://example.net:5231
    # Domain to make ourselves available as. Must be
    # compatible with the XMPP server configuration
    # and should have a TLS certificate.
    domain: git.example.net
    # Password used by the component to identify itself
    # to the server. Must match the server configuration.
    password: xxxxxx

services:
    # An optional HTTP file upload (XEP-0363) service
    upload: upload.example.net

persistence:
    # Where to persist the data between restarts (optional but recommended).
    pubsub: "pubsub.example.net"

registration:
    # Who can / cannot create “projects” in this instance. A list
    # of regular expressions.
    allow:
        - "^.*@example.net$"
    deny: []

http:
    # Details of the webhook server. Typically this should
    # run behind a proxy such as Nginx.
    host: "127.0.0.1"
    port: 3001
    path: /webhooks
    # This is a template of the *public* URL of the webhook endpoints.
    # The `{id}` parameter will be replaced with the random
    # UUID corresponding to each project. The final URL is shown to the
    # user on project creation.
    url: "https://example.net/webhooks/{id}"
```

### Importing and exporting state

If so configured by the component administrator in `settings.yaml`, state is persisted via PubSub.

It is possible to override the PubSub data, or provide initial state, on start-up via `node ./index.js --load-file /path/to/file.json` (or `npm start -- --load-file /path/to/file.json`).

State data may be exported to file via `node ./index --save-file /path/to/file.json` (or `npm start -- --save-file /path/to/file.json`).

## Usage

### Introduction

When started and connected to the XMPP server, a new item will be available on the server upon running a service discovery query (therefore, a client supporting service discovery should be used). A discovery query might return, for instance:

[![](doc/media/005-services-list-s.png)](doc/media/005-services-list.png)

After a project has been created, it is revealed in the service discovery results:

[![](doc/media/010-component-items-s.png)](doc/media/010-component-items.png)

### Creating endpoints

#### The administrator's job

The component administrator (i.e., the person who runs the component) is able to configure who should be able to create endpoints via a series of regular expressions in the `settings.yaml` configuration file. For instance, the following setup gives everyone the right to create endpoints except users of `example.biz` and anyone whose JID starts with `spammer`:

```yaml
registration:
    # Who can / cannot create ‘projects’ in this instance. A list
    # of regular expressions.
    allow:
        - "^.*$"
    deny:
        - "^.*@example\\.biz$"
        -"^spammer@.*$"
```

#### The user's job

As a user, provided that you are authorised by the component's policies, your first step is to make yourself known to the component. You do this via your client's [registration](https://xmpp.org/extensions/xep-0077.html "XEP-0077: In-Band Registration") mechanism.

[![](doc/media/007-registration-s.png)](doc/media/007-registration.png)

Once registered, querying the component will reveal an `Add a project` [ad-hoc command](https://xmpp.org/extensions/xep-0050.html "XEP-0050: Ad-Hoc Commands").

[![](doc/media/030-add-project-command-s.png)](doc/media/030-add-project-command.png)

When executed, a form will be shown where the user can configure the details of his ‘project’ (*project* was what endpoints were originally called).

[![](doc/media/033-add-project-dialog-s.png)](doc/media/033-add-project-dialog.png)

An endpoint has both an XMPP JID (chosen by the user), to which other people may subscribe, and an HTTP API implementing the webhook. The URL of this API is a combination of the parameters in `settings.yaml` (chosen by the component administrator) and a random identifier, therefore not under control of the user creating the endpoint. Both the HTTP URL and the XMPP JID will be sent to the user in an XMPP message upon successful creation.

[![](doc/media/036-project-created-message-s.png)](doc/media/036-project-created-message.png)

### Receiving notifications

Any authorised user can subscribe to any of the available projects as if it were just another XMPP contact. From then on the subscribing user will receive notifications on webhook events.

[![](doc/media/050-notification-desktop-s.png)](doc/media/050-notification-desktop.png)

Note that unlike the case for endpoint creators, endpoint subscribers do not need a ‘fancy’ XMPP client (i.e., one supporting [service discovery](https://xmpp.org/extensions/xep-0030.html "XEP-0030: Service Discovery") and [ad-hoc commands](https://xmpp.org/extensions/xep-0050.html "XEP-0050: Ad-Hoc Commands")); pretty much any client will do. For instance [Conversations](https://conversations.im/) or any of its many derivatives will do a superb job.

[![](doc/media/051-notification-mobile-s.png)](doc/media/051-notification-mobile.png)

### Chatrooms ([MUC](https://xmpp.org/extensions/xep-0045.html "XEP-0045: Multi-User Chat"))

Via an ad hoc command, the endpoint owner can add chatroom subscriptions so that the component items can participate in project MUC rooms.

[![](doc/media/040-add-groupchat-subscription-command-s.png)](doc/media/040-add-groupchat-subscription-command.png)

#### Invitations

At endpoint creation time, the owner can choose to enable the option to accept chatroom invitations. When that option is active, any participant in a chatroom can send an invitation to it and the endpoint will automatically join it and start posting messages.

## Notifications

### Recognised webhook events

#### Plain Git

Save this as `./.git/hooks/post-commit` in your Git project directory:

```bash
#!/bin/bash

# This is your webhook endpoint, obtained when creating the project item
# in the XMPP server via the `Add a project` command.
URL="https://example.net/webhooks/c83c53f0-4e09-4aea-8ecc-79077594bc4f"

git log -n 1 --format=medium |
        curl -qs "$URL" -H "Content-Type: text/plain" --data-binary @-

exit 0

```

Every time you commit something in your computer (and assuming you have connectivity to the webhook endpoint) a notification will be sent to all subscribers to the project JID.

#### Gitlab

In the project settings in Gitlab, add the webhook URL and enable the event types that you are interested in (only `push`, `issue`, `merge_request` and `note` events have templates defined at this time, but you can easily add appropriate entries to `templates.yaml` – see below for details).

#### Gitea

[Gitea webhooks](https://docs.gitea.io/en-us/webhooks/) are supported. If `X-Gitea-Signature` is provided as a secret token name and the signing secret is saved as the secret token value, webhook payload integrity will be verified by the component and any events failing the integrity check will be silently dropped.

[![](doc/media/061-gitea-endpoint-creation-s.png)](doc/media/061-gitea-endpoint-creation.png)


The following event types are currently supported out of the box:

* push
* issues
* issue comments

Additional events may be defined by the component instance administrator by adding the relevant entries in [`templates.yaml`](/templates.yaml) following the model of the existing Gitea events. *Note: if you modify `templates.yaml` remember to save a copy, as it will be overwritten every time you upgrade the component.*

#### Slack

Following a [user request](https://gitlab.com/navlost.eu/xmpp/components/webhooks/-/issues/1), support for [Slack notifications](https://api.slack.com/messaging/webhooks) has been added. This support is completely untested and unsupported, as the author is not a Slack user.

#### Stripe

[Stripe webhooks](https://stripe.com/docs/webhooks) are supported, including [signature verification](https://stripe.com/docs/webhooks/signatures) provided that the endpoint creator has defined `Stripe-Signature` as a secret token name and given the corresponding signing key as secret token value.

The following event types are currently supported out of the box:

* payment_intent.created
* payment_intent.amount_capturable_updated
* payment_intent.succeeded
* payment_intent.canceled

Additional events may be defined by the component instance administrator by adding the relevant entries in [`templates.yaml`](/templates.yaml) following the model of the existing Stripe events. *Note: if you modify `templates.yaml` remember to save a copy, as it will be overwritten every time you upgrade the component.*

#### SMS

Android users can forward their SMS to XMPP via the [‘Incoming SMS to URL forwarder’](https://github.com/bogkonstantin/android_income_sms_gateway_webhook) app [available on F-Droid](https://f-droid.org/packages/tech.bogomolov.incomingsmsgateway/).

The [template definition](https://gitlab.com/navlost.eu/xmpp/components/webhooks/-/blob/7a4dde6cc246bf640710a3e88be2b3cddda604fe/templates.yaml#L352) for events from this application nicely illustrates the use of some extra XMPP features such as [Attention](https://xmpp.org/extensions/xep-0224.html) and [Message Processing Hints](https://xmpp.org/extensions/xep-0334.html).

#### Nextcloud

[Nextcloud](https://nextcloud.com/) has a [webhooks plugin](https://github.com/kffl/nextcloud-webhooks) that integrates with [Nextcloud Flow](https://docs.nextcloud.com/server/latest/developer_manual/digging_deeper/flow.html) and allows the user to receive events when things happen on their cloud account, such as receiving new files.

If `X-Nextcloud-Webhooks` is provided as a secret token name and the signing secret is saved as the secret token value, webhook payload integrity will be verified by the component and any events failing the integrity check will be silently dropped.

#### Media type fallback

If the message wasn't recognised and handled by another middleware, a [`POST`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST) or [`PUT`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT) request will be handled by [`lib/WebhookHttpApi/middleware/99-default.js`](lib/WebhookHttpApi/middleware/99-default.js) which will cause the request to be matched according to the value of the [`Content-Type`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type) header.

Currently, the following MIME types are handled:

MIME type | Action
----------|--------
`application/json` | The JSON object is pretty-printed and displayed as the message.
`application/pdf` | Sent as an attachment and offered for download.
`image/jpeg` | Sent as an attachment and offered for download.
`image/png` | Sent as an attachment and offered for download.
`text/plain` | The request body is displayed as the message.

Refer to the relevant entries in [`templates.yaml`](templates.yaml) for an example of how to handle file uploads.

#### Adding other integrations

Write a handler in `WebhookHttpApi/middleware` and add it to `WebhookHttpApi/middleware/index.js`. Your handler should call `next()` without taking any actions if it doesn't recognise the payload. If it does, it should call `req.app.locals.parent.emit('message', {id: req.params.id, type: "some_event_type", payload: req.body})` or something similar and return [HTTP 202 Accepted](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/202). Look at the existing handlers for guidance.

### Notification templates

When a `message` event is received, the component looks at [`templates.yaml`](templates.yaml) for an [EJS](https://ejs.co/) template to use to prepare the actual XMPP message. The keys in [`templates.yaml`](templates.yaml) correspond to the `type` parameter in the `message` payload. For instance, a plain Git commit will emit a `message` like:

```json
{
  "id": "c83c53f0-4e09-4aea-8ecc-79077594bc4f",
  "type": "commit",
  "payload": {
    "subject": "First line of commit message",
    "message": "First line of commit message\n\nDetailed commit description.\n",
    "author": "A. Commiter <commiter@example.net>",
    "tstamp": "1970-01-01T00:00:00.000Z",
    "commit": "20436fb7dcd66de4f7a12436c640f03129bbdf18"
  }
}
```

This will match the `commit` key in [`templates.yaml`](templates.yaml).

Each template consists of a `subject` part and a `body` part, as notifications are sent by default with a type of `headline`. For chat room notifications, the subject is sent as a separate message which will cause the room subject to be changed, if allowed by the latter's configuration settings.

The webhook notification payload is available to the template as the `data` property. You are free to format the message in whichever way it suits your users' needs.

#### File uploads

The component supports [XEP-0363: HTTP File Upload](https://xmpp.org/extensions/xep-0363.html). The component administrator must enable this by specifying the name of the upload service in the configuration file. See [`settings-example.yaml`](settings-example.yaml).

To upload a file you must create a template entry with the following keys:

```yaml
some-entry:
    upload:
        body:     # File contents as a Buffer.
        filename: # Name to save the content as.
        mimetype: # MIME type of the content.
```

This is handy in combination with the [media type fallbacks](#media-type-fallbacks). For instance, to allow users to upload JPEG files via a webhook there exists the following entry as part of the default [template definitions](templates.yaml):

```yaml
image/jpeg:
    upload:
        body: <%- payload %>
        filename: <%- path %>
        mimetype: image/jpeg
```

So an authorised user could send a picture to all webhook XMPP subscribers via something like:

```bash
curl -H "Content-Type: image/jpeg" "https://user:passwd@example.net/webhooks/c83c53f0-4e09-4aea-8ecc-79077594bc4f/my-cat.jpeg" --data-binary @my-cat.jpeg
```

## Actions (slash commands)

*Actions* are commands that are sent to an external system for processing; for instance, API requests. They enable two-way interaction between the component and the external system.

These are known as *slash commands* in other contexts. In this component, all actions may be invoked as [XEP-0050](https://xmpp.org/extensions/xep-0050.html "Ad-Hoc Commands") ad hoc commands and, when so configured, directly from a chat message in the form of a slash command.

Whether actions are available on your server depends on the administrator, as those must be made available explicitly, unlike webhook handler, which come installed by default.

For more about this feature, read the [actions](doc/actions.md) page in this repository.

[![](doc/media/108-action-example.png)](doc/media/108-action-example.png)

### Creating and commenting on issues via XMPP without a GitLab account (`gitlab-issues`)

Not everyone has an account on every GitLab server out there, and not everybody wants one. For this reason, administrators may offer the `gitlab-issues` template, which is a combination of `gitlab-read` and `gitlab-write` with the exception that it requires the endpoint owner to configure an access token with *write* access to the issue API. This means that your users can report issues with nothing more than an XMPP account. ☺

A similar but somewhat less tested template exists also for [Gitea](https://gitea.io/) ([`/data/templates/actions/gitea.yaml`](/data/templates/actions/gitea.yaml)).

## Outgoing messages

If an endpoint has been configured with the *Receive chat messages* option enabled, any messages sent to its JID will be queued by the component and delivered on the next [`GET`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET) request made to the endpoint.

The `GET` method recognises the following values for the [`Accept`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept) header:

Value              | Response
-------------------|---------
`text/plain`       | The body of any messages having one will be sent verbatim, one message per line. It won't be possible to distinguish between multiline and multiple one-line messages. Sender information is lost.
`text/xml`         | Same as `application/xml`.
`application/xml`  | An XMPP-like XML stream containing all of the received stanzas.
`application/json` | A JSON payload with a (quite wordy) structure based on the XML form of the stanza.

### Sending and receiving messages via [cURL](https://curl.se/)

The media type fallback permits a form of poor-man, half-duplex XMPP communication without an XMPP client.

For instance, assume the existence of an endpoint `https://example.net/webhooks/c83c53f0-4e09-4aea-8ecc-79077594bc4f` owned by a JID `xmpp://webhook@example.net` and protected with [Basic authentication](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication) credentials of `user` / `passwd`.

To send the message *‘Hello there’* to everyone subscribing to `xmpp://webhook@example.net`'s presence, issue the following cURL request:

```bash
# POST is implicit in the presence of --data
curl -s "https://user:passwd@example.net/webhooks/c83c53f0-4e09-4aea-8ecc-79077594bc4f" -H "Content-Type: text/plain" --data "Hello there"
```

If the endpoint has been configured to receive chat messages, any messages sent to `xmpp://webhook@example.net` can be retrieved with this cURL request:

```bash
# Accept: application/xml or Accept: application/json (the default) are also supported.
curl -s "https://user:passwd@example.net/webhooks/c83c53f0-4e09-4aea-8ecc-79077594bc4f" -H "Accept: text/plain"
```

In some cases this may simplify integration with non XMPP aware systems. For instance, see snippet $2238992 for an example of how to leverage this feature to receive Git patches over XMPP.

## Implemented XEPs

XEP | Version | Description
----|---------|-------------
[XEP-0004: Data Forms](https://xmpp.org/extensions/xep-0004.html) | 2.12.1 (2021-06-08) | Data forms are used along with ad-hoc commands to manage endpoints. Normal subscribers do not need data forms support.
[XEP-0012: Last Activity](https://xmpp.org/extensions/xep-0012.html) | 2.0 (2008-11-26) | For an endpoint, shows when the last webhook message was successfully received and processed; for the component, it shows how long it's been running since the last restart.
[XEP-0030: Service Discovery](https://xmpp.org/extensions/xep-0030.html) | 2.5rc3 (2017-10-03) | Used to find out what features the host server supports and to identify Publish-Subscribe and HTTP Upload services if none are specified in the configuration.
[XEP-0045: Multi-User Chat](https://xmpp.org/extensions/xep-0045.html) | 1.34.2 (2021-06-24) | The endpoints can join and post to chatrooms.
[XEP-0050: Ad-Hoc Commands](https://xmpp.org/extensions/xep-0050.html) | 1.3.0 (2020-06-09) | Ad-hoc commands are used to create and manage endpoints. Normal subscribers do not need them.
[XEP-0054: vcard-temp](https://xmpp.org/extensions/xep-0054.html) | 1.2 (2008-07-16) | Gives endpoint owners a convenient place where to communicate important information about their endpoint.
[XEP-0060: Publish-Subscribe](https://xmpp.org/extensions/xep-0060.html) | 1.22.1 (2021-12-26) | State data such as the list of endpoints, subscribers, etc., is saved in a PubSub node.
[XEP-0077: In-Band Registration](https://xmpp.org/extensions/xep-0077.html) | 2.4 (2012-01-25) | Used to keep track of endpoint owners. Theoretically this could also allow owners to claim a prefix in their webhook URLs and some other things.
[XEP-0092: Software Version](https://xmpp.org/extensions/xep-0092.html) | 1.1 (2007-02-15) | Useful information when reporting bugs.
[XEP-0114: Jabber Component Protocol](https://xmpp.org/extensions/xep-0114.html) | 1.6 (2012-01-25) | The service itself is implemented as a component.
[XEP-0115: Entity Capabilities](https://xmpp.org/extensions/xep-0115.html) | 1.5.2 (2020-05-05) | Implemented for both endpoints and the component itself.
[XEP-0199: XMPP Ping](https://xmpp.org/extensions/xep-0199.html) | 2.0.1 (2019-03-26) | Implemented for both endpoints and the component itself.
[XEP-0224: Attention](https://xmpp.org/extensions/xep-0224.html) | 1.0 (2008-11-13) | If the template key `urn:xmpp:attention:0` evalues to the literal string value `"true"` (as opposed to boolean `true`), the Attention extension will be included in the message. See the entry for `sms/tech.bogomolov.incomingsmsgateway` in [`templates.yaml`](/templates.yaml) for a usage example.
[XEP-0334: Message Processing Hints](https://xmpp.org/extensions/xep-0334.html) | 0.3.0 (2018-01-25) | Hints are configurable via templating. See the entry for `sms/tech.bogomolov.incomingsmsgateway` in [`templates.yaml`](/templates.yaml) for a usage example.
[XEP-0363: HTTP File Upload](https://xmpp.org/extensions/xep-0363.html) | 1.0.0 (2020-02-11) | Webhooks can accept binary data and the component will forward it as an attachment.

## Security

> This is not production quality software. It was put together from various bits of internal code copied and pasted together in a haphazard way. Users should do their own risk assessment before deploying this code.

### Confidentiality

End users should be aware that the component administrator (and the XMPP server administrator, if different) have access to the list of defined ‘projects’, their properties and the list of subscribers, all of which information may be persisted on disk. The component (and if different, XMPP and HTTP proxy) server administrator(s) also have access to the webhook payloads. Users should only create projects in servers that they trust sufficiently for this purpose.

Subscribers to a project JID should know that their JID is accessible to the component administrator and that this information is persisted to disk. The component does not request nor require presence information from subscribers, but some clients pre-emptively send subscription authorisation and presence information to JIDs they subscribe to – meaning that the component administrator may be able to see subscribers' presence status.

The connection between the component and the XMPP server is not encrypted. Risks derived from this may be mitigated by running the component on the same host as the XMPP server or encapsulating the connection in an encrypted tunnel.

The HTTP API is not encrypted. It is strongly recommended to have the HTTP API listening on the loopback interface only and to proxy the connection via a properly secured HTTPS server, such as Nginx.

The list of projects, their properties and subscribers may be saved to a Publish-Subscribe XMPP service if so configured by the component administrator (and this will usually be the case). The data is saved unencrypted. Administrators concerned by this risk should ensure that the PubSub service being used (which does not to be the same XMPP server as the one hosting the component) provides satisfactory security guarantees.

The component does not support end to end encryption. This is by design as it would not mitigate any of the risks mentioned above.

### Integrity

The webhook handlers provided in this repository as of this writing do not support any form of integrity checking, with the exception of [Stripe webhooks](https://stripe.com/docs/webhooks), which are [checked for integrity](https://gitlab.com/navlost.eu/xmpp/components/webhooks/-/issues/21) if the endpoint owner has provided the secret during handler configuration. Otherwise, where available (e.g., [Gitea webhooks](https://docs.gitea.io/en-us/webhooks/) appear to provide an HMAC hash) it is the responsibility of the handler author to make their own decisions as to integrity assurance.

### Availability

The component supports persisting project properties and subscriptions to a PubSub node but this is an optional feature. If not used, project definitions will be lost when the component shuts down.

A loss of connectivity between the component and the XMPP server will cause it to be unavailable. Any messages received while disconnected may be irretrievably lost. The component will try to reconnect to the server at regular intervals.

A loss of connectivity between the component and the HTTP proxy, if one is used, will cause any webhook notifications to not be received – this will normally be detectable by the sender. What happens in this case depends on the sending agent, which may or may not try to resend the notification.

If the subscriber is offline, messages will typically be held by the server until the user becomes available again. Whether messages are then archived by the server or by the client depends on their respective configurations. The component sends messages as type `headline`, with no explicit storage instructions.

In order to be able to create and manage ‘projects’, users must have an XMPP client providing a service discovery ([XEP-0030: Service Discovery](https://xmpp.org/extensions/xep-0030.html)) interface and supporting [XEP-0050: Ad-Hoc Commands](https://xmpp.org/extensions/xep-0050.html) and [XEP-0004: Data Forms](https://xmpp.org/extensions/xep-0004.html).

Users merely wishing to receive notifications only need to be able to discover a project's JID somehow, either via a service discovery mechanism or by virtue of the JID being announced publicly, e.g., in the project's documentation.

Participants in chatrooms which have been signed up to receive notifications by a project's creator do not need to perform any form of discovery.

### Authenticity

#### HTTP

The component supports one authentication and one authorisation mechanism (in practice, this boils down to the same thing: accepting a webhook submission).

The owner of a webhook may, at creation time or any time later, optionally provide a username and password, which will be using as credentials in HTTP's [Basic authentication](https://datatracker.ietf.org/doc/html/rfc7235) mechanism.

The owner may also optionally provide the name of an arbitrary HTTP header (for instance, [`X-GitLab-Token`](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#validate-payloads-by-using-a-secret-token)) and a corresponding value.

If both mechanisms are configured, requests that satisfy *either* of them are allowed. If neither is configured, all requests are allowed.

#### XMPP

Authentication of XMPP users is the responsibility of the XMPP server hosting their accounts.

## Container image

The component is available as a container image compatible with [Docker](https://www.docker.com/), [podman](https://podman.io/), etc. To use it:

```bash
docker pull registry.gitlab.com/navlost.eu/xmpp/components/webhooks:latest
# or
podman pull registry.gitlab.com/navlost.eu/xmpp/components/webhooks:latest
```

The HTTP API is exposed on port 3001.

## Podman pod

The [create-pod.sh](etc/container/create-pod.sh) script can be used to create, configure and run a [*pod*](https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods "Podman: Managing pods and containers in a local container runtime") consisting of an [ejabberd](https://www.ejabberd.im/) server running an instance of this component.

### Usage

As root (see below why):

```bash
# Replace `chat.example.com` with your own domain name
./create-pod.sh chat.example.com
```

The script will build or pull the images, create the configuration, grab the TLS certificates and start everything. It may take a couple of minutes the first time, and restart once or twice. At the end you should have a running pod named `xmpp-webhooks-pod`:

```
# podman pod ps
POD ID        NAME               STATUS   CREATED         INFRA ID      # OF CONTAINERS
d7073e6568ab  xmpp-webhooks-pod  Running  28 seconds ago  995ac595aeef  2
```

Subsequent restarts may omit the domain name:

```bash
./create-pod.sh
```

The pod's default configuration allows anyone to create endpoints on it. To restrict this or change any of the other options, modify the `settings.yaml` file that will be created under [`etc/container/etc/xmpp-webhooks`](etc/container/etc/xmpp-webhooks) after the first run.

Likewise, the ejabberd configuration will be found in `ejabberd.yml` under [`etc/container/etc/ejabberd`](etc/container/etc/ejabberd).

The message templates are in [`etc/container/etc/xmpp-webhooks/templates.yaml`](etc/container/etc/xmpp-webhooks/templates.yaml) (which by default symlinks to [`templates.yaml`](templates.yaml)).

### Requirements

* You need to have [podman](https://podman.io/) installed. It may work with Docker (with modifications) but that has not been tested at all.
* It will take over TCP ports 80 (HTTP), 443 (HTTPS), 3001 (API), 5222 (XMPP c2s) and 5269 (XMPP s2s). What this means is that:
  - you will not be able to run this alongside an HTTP or XMPP server on the same host (unless they listen on non-standard ports); and
  - it needs to run as root in order to be able to bind to ports 80 & 443.
* The domain name being used should point to the host running this pod, and it should be reachable from the internet, if nothing else because of the [ACME](https://datatracker.ietf.org/doc/html/rfc8555) verification.
* The path [`etc/container/var`](etc/container/var) path needs to be writable.

## Demo server

A demonstration instance is available at `webhooks.xmpp.labs.navlost.eu`. As of this writing it hosts two endpoints:

* [demo@webhooks.xmpp.labs.navlost.eu](xmpp:demo@webhooks.xmpp.labs.navlost.eu?subscribe) is a generic endpoint to which everyone can subscribe and send HTTP payloads (check the endpoint's vCard for the authorisation token).
* [self@webhooks.xmpp.labs.navlost.eu](xmpp:self@webhooks.xmpp.labs.navlost.eu?subscribe) is an endpoint where notifications are sent every time this project is updated in GitLab. Everyone can subscribe to it.

Users can also create their own endpoints on this server and decide who should be allowed to subscribe to them.

*Note: for more than cursory testing or to host your own component, consider using the [container image](#container-image) (if integrating with your existing XMPP and web servers) or the [podman pod](#podman-pod) script (if spinning up a dedicated server).*

### Disclaimer

**The aforementioned server is made available strictly on the understanding that it is to be used for demonstration and testing purposes only and there are no availability nor security guarantees whatsoever. In particular, do not use it with non-public or personal data as I, the developer and maintainer, have full access to it and can and often do check what is going through it in order to improve the software and mitigate abuse.**

### Contact

Questions and feedback may be posted to [lounge@conference.xmpp.labs.navlost.eu](xmpp:lounge@conference.xmpp.labs.navlost.eu?join) but please be patient: it may be days or weeks before anyone sees your message. GitLab itself is of course also an option, and it offers an [email service desk](mailto:contact-project+navlost-eu-xmpp-components-webhooks-24572215-issue-@incoming.gitlab.com) for those who do not have an account or don't want to use it. See also [CONTRIBUTING](CONTRIBUTING.md) for further details.
